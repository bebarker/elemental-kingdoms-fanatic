﻿namespace ElementalKingdoms.ui
{
    partial class MapStagesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rbExplore = new System.Windows.Forms.RadioButton();
            this.rbFight = new System.Windows.Forms.RadioButton();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // rbExplore
            // 
            this.rbExplore.AutoSize = true;
            this.rbExplore.Checked = true;
            this.rbExplore.Location = new System.Drawing.Point(178, 28);
            this.rbExplore.Name = "rbExplore";
            this.rbExplore.Size = new System.Drawing.Size(76, 21);
            this.rbExplore.TabIndex = 0;
            this.rbExplore.TabStop = true;
            this.rbExplore.Text = "Explore";
            this.rbExplore.UseVisualStyleBackColor = true;
            // 
            // rbFight
            // 
            this.rbFight.AutoSize = true;
            this.rbFight.Location = new System.Drawing.Point(294, 28);
            this.rbFight.Name = "rbFight";
            this.rbFight.Size = new System.Drawing.Size(60, 21);
            this.rbFight.TabIndex = 1;
            this.rbFight.Text = "Fight";
            this.rbFight.UseVisualStyleBackColor = true;
            // 
            // MapStagesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 481);
            this.Controls.Add(this.rbFight);
            this.Controls.Add(this.rbExplore);
            this.Name = "MapStagesForm";
            this.Text = "MapStagesForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbExplore;
        private System.Windows.Forms.RadioButton rbFight;
        private System.Windows.Forms.ToolTip toolTip;
    }
}