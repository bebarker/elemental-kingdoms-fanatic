﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ElementalKingdoms.core;

namespace ElementalKingdomsTests
{
    [TestClass]
    public class BattleCardTests
    {
        [TestMethod]
        public void TestBonusSkillHp()
        {
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 500 },
                    AttackArray = new int[] { 0 },
                }
            );
            UserCard uc = new UserCard() { CardId = 1, Level = 0 };
            BattleCard bc = new BattleCard(uc);
            Assert.AreEqual(500, bc.CurrentHP);// Mountain Lion level 10 = 265 hp

            bc.BonusSkillHP += 200;
            Assert.AreEqual(200, bc.BonusSkillHP);
            Assert.AreEqual(700, bc.CurrentHP);

            bc.CurrentHP -= 500;
            Assert.AreEqual(200, bc.BonusSkillHP);
            Assert.AreEqual(200, bc.CurrentHP);

            bc.BonusSkillHP += 200;
            Assert.AreEqual(400, bc.BonusSkillHP);
            Assert.AreEqual(400, bc.CurrentHP);

            bc.CurrentHP += 200;
            Assert.AreEqual(400, bc.BonusSkillHP);
            Assert.AreEqual(600, bc.CurrentHP);

            bc.BonusSkillHP -= 200;
            Assert.AreEqual(200, bc.BonusSkillHP);
            Assert.AreEqual(600, bc.CurrentHP);

            //bc.CurrentHP -= 100;
            //Assert.AreEqual(200, bc.CurrentHP);
            //bc.BonusSkillHP -= 100;
            //Assert.AreEqual(200, bc.CurrentHP);

            bc.BonusSkillHP -= 1000000;
            Assert.AreEqual(bc.BonusSkillHP, 0);
        }

        [TestMethod]
        public void Regeneration()
        {
            // Prepare: Card with healing. Card that does damage. Combat
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1,
                    Type = SkillType.Heal,
                    LanchType= LaunchType.OnSkillExecutePhase,
                    AffectType = 21,
                    AffectValue = 100,
                    SkillCategory = 4,
                    Desc = "Restores 100 HP to all of your cards"
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 200 },
                    Wait = 0,
                    Skill = 1
                }
            );

            UserCard uc = new UserCard() { CardId = 1, Level = 0 };

            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(uc);
            attacker.UserDeck.Cards.Add(uc);

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(uc);

            const int maxHP = 24840;

            Battle b = new Battle(attacker, defender);
            b.Round();

            // First card of attacker is on battlefield (since waittime = 0)
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Hand.Count);

            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP); // ensure not healed above its max hp

            Assert.AreEqual(maxHP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(maxHP - 200, b.DefendPlayer.CurrentHP);

            // Defender puts card on battlefield. this card attacks the card of the attacker
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Hand.Count);

            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].CurrentHP);

            Assert.AreEqual(maxHP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(maxHP - 200, b.DefendPlayer.CurrentHP);

            // Attacker puts another card on battlefield. first card attacks defending card, second attacks player. Both cards also cast heal, so first card hp is back to max hp.
            b.Round();

            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Hand.Count);

            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(800, b.DefendPlayer.Battlefield[0].CurrentHP);

            Assert.AreEqual(maxHP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(maxHP - 400, b.DefendPlayer.CurrentHP);

            // No more cards for defender to put on battlefield. Heal a little bit, attack a little bit
            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Hand.Count);

            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(900, b.DefendPlayer.Battlefield[0].CurrentHP);

            Assert.AreEqual(maxHP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(maxHP - 400, b.DefendPlayer.CurrentHP);

            // No more cards for attacker to put on battlefield. Heal a little bit, attack a little bit
            b.Round();

            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Hand.Count);

            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(700, b.DefendPlayer.Battlefield[0].CurrentHP);

            Assert.AreEqual(maxHP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(maxHP - 600, b.DefendPlayer.CurrentHP);
        }

        [TestMethod]
        public void Snipe()
        {
            // Prepare: Card with healing. Card that does damage. Combat
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 2,
                    Type = SkillType.Snipe,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 22,
                    AffectValue = 150,
                    SkillCategory = 2,
                    Desc = "Deals 150 damage to the card with the least HP."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 200 },
                    Wait = 0,
                    Skill = 2
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 1 },
                    Wait = 0
                }
            );


            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield[0].CurrentTotalAttack);
            Assert.AreEqual(2, b.Rounds.Count);
            Assert.AreEqual(999, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);

            // Put attacker on bf. 1st one deals 150 snipe damage and 200 attack damage, second deals 150 snipe damage
            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(999, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(500, b.DefendPlayer.Battlefield[0].CurrentHP);

            // another defender
            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(998, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(999, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(500, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[1].CurrentHP);

            // another attacker. first defender is sniped for 150, attacked for 200 and sniped for 150 (and dies). 2nd defender is hit for 200, then sniped for 150
            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(998, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(999, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(650, b.DefendPlayer.Battlefield[0].CurrentHP);

            // another defender.
            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(997, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(998, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(650, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[1].CurrentHP);

            // another attacker. 150 + 200 to #1, 150 to #1 + 200 to #2, 150 to #1 = dead
            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(997, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(998, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(800, b.DefendPlayer.Battlefield[0].CurrentHP);

            // another defender
            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(996, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(997, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(800, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[1].CurrentHP);

            // another attacker. 150 + 200 to #1, 150 to #1 + 200 to #2, 150 to #1
            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(996, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(997, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(150, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(800, b.DefendPlayer.Battlefield[1].CurrentHP);
        }

        [TestMethod]
        public void Thunderbolt()
        {
            // Prepare: Card with Evasion. Card that traps. Combat
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1,
                    Type = SkillType.Stun,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 6,
                    AffectValue = 1,
                    AffectValue2 = 100,
                    SkillCategory = 2,
                    Desc = "Deals 1 damage to 1 of the opponent's cards. There is a 100% chance that card will not be able to attack in the next round."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 1
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0
                }
            );


            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.AreEqual(999, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.AreEqual(999, b.DefendPlayer.Battlefield[0].CurrentHP);
        }

        [TestMethod]
        public void Evasion()
        {
            // Prepare: Card with Evasion. Card that traps. Combat
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1,
                    Type = SkillType.Stun,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 6,
                    AffectValue = 1,
                    AffectValue2 = 100,
                    SkillCategory = 2,
                    Desc = "Deals 1 damage to 1 of the opponent's cards. There is a 100% chance that card will not be able to attack in the next round."
                }
            );
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 2,
                    Type = SkillType.Immunity,
                    LanchType = 0,
                    AffectType = 71,
                    SkillCategory = 3,
                    Desc = "Grants immunity to Trap, Seal, and Confusion. Also cannot be frozen or stunned."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 1
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 2
                }
            );


            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.AreEqual(999, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.AreEqual(999, b.DefendPlayer.Battlefield[0].CurrentHP);
        }

        [TestMethod]
        public void GhostStep()
        {
            // Prepare: Card with Evasion. Card that traps. Combat
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1,
                    Type = SkillType.Stun,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 8,
                    AffectValue = 1,
                    AffectValue2 = 100,
                    SkillCategory = 2,
                    Desc = "Deals 1 damage to ALL of the opponent's cards. There is a 100% chance that card will not be able to attack in the next round."
                }
            );
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 2,
                    Type = SkillType.Immunity,
                    LanchType = 0,
                    AffectType = 71,
                    SkillCategory = 3,
                    Desc = "Grants immunity to Trap, Seal, and Confusion. Also cannot be frozen or stunned."
                }
            );
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 3,
                    Type = SkillType.Immunity,
                    LanchType = LaunchType.Runes,
                    AffectType = 60,
                    AffectValue = 2,
                    SkillCategory = 2,
                    Desc = "Gives evasion to all cards on your battlefield.(Grants immunity to Trap, Seal, and Confusion. Also cannot be frozen or stunned)."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 1
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Race = Race.Mountain
                }
            );

            Rune.Runes = new System.Collections.Generic.List<Rune>();
            Rune.Runes.Add(
                new Rune()
                {
                    Id = 1,
                    Name = "Ghost Step",
                    LockSkill1 = 3,
                    LockSkill2 = 3,
                    LockSkill3 = 3,
                    LockSkill4 = 3,
                    LockSkill5 = 3,
                    SkillTimes = 5,
                    SkillConditionColor = 0,
                    SkillConditionCompare = 1,
                    SkillConditionRace = 4,
                    SkillConditionSlide = 1,
                    SkillConditionType = 4,
                    SkillConditionValue = 1
                }
            );


            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Runes.Add(new UserRune() { RuneId = 1, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);

            b.Round();//d
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);

            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.AreEqual(999, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();//d
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.IsFalse(b.DefendPlayer.Battlefield[1].Paralyzed);
            Assert.AreEqual(999, b.DefendPlayer.Battlefield[0].CurrentHP);

            // Ghost Step should be active now.
            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.IsFalse(b.DefendPlayer.Battlefield[1].Paralyzed);
            Assert.AreEqual(998, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(999, b.DefendPlayer.Battlefield[1].CurrentHP);

            // active #2
            b.Round();//d
            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.IsFalse(b.DefendPlayer.Battlefield[1].Paralyzed);
            Assert.AreEqual(997, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(998, b.DefendPlayer.Battlefield[1].CurrentHP);

            // active #3
            b.Round();//d
            b.Round();//a
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.IsFalse(b.DefendPlayer.Battlefield[1].Paralyzed);
            Assert.AreEqual(996, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(997, b.DefendPlayer.Battlefield[1].CurrentHP);

            // active #4
            b.Round();//d
            b.Round();//a
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.IsFalse(b.DefendPlayer.Battlefield[1].Paralyzed);

            // active #5
            b.Round();//d
            b.Round();//a
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.IsFalse(b.DefendPlayer.Battlefield[1].Paralyzed);

            // Not active anymore
            b.Round();
            b.Round();//a
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Paralyzed);
            Assert.IsTrue(b.DefendPlayer.Battlefield[1].Paralyzed);
            Assert.AreEqual(993, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(994, b.DefendPlayer.Battlefield[1].CurrentHP);

        }



        [TestMethod]
        public void DivineProtection()
        {
            //	1124	Divine Protection 8	15	6	0	0	73	400	0	5	Increases the Max HP of yourself and 2 cards near you by 400.	0

            // Prepare: Card with Divine Protection
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1124,
                    Type = SkillType.HPIncrease,
                    LanchType = LaunchType.WhileOnBattlefield,
                    AffectType = 73,
                    AffectValue = 400,
                    SkillCategory = 5,
                    Desc = "Increases the Max HP of yourself and 2 cards near you by 400."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 1124
                }
            );


            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1400, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();//d
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1400, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(400, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1400, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();//a
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(400, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1400, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();//d
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(800, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();//a
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[2].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(800, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();//d
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[2].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(800, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.DefendPlayer.Battlefield[0].CurrentHP);


            b.Round();//a
            Assert.AreEqual(4, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[2].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[3].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[3].CurrentHP);
            Assert.AreEqual(800, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.DefendPlayer.Battlefield[0].CurrentHP);


            b.Round(); //d
            b.Round(); //a
            Assert.AreEqual(5, b.AttackPlayer.Battlefield.Count);
            b.Round(); //d
            b.Round(); //a
            Assert.AreEqual(6, b.AttackPlayer.Battlefield.Count);

            // Increase attack of a defender to test if the bonus hp is properly removed from the card on death.
            b.DefendPlayer.Battlefield[0].CurrentAttack = 3000;

            b.Round();//d
            Assert.AreEqual(5, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[2].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[3].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[3].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[4].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[4].CurrentHP);

            // Attacking something in between does not cause the DP to be applied to the new neighbour
            b.DefendPlayer.Battlefield[0].CurrentAttack = 0;
            b.DefendPlayer.Battlefield[1].CurrentAttack = 3000;

            b.Round(); //a
            b.Round(); //d
            Assert.AreEqual(4, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1400, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[2].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[3].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[3].CurrentHP);

            // Test if no null reference exception is thrown when a card with divine protection dies in a turn where its neighbour also died.
            b.DefendPlayer.Battlefield[0].CurrentAttack = 3000;

            b.Round(); //a
            b.Round(); //d
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[1].CurrentHP);
        }

        [TestMethod]
        public void OriginsGuard()
        {
            //	386	Origins Guard 10	15	6	0	0	25	400	5	5	Increases the HP of ALL other cards on your battlefield by 400.	0

            // Prepare: Card with Divine Protection
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 386,
                    Type = SkillType.HPIncrease,
                    LanchType = LaunchType.WhileOnBattlefield,
                    AffectType = 25,
                    AffectValue = 400,
                    AffectValue2 = 5,
                    SkillCategory = 5,
                    Desc = "Increases the HP of ALL other cards on your battlefield by 400."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 386
                }
            );


            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();//d
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();//a
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1400, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(1400, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();//d
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1400, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(1400, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();//a
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[2].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();//d
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(800, b.AttackPlayer.Battlefield[2].BonusSkillHP);
            Assert.AreEqual(1800, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);


            b.Round();//a
            Assert.AreEqual(4, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[1].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[2].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(1200, b.AttackPlayer.Battlefield[3].BonusSkillHP);
            Assert.AreEqual(2200, b.AttackPlayer.Battlefield[3].CurrentHP);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);
        }



        [TestMethod]
        public void PowerSource()
        {
            //	1	336	Power Source 10	14	6	0	0	24	200	5	5	Increases the Attack of ALL other cards on your battlefield by 200.	0

            // Prepare: Card with Power Source
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 336,
                    Type = SkillType.AttackIncrease,
                    LanchType = LaunchType.WhileOnBattlefield,
                    AffectType = 24,
                    AffectValue = 200,
                    AffectValue2 = 5,
                    SkillCategory = 5,
                    Desc = "Increases the Attack of ALL other cards on your battlefield by 200."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 100000 }, // more hp or else we will have won before all cards in play
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 336
                }
            );


            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);

            b.Round();//d
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].CurrentTotalAttack);

            b.Round();//a
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(200, b.AttackPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(200, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);
            Assert.AreEqual(200, b.AttackPlayer.Battlefield[1].BonusSkillAttack);
            Assert.AreEqual(200, b.AttackPlayer.Battlefield[1].CurrentTotalAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].CurrentTotalAttack);

            b.Round();//d
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(200, b.AttackPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(200, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);
            Assert.AreEqual(200, b.AttackPlayer.Battlefield[1].BonusSkillAttack);
            Assert.AreEqual(200, b.AttackPlayer.Battlefield[1].CurrentTotalAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].CurrentTotalAttack);

            b.Round();//a
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[1].BonusSkillAttack);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[1].CurrentTotalAttack);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[2].BonusSkillAttack);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[2].CurrentTotalAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].CurrentTotalAttack);

            b.Round();//d
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[1].BonusSkillAttack);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[1].CurrentTotalAttack);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[2].BonusSkillAttack);
            Assert.AreEqual(400, b.AttackPlayer.Battlefield[2].CurrentTotalAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].CurrentTotalAttack);


            b.Round();//a
            Assert.AreEqual(4, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(600, b.AttackPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(600, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);
            Assert.AreEqual(600, b.AttackPlayer.Battlefield[1].BonusSkillAttack);
            Assert.AreEqual(600, b.AttackPlayer.Battlefield[1].CurrentTotalAttack);
            Assert.AreEqual(600, b.AttackPlayer.Battlefield[2].BonusSkillAttack);
            Assert.AreEqual(600, b.AttackPlayer.Battlefield[2].CurrentTotalAttack);
            Assert.AreEqual(600, b.AttackPlayer.Battlefield[3].BonusSkillAttack);
            Assert.AreEqual(600, b.AttackPlayer.Battlefield[3].CurrentTotalAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].BonusSkillAttack);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].CurrentTotalAttack);
        }


        [TestMethod]
        public void FireGod()
        {
            //	645	Fire God 10	                28	4	0	0	46	200	0	2	Burns every card on the opponent's battlefield, causing them to all lose 200 HP after each move.	0
            //	799	Desperation:  Fire God 10	28	9	0	0	53	645	0	2	After being eliminated, casts Fire God 10: Burns every card on the opponent's battlefield, causing them to all lose 200 HP after each move.	0
            //	960	Quick Strike: Fire God 10	28	10	0	0	59	645	0	2	Upon entering the battlefield, casts Fire God 10: Burns every card on the opponent's battlefield, causing them to lose 200 HP after each move.	0

            // Prepare
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 645,
                    Name = "Fire God 10",
                    Type = SkillType.Burn,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 46,
                    AffectValue = 200,
                    AffectValue2 = 0,
                    SkillCategory = 2,
                    Desc = "Burns every card on the opponent's battlefield, causing them to all lose 200 HP after each move."
                }
            );
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 799,
                    Name = "Desperation: Fire God 10",
                    Type = SkillType.Burn,
                    LanchType = LaunchType.OnDeath,
                    AffectType = 53,
                    AffectValue = 645,
                    AffectValue2 = 0,
                    SkillCategory = 2,
                    Desc = "After being eliminated, casts Fire God 10: Burns every card on the opponent's battlefield, causing them to all lose 200 HP after each move."
                }
            );
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 960,
                    Name = "Quick Strike: Fire God 10",
                    Type = SkillType.Burn,
                    LanchType = LaunchType.OnEnterBattlefield,
                    AffectType = 59,
                    AffectValue = 645,
                    AffectValue2 = 0,
                    SkillCategory = 2,
                    Desc = "Upon entering the battlefield, casts Fire God 10: Burns every card on the opponent's battlefield, causing them to lose 200 HP after each move."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 700 }, // more hp or else we will have won before all cards in play
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 645
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 700 }, // more hp or else we will have won before all cards in play
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 799
                }
            );


            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0, SkillNew = 960 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(700, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(false, b.AttackPlayer.Battlefield[0].Burned);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[0].Burns.Count);

            b.Round();//d
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(700, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(true, b.AttackPlayer.Battlefield[0].Burned);
            Assert.AreEqual(1, b.AttackPlayer.Battlefield[0].Burns.Count);
            Assert.AreEqual(700, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(false, b.DefendPlayer.Battlefield[0].Burned);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield[0].Burns.Count);

            b.Round(); //a
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(500, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(true, b.AttackPlayer.Battlefield[0].Burned);
            Assert.AreEqual(1, b.AttackPlayer.Battlefield[0].Burns.Count);
            Assert.AreEqual(700, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(false, b.AttackPlayer.Battlefield[1].Burned);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[1].Burns.Count);

            Assert.AreEqual(700, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(true, b.DefendPlayer.Battlefield[0].Burned);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield[0].Burns.Count);

            b.Round(); //d
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(500, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(true, b.AttackPlayer.Battlefield[0].Burned);
            Assert.AreEqual(1, b.AttackPlayer.Battlefield[0].Burns.Count);
            Assert.AreEqual(700, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(false, b.AttackPlayer.Battlefield[1].Burned);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[1].Burns.Count);

            Assert.AreEqual(300, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(true, b.DefendPlayer.Battlefield[0].Burned);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield[0].Burns.Count);


            b.Round(); //a
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(300, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(true, b.AttackPlayer.Battlefield[0].Burned);
            Assert.AreEqual(1, b.AttackPlayer.Battlefield[0].Burns.Count);
            Assert.AreEqual(700, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(false, b.AttackPlayer.Battlefield[1].Burned);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[1].Burns.Count);
            Assert.AreEqual(700, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(false, b.AttackPlayer.Battlefield[2].Burned);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[2].Burns.Count);

            Assert.AreEqual(300, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(true, b.DefendPlayer.Battlefield[0].Burned);
            Assert.AreEqual(3, b.DefendPlayer.Battlefield[0].Burns.Count);


            b.Round(); //d
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(300, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(true, b.AttackPlayer.Battlefield[0].Burned);
            Assert.AreEqual(2, b.AttackPlayer.Battlefield[0].Burns.Count);
            Assert.AreEqual(700, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(true, b.AttackPlayer.Battlefield[1].Burned);
            Assert.AreEqual(1, b.AttackPlayer.Battlefield[1].Burns.Count);
            Assert.AreEqual(700, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(true, b.AttackPlayer.Battlefield[2].Burned);
            Assert.AreEqual(1, b.AttackPlayer.Battlefield[2].Burns.Count);
        }

        [TestMethod]
        public void ValidateSkillKillNotDefendingAnymore()
        {
            // A card that is killed by a skill should not be defending anymore. Verify that this is really the case.
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 2,
                    Type = SkillType.Snipe,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 22,
                    AffectValue = 5000,
                    SkillCategory = 2,
                    Desc = "Deals 5000 damage to the card with the least HP."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 200 },
                    Wait = 0,
                    Skill = 2
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 1 },
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            const int MAX_HP = 17000; // for level 101

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 200, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield[0].CurrentTotalAttack);
            Assert.AreEqual(2, b.Rounds.Count);
            Assert.AreEqual(999, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 200, b.DefendPlayer.CurrentHP);

            // Snipe kills defender. Opponent should receive 200 damage.
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(999, b.AttackPlayer.Battlefield[0].CurrentHP);

            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 400, b.DefendPlayer.CurrentHP);
        }

        [TestMethod]
        public void Reanimation()
        {
            // A card that is killed by a skill should not be defending anymore. Verify that this is really the case.
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 597,
                    Type = SkillType.Resurrection,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 39,
                    AffectValue = 0,
                    SkillCategory = 2,
                    Desc = "Summons a card that does not have Reanimation from your cemetery straight into the battlefield."
                }
            );
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 689,
                    Type = SkillType.Immunity,
                    LanchType = LaunchType.OnSkillHit,
                    AffectType = 52,
                    AffectValue = 0,
                    SkillCategory = 3,
                    Desc = "Provides immunity to all spells and abilities."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 100 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 597
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 100 },
                    AttackArray = new int[] { 100 },
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);

            b.AttackPlayer.Deck.Add(new BattleCard(new UserCard() { CardId = 1, Level = 0 }));

            b.Round();//d
            Assert.AreEqual(0, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);

            b.Round();//a
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);

            b.AttackPlayer.Deck.Add(new BattleCard(new UserCard() { CardId = 1, Level = 0 }));

            b.Round();//d
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);

            b.Round();//a
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
        }


        [TestMethod]
        public void DesperationReanimation()
        {
            // A card that is killed by a skill should not be defending anymore. Verify that this is really the case.
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 597,
                    Type = SkillType.Resurrection,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 39,
                    AffectValue = 0,
                    SkillCategory = 2,
                    Desc = "Summons a card that does not have Reanimation from your cemetery straight into the battlefield."
                }
            );
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1115,
                    Type = SkillType.Resurrection,
                    LanchType = LaunchType.OnDeath,
                    AffectType = 53,
                    AffectValue = 597,
                    SkillCategory = 2,
                    Desc = "After being eliminated, casts Reanimation:Summons a card that does not have Reanimation from your cemetery straight into the battlefield."
                }
            );
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1116,
                    Type = SkillType.Resurrection,
                    LanchType = LaunchType.OnEnterBattlefield,
                    AffectType = 59,
                    AffectValue = 597,
                    SkillCategory = 2,
                    Desc = "Upon entering the battlefield, casts Reanimation:Summons a card that does not have Reanimation from your cemetery straight into the battlefield."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 100 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 597
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 100 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 1115
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 3,
                    HpArray = new int[] { 100 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 1116
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 4,
                    HpArray = new int[] { 100 },
                    AttackArray = new int[] { 100 },
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 4, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 4, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);

            b.AttackPlayer.Deck.Add(new BattleCard(new UserCard() { CardId = 2, Level = 0 }));

            b.Round();//d
            Assert.AreEqual(0, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);

            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);

            // Defender kills card with Desperation: Reanimation. The current card in GY should be put back on BF
            b.Round();//d
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);

            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
        }

        [TestMethod]
        public void FrostShock()
        {
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1281,
                    Type = SkillType.FrostShock,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 96,
                    AffectValue = 50,
                    AffectValue2 = 10,
                    SkillCategory = 2,
                    Desc = "Deals 50 + 10x (number of cards on opponent's battlefield) to all opponent's cards. The cards receiving damage will have a 50% chance to get frozen and can't attack or use skills in the next turn."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 1281
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0
                }
            );


            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Frozen);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            //Assert.IsTrue(b.DefendPlayer.Battlefield[0].Frozen); // can't properly test the freezing right now, since I'm not able to influence the random generator in a test case.
            // dmg 50 + 1 * 10 = 60
            Assert.AreEqual(940, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Frozen);
            Assert.AreEqual(940, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[1].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            //Assert.IsTrue(b.DefendPlayer.Battlefield[0].Frozen);
            // dmg 50 + 2 * 10 = 70
            Assert.AreEqual(870, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(930, b.DefendPlayer.Battlefield[1].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(3, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Frozen);
            Assert.AreEqual(870, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(930, b.DefendPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[2].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(3, b.DefendPlayer.Battlefield.Count);
            //Assert.IsTrue(b.DefendPlayer.Battlefield[0].Frozen);
            // dmg 50 + 2 * 10 = 80
            Assert.AreEqual(790, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(850, b.DefendPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(920, b.DefendPlayer.Battlefield[2].CurrentHP);
        }

        [TestMethod]
        public void ShieldOfEarth()
        {
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1331,
                    Type = SkillType.ShieldOfEarth,
                    LanchType = LaunchType.AfterDefend,
                    LanchCondition = LaunchCondition.ReceivedDamage,
                    AffectType = 100,
                    AffectValue = 0,
                    AffectValue2 = 10,
                    SkillCategory = 1,
                    Desc = "When this card receives physical attack, the attacker would be stunned and can't attack or use skills in the next turn."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 1331
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 1 },
                    Wait = 0
                }
            );


            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            // def_1 has hit atk_1 -> def_1 is now stunned
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Stunned);
            Assert.AreEqual(999, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Stunned);
            Assert.AreEqual(999, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Stunned);
            Assert.AreEqual(999, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Stunned);
            Assert.AreEqual(999, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Stunned);
            Assert.AreEqual(998, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Stunned);
            Assert.AreEqual(998, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.IsFalse(b.DefendPlayer.Battlefield[0].Stunned);
            Assert.AreEqual(998, b.AttackPlayer.Battlefield[0].CurrentHP);
        }

        [TestMethod]
        public void Inspire()
        {
            Skill.ClearCache();
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1294,
                    Type = SkillType.AttackIncrease,
                    LanchType = LaunchType.BeforeAttack,
                    LanchCondition = LaunchCondition.Always,
                    AffectType = 97,
                    AffectValue = 80,
                    AffectValue2 = 0,
                    SkillCategory = 1,
                    Desc = "This card's attack would be increased by 80x (number of cards on your battlefield) when attacking."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 1294
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0
                }
            );


            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            const int MAX_HP = 17000;
            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 80, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 80, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 80 - 160, b.DefendPlayer.CurrentHP);
            Assert.AreEqual(1000 - 160, b.DefendPlayer.Battlefield[0].CurrentHP);

        }


        [TestMethod]
        public void Guard_Single()
        {
            // Cards with the Guard skill will block damage, but no more then their current HP
            // 1	810	Guard	34	14	0	0	54	0	0	3	If your Hero is attacked, the Guard will take the HP damage for your Hero.	0
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 810,
                    Type = SkillType.Guard,
                    LanchType = LaunchType.OnHeroDamaged,
                    LanchCondition = LaunchCondition.Always,
                    AffectType = 54,
                    AffectValue = 0,
                    AffectValue2 = 0,
                    SkillCategory = 3,
                    Desc = "If your Hero is attacked, the Guard will take the HP damage for your Hero."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Skill = 810,
                    Wait = 0
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 800 },
                    Skill = 810,
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            const int MAX_HP = 17000; // for level 101

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            // Sneak in another card with attack - do it here to avoid random shuffles to intervere.
            b.AttackPlayer.Deck.Add(new BattleCard(new UserCard() { CardId = 2, Level = 0 }));

            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(200, b.DefendPlayer.Battlefield[0].CurrentHP); // the damage from the 2nd attacker should be blocked by this defender
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(200, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            // Attacker hits again. Defender should be dead due to to guarding the unblocked attacker, the defender should have received the overkill damage.
            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 600, b.DefendPlayer.CurrentHP);
        }


        [TestMethod]
        public void Guard_Multiple()
        {
            // Cards with the Guard skill will block damage, but no more then their current HP
            // If there are multiple guards, they should all block damage up until their total hp.
            // 1	810	Guard	34	14	0	0	54	0	0	3	If your Hero is attacked, the Guard will take the HP damage for your Hero.	0
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 810,
                    Type = SkillType.Guard,
                    LanchType = LaunchType.OnHeroDamaged,
                    LanchCondition = LaunchCondition.Always,
                    AffectType = 54,
                    AffectValue = 0,
                    AffectValue2 = 0,
                    SkillCategory = 3,
                    Desc = "If your Hero is attacked, the Guard will take the HP damage for your Hero."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Skill = 810,
                    Wait = 0
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 800 },
                    Skill = 810,
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            const int MAX_HP = 17000; // for level 101

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            // Sneak in another card with attack - do it here to avoid random shuffles to intervere.
            b.AttackPlayer.Deck.Add(new BattleCard(new UserCard() { CardId = 2, Level = 0 }));

            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(200, b.DefendPlayer.Battlefield[0].CurrentHP); // the damage from the 3nd attacker should be blocked by this defender
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(200, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            // Attacker hits again. Defender should be dead due to to guarding the unblocked attacker, the defender should have received the overkill damage.
            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(400, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(400, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 400, b.DefendPlayer.CurrentHP);
        }





        [TestMethod]
        public void Silence_Force_Bug()
        {
            // If a card with (Mountain) Force is silenced, and then killed, its buff will not be removed from the stack. 
            // If it gets ressurected, it will add its buff another time.
            // The buff will never be applied to itself
            // 1	310	Mountain Force 4	14	6	0	0	24	100	4	5	Increases the Attack of all other Mountain cards on your battlefield by 100.	3
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 312,
                    Type = SkillType.AttackIncrease,
                    LanchType = LaunchType.WhileOnBattlefield,
                    LanchCondition = LaunchCondition.Always,
                    AffectType = 24,
                    AffectValue = 100,
                    AffectValue2 = 4,
                    SkillCategory = 5,
                    Desc = "Increases the Attack of all other Mountain cards on your battlefield by 100."
                }
            );
            /*
      {
        "SkillId": 1259,
        "Name": "Silence",
        "Type": 46,
        "LanchType": 4,
        "LanchCondition": 0,
        "LanchConditionValue": 0,
        "AffectType": 89,
        "AffectValue": 2,
        "AffectValue2": 0,
        "SkillCategory": 2,
        "Desc": "Nullifies all skills of the card directly across every turn. This skill doesn't work against Resistance or Demons."
      },*/
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1259,
                    Type = SkillType.Silence,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    LanchCondition = LaunchCondition.Always,
                    AffectType = 89,
                    AffectValue = 2,
                    AffectValue2 = 0,
                    SkillCategory = 2,
                    Desc = "Nullifies all skills of the card directly across every turn. This skill doesn't work against Resistance or Demons."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Skill = 312,
                    Race = Race.Mountain,
                    Wait = 0
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 10000 },
                    AttackArray = new int[] { 1000 },
                    Skill = 1259,
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            const int MAX_HP = 17000; // for level 101

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);
            Assert.AreEqual(0, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);

            b.Round();
            Assert.AreEqual(0, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(10000, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            // The new attacker should now get the buff from the previous attacker
            Assert.AreEqual(100, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);
            Assert.AreEqual(9900, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);
            
        }

        [TestMethod]
        public void DivineShield()
        {
            //  {
            //"SkillId": 1269,
            //"Name": "Divine Shield",
            //"Type": 53,
            //"LanchType": 5,
            //"LanchCondition": 3,
            //"LanchConditionValue": 0,
            //"AffectType": 94,
            //"AffectValue": 999999,
            //"AffectValue2": 0,
            //"SkillCategory": 3,
            //"Desc": "The first physical attack against this card will be nullified."
            //  },
            
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1269,
                    Type = SkillType.DivineShield,
                    LanchType = LaunchType.BeforeDefend,
                    LanchCondition = LaunchCondition.ReceivedDamage,
                    AffectType = 94,
                    AffectValue = 999999,
                    AffectValue2 = 0,
                    SkillCategory = 3,
                    Desc = "The first physical attack against this card will be nullified."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 2000 },
                    AttackArray = new int[] { 0 },
                    Skill = 1269,
                    Wait = 0
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 1000 },
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(2000, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            // First attack should not hurt the card
            Assert.AreEqual(2000, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(2000, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            // Second attack should hurt the card as normal
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);

        }


        [TestMethod]
        public void DreadRoar()
        {
            /*
{
"SkillId": 1236,
"Name": "Dread Roar",
"Type": 44,
"LanchType": 4,
"LanchCondition": 0,
"LanchConditionValue": 0,
"AffectType": 84,
"AffectValue": 10,
"AffectValue2": 0,
"SkillCategory": 4,
"Desc": "Every turn halves all cards' Attack on opponent's battlefield for 1 round. Ignores Immunity. This skill doesn't work against Demons."
},
*/
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1236,
                    Type = SkillType.Roar,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    LanchCondition = LaunchCondition.Always,
                    AffectType = 84,
                    AffectValue = 10,
                    AffectValue2 = 0,
                    SkillCategory = 4,
                    Desc = "Every turn halves all cards' Attack on opponent's battlefield for 1 round. Ignores Immunity. This skill doesn't work against Demons."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 1000 },
                    Wait = 0
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Skill = 1236,
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);
            
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            // His attack should still be 1000
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            // His attack should still be 1000
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentTotalAttack);
            // But the defender's card should have only received 500 damage
            Assert.AreEqual(500, b.DefendPlayer.Battlefield[0].CurrentHP);

        }


        [TestMethod]
        public void Spine()
        {
            /*
{
"SkillId": 1494,
"Name": "spine 2",
"Type": 3,
"LanchType": 3,
"LanchCondition": 3,
"LanchConditionValue": 0,
"AffectType": 3,
"AffectValue": 0,
"AffectValue2": 20,
"SkillCategory": 1,
"Desc": "After receiving physical damage, cause 20% of received damage to the attacker."
},
*/
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1494,
                    Type = SkillType.CounterAttack,
                    LanchType = LaunchType.AfterDefend,
                    LanchCondition = LaunchCondition.ReceivedDamage,
                    AffectType = 3,
                    AffectValue = 0,
                    AffectValue2 = 20,
                    SkillCategory = 1,
                    Desc = "After receiving physical damage, cause 20% of received damage to the attacker."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Skill = 1494,
                    Wait = 0
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 100 },
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(900, b.AttackPlayer.Battlefield[0].CurrentHP);
            // the other card should receive 20% of the damage
            Assert.AreEqual(980, b.DefendPlayer.Battlefield[0].CurrentHP);

        }

        [TestMethod]
        public void Apocalypse()
        {
            /*
{
"SkillId": 2004,
"Name": "Apocalypse",
"Type": 0,
"LanchType": 5,
"LanchCondition": 0,
"LanchConditionValue": 0,
"AffectType": 122,
"AffectValue": "1456_645",
"AffectValue2": 0,
"SkillCategory": 2,
"Desc": "Deal 150+60x(enemy cards in the battlefield) to all enemy cards and burn them. Every enemy card loses 200 HP when it ends the activation."
},

            
      {
        "SkillId": 645,
        "Name": "Fire God 10",
        "Type": 28,
        "LanchType": 4,
        "LanchCondition": 0,
        "LanchConditionValue": 0,
        "AffectType": 46,
        "AffectValue": 200,
        "AffectValue2": 0,
        "SkillCategory": 2,
        "Desc": "Burns all the cards on the opponent's battlefield. The enemy burnt will lose 200 HP every time after its action.",
        "EvoRank": 0
      },
      {
        "SkillId": 1456,
        "Name": "Asura's Flame 5",
        "Type": 66,
        "LanchType": 4,
        "LanchCondition": 0,
        "LanchConditionValue": 0,
        "AffectType": 121,
        "AffectValue": 150,
        "AffectValue2": 60,
        "SkillCategory": 2,
        "Desc": "Deal magic damage to all enemy cards. The damage = 150 + 60 x (number of all enemy cards in the battlefield)"
      },

*/
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1456,
                    Type = SkillType.Asura,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    LanchCondition = 0,
                    AffectType = 121,
                    AffectValue = 150,
                    AffectValue2 = 60,
                    SkillCategory = 2,
                    Desc = "Deal magic damage to all enemy cards. The damage = 150 + 60 x (number of all enemy cards in the battlefield)"
                }
            );
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 645,
                    Type = SkillType.Burn,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    LanchCondition = 0,
                    AffectType = 46,
                    AffectValue = 200,
                    AffectValue2 = 0,
                    SkillCategory = 2,
                    Desc = "Burns all the cards on the opponent's battlefield. The enemy burnt will lose 200 HP every time after its action."
                }
            );
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 2004,
                    Type = 0,
                    LanchType = LaunchType.BeforeDefend,
                    LanchCondition = 0,
                    AffectType = 122,
                    AffectValue = 1456,
                    AffectValueExtra = 645,
                    AffectValue2 = 0,
                    SkillCategory = 2,
                    Desc = "Deal 150+60x(enemy cards in the battlefield) to all enemy cards and burn them. Every enemy card loses 200 HP when it ends the activation."
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Skill = 2004,
                    Wait = 0
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            // 1000 - 150 - 60 = 790
            Assert.AreEqual(790, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Burned);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(590, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[1].CurrentHP);
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Burned);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            // 590 - 150 - 120 = 320
            Assert.AreEqual(320, b.DefendPlayer.Battlefield[0].CurrentHP);
            // 1000 - 150 - 120 = 730
            Assert.AreEqual(730, b.DefendPlayer.Battlefield[1].CurrentHP);
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Burned);
            Assert.IsTrue(b.DefendPlayer.Battlefield[1].Burned);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(3, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(120, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(530, b.DefendPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[2].CurrentHP);
            Assert.IsTrue(b.DefendPlayer.Battlefield[0].Burned);

        }


        [TestMethod]
        public void DyingStrike()
        {
            /*
            {
"SkillId": 1330,
"Name": "Dying Strike",
"Type": 14,
"LanchType": 9,
"LanchCondition": 0,
"LanchConditionValue": 0,
"AffectType": 111,
"AffectValue": 200,
"AffectValue2": 0,
"SkillCategory": 2,
"Desc": "When receiving fatal damage, this card will cause 200% of base attack power to the opponent card. This damage will not be affected by Ice Shield,Parry or Dodge. This card can only cast this skill once in a battle. "
},

    */
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1330,
                    Type = SkillType.AttackIncrease,
                    LanchType = LaunchType.OnDeath,
                    LanchCondition = 0,
                    AffectType = 111,
                    AffectValue = 200,
                    AffectValue2 = 0,
                    SkillCategory = 2,
                    Desc = "When receiving fatal damage, this card will cause 200% of base attack power to the opponent card. This damage will not be affected by Ice Shield,Parry or Dodge. This card can only cast this skill once in a battle. "
                }
            );

            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 300 },
                    Skill = 1330,
                    Wait = 0
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 1000 },
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(0, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            // Should have received 300 * 2 = 600 damage
            Assert.AreEqual(400, b.DefendPlayer.Battlefield[0].CurrentHP);
        }



        [TestMethod]
        public void DianasProtection()
        {
            /*
            
{
"SkillId": 1448,
"Name": "Diana's Protection 7",
"Type": 65,
"LanchType": 4,
"LanchCondition": 0,
"LanchConditionValue": 0,
"AffectType": 120,
"AffectValue": 25,
"AffectValue2": 0,
"SkillCategory": 4,
"Desc": "Restore 25% HP to all your cards."
},
*/
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1448,
                    Type = SkillType.PercentageHeal,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 120,
                    AffectValue = 25,
                    SkillCategory = 4,
                    Desc = "Restore 25% HP to all your cards."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 400 },
                    Wait = 0,
                    Skill = 1448
                }
            );

            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP); // ensure not healed above its max hp
            
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(600, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            // Should receive 500 healing
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(600, b.DefendPlayer.Battlefield[0].CurrentHP);
            
            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(600, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(850, b.DefendPlayer.Battlefield[0].CurrentHP);
        }


        [TestMethod]
        public void WaterShield()
        {
            /*
{
"SkillId": 1532,
"Name": "Water Shield 10",
"Type": 70,
"LanchType": 5,
"LanchCondition": 3,
"LanchConditionValue": 0,
"AffectType": 123,
"AffectValue": 200,
"AffectValue2": 1000,
"SkillCategory": 3,
"Desc": "This card will only receive 200 damage from each physical attack. The additional damage will restore your hero's HP by 1000 at most."
},
*/
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1532,
                    Type = SkillType.WaterShield,
                    LanchType = LaunchType.BeforeDefend,
                    LanchCondition = LaunchCondition.ReceivedDamage,
                    AffectType = 123,
                    AffectValue = 200,
                    AffectValue2 = 1000,
                    SkillCategory = 3,
                    Desc = "This card will only receive 200 damage from each physical attack. The additional damage will restore your hero's HP by 1000 at most."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 1500 },
                    Wait = 0,
                    Skill = 1532
                }
            );

            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            const int MAX_HP = 24840;

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.AttackPlayer.CurrentHP = 1000;
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);

            Assert.AreEqual(1000, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 1500, b.DefendPlayer.CurrentHP);

            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);

            Assert.AreEqual(2000, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 1500, b.DefendPlayer.CurrentHP);

            Assert.AreEqual(800, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);
            
        }


        [TestMethod]
        public void Salvo()
        {
            /*
{
"SkillId": 1549,
"Name": "Salvo 5",
"Type": 12,
"LanchType": 4,
"LanchCondition": 0,
"LanchConditionValue": 0,
"AffectType": 22,
"AffectValue": 120,
"AffectValue2": 10,
"SkillCategory": 1,
"Desc": "Deal 120 damage to all enemy cards."
},
},
*/
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1549,
                    Type = SkillType.Snipe,
                    LanchType = LaunchType.OnSkillExecutePhase,
                    AffectType = 22,
                    AffectValue = 120,
                    AffectValue2 = 10,
                    SkillCategory = 1,
                    Desc = "Deal 120 damage to all enemy cards."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                    Skill = 1549
                }
            );

            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);            
            Assert.AreEqual(880, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(880, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(760, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();
            Assert.AreEqual(2, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(640, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(760, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(760, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[1].CurrentHP);


            b.Round();
            Assert.AreEqual(3, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(640, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(760, b.AttackPlayer.Battlefield[1].CurrentHP);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[2].CurrentHP);
            Assert.AreEqual(400, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(640, b.DefendPlayer.Battlefield[1].CurrentHP);

        }



        [TestMethod]
        public void Resurrection()
        {
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 407,
                    Type = SkillType.Resurrection,
                    LanchType = LaunchType.AfterDeath,
                    AffectType = 27,
                    AffectValue = 100,
                    SkillCategory = 2,
                    Desc = "When this card dies, there is a 100% chance it is returned to your hand. If your hand is full, it will be returned to the deck."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 100 },
                    AttackArray = new int[] { 100 },
                    Wait = 0,
                    Skill = 407
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);

            b.Round();//d
            Assert.AreEqual(0, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Cemetary.Count);
            Assert.AreEqual(1, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);

            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Cemetary.Count);
            Assert.AreEqual(0, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.AttackPlayer.Deck.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(1, b.DefendPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Deck.Count);

            b.Round();//d
            Assert.AreEqual(0, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Cemetary.Count);
            Assert.AreEqual(1, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.AttackPlayer.Deck.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(0, b.DefendPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Deck.Count);

            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Cemetary.Count);
            Assert.AreEqual(0, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.AttackPlayer.Deck.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(1, b.DefendPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Deck.Count);
        }

        /// <summary>
        /// Test to verify that a card with multiple resurrect skills (due to Dirt rune) is not added to your hand/deck twice
        /// </summary>
        [TestMethod]
        public void Bug_DoubleResurrection()
        {
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 407,
                    Type = SkillType.Resurrection,
                    LanchType = LaunchType.AfterDeath,
                    AffectType = 27,
                    AffectValue = 100,
                    SkillCategory = 2,
                    Desc = "When this card dies, there is a 100% chance it is returned to your hand. If your hand is full, it will be returned to the deck."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 100 },
                    AttackArray = new int[] { 100 },
                    Wait = 0,
                    Skill = 407,
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0, SkillNew = 407 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0, SkillNew = 407 });

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);

            b.Round();//d
            Assert.AreEqual(0, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Cemetary.Count);
            Assert.AreEqual(1, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);

            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Cemetary.Count);
            Assert.AreEqual(0, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.AttackPlayer.Deck.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(1, b.DefendPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Deck.Count);

            b.Round();//d
            Assert.AreEqual(0, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Cemetary.Count);
            Assert.AreEqual(1, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.AttackPlayer.Deck.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(0, b.DefendPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Deck.Count);

            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.AttackPlayer.Cemetary.Count);
            Assert.AreEqual(0, b.AttackPlayer.Hand.Count);
            Assert.AreEqual(0, b.AttackPlayer.Deck.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(1, b.DefendPlayer.Hand.Count);
            Assert.AreEqual(0, b.DefendPlayer.Deck.Count);
        }

        
        [TestMethod]
        public void DoubleAttack()
        {
            Skill.Skills = new System.Collections.Generic.List<Skill>();
            Skill.Skills.Add(
                new Skill()
                {
                    SkillId = 1490,
                    Name = "Double Attack",
                    Type = 0,
                    LanchType = 0,
                    AffectType = 131,
                    AffectValue = 0,
                    SkillCategory = 2,
                    Desc = "This card can deal physical attack twice to the card in each round."
                }
            );
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 100 },
                    AttackArray = new int[] { 100 },
                    Wait = 0,
                    Skill = 1490,
                }
            );
            Card.Cards.Add(
                new Card()
                {
                    Id = 2,
                    HpArray = new int[] { 300 },
                    AttackArray = new int[] { 0 },
                    Wait = 0,
                }
            );

            const int MAX_HP = 24840;

            User attacker = new User();
            attacker.Level = 150;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 150;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 2, Level = 0 });

            Battle b = new Battle(attacker, defender);
            b.Round();
            
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP - 100, b.DefendPlayer.CurrentHP); // double attack should not hit a player twice
            Assert.AreEqual(100, b.AttackPlayer.Battlefield[0].CurrentHP);

            b.Round();//d
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP - 100, b.DefendPlayer.CurrentHP);
            Assert.AreEqual(100, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(300, b.DefendPlayer.Battlefield[0].CurrentHP);

            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP - 100, b.DefendPlayer.CurrentHP);
            Assert.AreEqual(100, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(100, b.DefendPlayer.Battlefield[0].CurrentHP); // should have lost 2* 100 hp

            b.Round();//d (don't care, nothing should happen)
            b.Round();//a
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Cemetary.Count);
            Assert.AreEqual(MAX_HP - 100, b.DefendPlayer.CurrentHP); // defending player shouldn't have lost health - the 2nd attack will not trigger if the defending card is dead
            Assert.AreEqual(100, b.AttackPlayer.Battlefield[0].CurrentHP);
        }
    }
}
