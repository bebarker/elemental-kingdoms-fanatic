﻿using System;
using System.Collections.Generic;
using System.Text;
using ElementalKingdomsConsole.Properties;

namespace ElementalKingdomsConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Settings.Default.UpgradeOldSettings)
            {
                Settings.Default.Upgrade();
                Settings.Default.UpgradeOldSettings = false;
                Settings.Default.Save();
                try
                {
                    System.IO.File.Delete(@"cache/results.json");
                }
                catch (Exception)
                {
                }
            }

            if (String.IsNullOrEmpty(Settings.Default.DeviceId))
            {
                Settings.Default.DeviceId = ElementalKingdoms.server.ServerCommunicator.GenerateDeviceId();
                Settings.Default.Save();
            }

            /*
            //Get a pointer to the forground window.  The idea here is that
            //IF the user is starting our application from an existing console
            //shell, that shell will be the uppermost window.  We'll get it
            //and attach to it
            IntPtr ptr = GetForegroundWindow();

            int u;

            GetWindowThreadProcessId(ptr, out u);

            Process process = Process.GetProcessById(u);*/

            //            bool existingConsole = true; //process.ProcessName == "cmd";

            //            if (existingConsole)    //Is the uppermost window a cmd process?
            //            {
            //                //AttachConsole(process.Id);
            //                AttachConsole(-1);

            //#if DEBUG
            //                //we have a console to attach to ..
            //                Console.WriteLine("hello. It looks like you started me from an existing console.");
            //#endif
            //            }
            //            else
            //            {
            //                //no console AND we're in console mode ... create a new console.

            //                AllocConsole();
            //#if DEBUG
            //                Console.WriteLine(@"hello. It looks like you double clicked me to start
            //                   AND you want console mode.  Here's a new console.");
            //#endif
            //            }//*/

            ConsoleUI console = new ConsoleUI();

            // version info
            Console.WriteLine("{0} {1}", console.About.AssemblyTitle, console.About.AssemblyVersion);

            var options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                switch (options.Mode)
                {
                    case "league":
                    case "foh":
                        console.RunLeague(options);
                        break;
                    case "di":
                        console.RunDemon(options);
                        break;
                    default:
                        Console.WriteLine("Unknown mode: {0}", options.Mode);
                        break;
                }
            }
            else
            {
                // Usage has already been printed. Just ignore this.
            }

            //if (!existingConsole)
            //{
            //Console.WriteLine("press any key to continue ...");
            //Console.ReadKey();
            //Console.ReadLine();
            //}
            //else
            //{
            //    System.Windows.Forms.SendKeys.SendWait("{ENTER}");
            //}

            //FreeConsole();
        }
    }
}
