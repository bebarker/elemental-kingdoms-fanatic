﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class MapStage
    {
        public static List<MapStage> AllMapStages = new List<MapStage>();

        [Newtonsoft.Json.JsonProperty("MapStageId")]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public int EverydayReward { get; set; }
        public int Rank { get; set; }
        public int MazeCount { get; set; }
        public int NeedStar { get; set; }
        public int Prev { get; set; }
        public int Next { get; set; }
        public List<MapStageDetail> MapStageDetails { get; set; }
    }
}
