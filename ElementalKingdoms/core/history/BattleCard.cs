﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core.history
{
    public enum Location
    {
        None,
        Deck,
        Hand,
        Battlefield,
        Cemetary,
    }

    public class BattleCard
    {
        public UserCard Card { get; private set; }
        public string Name { get { return Card.Name; } }

        public Location Location { get; set; }
        public int LocationIndex { get; set; }

        public int CurrentWaitTime { get; set; }
        public int CurrentHP { get; set; }
        public int CurrentTotalAttack { get; set; }

        public bool Burned { get; set; }
        public bool Frozen { get; set; }
        public bool Paralyzed { get; set; }
        public bool Trapped { get; set; }
        public bool Confused { get; set; }
        public bool Stunned { get; set; }
        public bool Silenced { get; set; }
        public bool Poisoned { get; set; }
        public bool Laceration { get; set; }

        public List<int> Poisons = new List<int>();
        public Dictionary<string, int> Burns = new Dictionary<string, int>();

        public BattleCard(UserCard c)
        {
            Card = c;
            Location = Location.Deck;
            LocationIndex = -1;

            Reset();

            if (c.HP != null)
            {
                CurrentHP = c.HP.Value;
            }

            if (c.Attack != null)
            {
                CurrentTotalAttack = c.Attack.Value;
            }
        }

        public BattleCard Clone()
        {
            BattleCard card = new BattleCard(Card);
            card.Location = Location;
            card.LocationIndex = LocationIndex;
            card.CurrentWaitTime = CurrentWaitTime;
            card.CurrentHP = CurrentHP;
            card.CurrentTotalAttack = CurrentTotalAttack;

            card.Burned = Burned;
            card.Frozen = Frozen;
            card.Paralyzed = Paralyzed;
            card.Trapped = Trapped;
            card.Confused = Confused;
            card.Stunned = Stunned;
            card.Silenced = Silenced;
            card.Poisoned = Poisoned;
            card.Laceration = Laceration;

            return card;
        }

        internal void Reset()
        {
            CurrentWaitTime = Card.BaseCard.Wait;
            CurrentHP = Card.BaseCard.HpArray[Card.Level];
            CurrentTotalAttack = Card.BaseCard.AttackArray[Card.Level];

            Burned = false;
            Frozen = false;
            Paralyzed = false;
            Trapped = false;
            Confused = false;
            Stunned = false;
            Silenced = false;
            Poisoned = false;
            Laceration = false;
        }
    }
}
