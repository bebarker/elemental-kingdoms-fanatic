﻿namespace ElementalKingdoms.ui
{
    partial class UserInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.pbAvatar = new System.Windows.Forms.PictureBox();
            this.lblLevel = new System.Windows.Forms.Label();
            this.lblGems = new System.Windows.Forms.Label();
            this.lblGold = new System.Windows.Forms.Label();
            this.progressEnergy = new System.Windows.Forms.ProgressBar();
            this.lblEnergy = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbAvatar)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(96, 15);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(59, 17);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "lblName";
            // 
            // pbAvatar
            // 
            this.pbAvatar.Location = new System.Drawing.Point(15, 15);
            this.pbAvatar.Name = "pbAvatar";
            this.pbAvatar.Size = new System.Drawing.Size(75, 75);
            this.pbAvatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbAvatar.TabIndex = 1;
            this.pbAvatar.TabStop = false;
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Location = new System.Drawing.Point(12, 15);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(46, 17);
            this.lblLevel.TabIndex = 2;
            this.lblLevel.Text = "label1";
            // 
            // lblGems
            // 
            this.lblGems.AutoSize = true;
            this.lblGems.Location = new System.Drawing.Point(96, 73);
            this.lblGems.Name = "lblGems";
            this.lblGems.Size = new System.Drawing.Size(46, 17);
            this.lblGems.TabIndex = 3;
            this.lblGems.Text = "label1";
            // 
            // lblGold
            // 
            this.lblGold.AutoSize = true;
            this.lblGold.Location = new System.Drawing.Point(148, 73);
            this.lblGold.Name = "lblGold";
            this.lblGold.Size = new System.Drawing.Size(46, 17);
            this.lblGold.TabIndex = 4;
            this.lblGold.Text = "label1";
            // 
            // progressEnergy
            // 
            this.progressEnergy.Location = new System.Drawing.Point(99, 35);
            this.progressEnergy.MarqueeAnimationSpeed = 0;
            this.progressEnergy.Maximum = 50;
            this.progressEnergy.Name = "progressEnergy";
            this.progressEnergy.Size = new System.Drawing.Size(119, 35);
            this.progressEnergy.Step = 2;
            this.progressEnergy.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressEnergy.TabIndex = 5;
            // 
            // lblEnergy
            // 
            this.lblEnergy.AutoSize = true;
            this.lblEnergy.Location = new System.Drawing.Point(224, 53);
            this.lblEnergy.Name = "lblEnergy";
            this.lblEnergy.Size = new System.Drawing.Size(46, 17);
            this.lblEnergy.TabIndex = 6;
            this.lblEnergy.Text = "label1";
            // 
            // UserInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblEnergy);
            this.Controls.Add(this.progressEnergy);
            this.Controls.Add(this.lblGold);
            this.Controls.Add(this.lblGems);
            this.Controls.Add(this.lblLevel);
            this.Controls.Add(this.pbAvatar);
            this.Controls.Add(this.lblName);
            this.Name = "UserInfo";
            this.Size = new System.Drawing.Size(346, 146);
            ((System.ComponentModel.ISupportInitialize)(this.pbAvatar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.PictureBox pbAvatar;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.Label lblGems;
        private System.Windows.Forms.Label lblGold;
        private System.Windows.Forms.ProgressBar progressEnergy;
        private System.Windows.Forms.Label lblEnergy;
    }
}
