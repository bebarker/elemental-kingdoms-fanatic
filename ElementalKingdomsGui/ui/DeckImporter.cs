﻿using ElementalKingdoms.core;
using ElementalKingdoms.util;
using ElementalKingdomsGui.Properties;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElementalKingdomsGui.ui
{
    public partial class DeckImporter : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private MitziLoader mitziLoader;
        private CrystarkLoader crystarkLoader;

        public BindingList<User> Users { get; private set; }

        public DeckImporter()
        {
            InitializeComponent();

            mitziLoader = new MitziLoader();
            crystarkLoader = new CrystarkLoader();

            Users = new BindingList<User>();

            lbCustomDecks.DataSource = Users;
            //Users[0].NickName
            lbCustomDecks.DisplayMember = "NickName";

            AddMessage("Click browse to open a deck file");
        }

        private void AddMessage(string msg, params object[] prm)
        {
            //tbStatus.Text = String.Format(msg, prm);
            if (tbStatus.TextLength > 0)
            {
                tbStatus.AppendText(Environment.NewLine);
            }
            tbStatus.AppendText(String.Format(msg, prm));
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var a = openFileDialog1.FileNames;
                foreach (string f in openFileDialog1.FileNames)
                {
                    LoadFile(f);
                }
            }
        }

        private void LoadFile(string file)
        {
            // Maybe do some basic checking to see what kind of file it is
            if (LoadPeppa(file) || LoadMitzi(file) || LoadCrystark(file))
            {
                AddMessage("Succesfully imported {0}", file);
            }

        }

        private bool LoadCrystark(string file)
        {
            try
            {
                var users = crystarkLoader.Load(file);
                if (users.Count > 0)
                {
                    foreach (var u in users)
                    {
                        Users.Add(u);
                    }
                    AddMessage("Imported {0} Crystark decks", users.Count);
                    return true;
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error: {0}", ex);
            }
            return false;
        }

        private bool LoadMitzi(string file)
        {
            try
            {
                var users = mitziLoader.Load(file);
                if (users.Count > 0)
                {
                    foreach (var u in users)
                    {
                        Users.Add(u);
                    }
                    AddMessage("Imported {0} Mitzi decks", users.Count);
                    return true;
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error: {0}", ex);
            }
            return false;
        }

        private bool LoadPeppa(string file)
        {
            Dictionary<string, User> customDecks = null;
            var data = File.ReadAllText(file);

            // Check if the data file was an old file (containing only Decks)
            // Do this by checking if the AllDecks array is present, this is an element of the User object
            if (data.IndexOf("\"AllDecks\": [") == -1)
            {
                //It was an old deck file which had decks saved instead of users; try to load it like it was a deck and wrap it in a user
                customDecks = new Dictionary<string, User>();

                try
                {
                    Dictionary<string, Deck> decks = JObject.Parse(data).ToObject<Dictionary<string, Deck>>();

                    foreach (var item in decks)
                    {
                        User u = new User() { Level = Settings.Default.playerLevel, NickName = item.Key };
                        u.AllDecks.Add(item.Value);
                        customDecks.Add(item.Key, u);
                    }
                }
                catch (JsonException ex)
                {
                    log.Warn("Cough, Jason", ex);
                }
            }
            else
            {
                try
                {
                    customDecks = JObject.Parse(data).ToObject<Dictionary<string, User>>();
                }
                catch (JsonException ex)
                {
                    log.Warn("Cough, Jason", ex);
                }
            }
            if (customDecks != null && customDecks.Count > 0)
            {
                foreach (User u in customDecks.Values)
                {
                    Users.Add(u);
                }
                AddMessage("Imported {0} Peppa decks", customDecks.Count);
                return true;
            }
            return false;
        }
    }
}
