﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElementalKingdoms.util;

namespace ElementalKingdoms.core
{
    public class Game
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public User User { get; private set; }

        private static Game game = new Game();
        public static Game TheGame
        {
            get
            {
                return game;
            }
        }

        public List<User> ElementalWarsDecks { get; set; }
        public List<User> KingdomWarDecks { get; set; }
        public List<User> HydraDecks { get; set; }

        public List<int> HydraMeritCardIds = new List<int>();

        private Game()
        {
            User = new User();
            ElementalWarsDecks = new List<User>();
            KingdomWarDecks = new List<User>();
            HydraDecks = new List<User>();
        }

        public void Load()
        {
            if (!Directory.Exists("cache"))
            {
                CookieAwareWebClient wc = new CookieAwareWebClient();
                String resourcesFilename = System.IO.Path.GetTempPath() + "ekf_resources.zip";
                String dataFilename = System.IO.Path.GetTempPath() + "ekf_data.zip";
                wc.DownloadFile(@"http://ek.peppa84.com/data.zip", dataFilename);

                //wc.DownloadFileAsync(new Uri(@"http://ek.peppa84.com/resources.zip"), resourcesFilename, "unzip");
                //wc.DownloadFileCompleted += wc_DownloadFileCompleted;

                System.IO.Compression.ZipFile.ExtractToDirectory(dataFilename, ".");
            }

            JsonLoader.LoadSkills();
            JsonLoader.LoadCards();
            JsonLoader.LoadRunes();
            JsonLoader.LoadMapstages();
            JsonLoader.LoadLeague();
        }

        void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //System.IO.Compression.ZipFile.ExtractToDirectory(dataFilename, ".");
            throw new NotImplementedException();
        }
    }
}
