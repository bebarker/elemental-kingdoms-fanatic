﻿namespace ElementalKingdoms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.cbDeck = new System.Windows.Forms.ComboBox();
            this.btnSimulate = new System.Windows.Forms.Button();
            this.deckEditor1 = new ElementalKingdoms.ui.DeckEditor();
            this.userInfo1 = new ElementalKingdoms.ui.UserInfo();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnFreeShizzle = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(298, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Map";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbDeck
            // 
            this.cbDeck.FormattingEnabled = true;
            this.cbDeck.Location = new System.Drawing.Point(298, 41);
            this.cbDeck.Name = "cbDeck";
            this.cbDeck.Size = new System.Drawing.Size(121, 24);
            this.cbDeck.TabIndex = 3;
            // 
            // btnSimulate
            // 
            this.btnSimulate.Location = new System.Drawing.Point(379, 12);
            this.btnSimulate.Name = "btnSimulate";
            this.btnSimulate.Size = new System.Drawing.Size(75, 23);
            this.btnSimulate.TabIndex = 4;
            this.btnSimulate.Text = "Simulate";
            this.btnSimulate.UseVisualStyleBackColor = true;
            this.btnSimulate.Click += new System.EventHandler(this.btnSimulate_Click);
            // 
            // deckEditor1
            // 
            this.deckEditor1.Location = new System.Drawing.Point(12, 137);
            this.deckEditor1.Name = "deckEditor1";
            this.deckEditor1.Size = new System.Drawing.Size(729, 178);
            this.deckEditor1.TabIndex = 2;
            // 
            // userInfo1
            // 
            this.userInfo1.Location = new System.Drawing.Point(12, 12);
            this.userInfo1.Name = "userInfo1";
            this.userInfo1.Size = new System.Drawing.Size(280, 119);
            this.userInfo1.TabIndex = 0;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(268, 12);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(24, 23);
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Text = "@";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnFreeShizzle
            // 
            this.btnFreeShizzle.Location = new System.Drawing.Point(516, 52);
            this.btnFreeShizzle.Name = "btnFreeShizzle";
            this.btnFreeShizzle.Size = new System.Drawing.Size(75, 23);
            this.btnFreeShizzle.TabIndex = 6;
            this.btnFreeShizzle.Text = "Free shizzle";
            this.btnFreeShizzle.UseVisualStyleBackColor = true;
            this.btnFreeShizzle.Click += new System.EventHandler(this.btnFreeShizzle_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 335);
            this.Controls.Add(this.btnFreeShizzle);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnSimulate);
            this.Controls.Add(this.cbDeck);
            this.Controls.Add(this.deckEditor1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.userInfo1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private ui.UserInfo userInfo1;
        private System.Windows.Forms.Button button1;
        private ui.DeckEditor deckEditor1;
        private System.Windows.Forms.ComboBox cbDeck;
        private System.Windows.Forms.Button btnSimulate;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnFreeShizzle;
    }
}