﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ElementalKingdoms.core
{
    public enum OpType
    {
        Bloodsucker = 2,
        RuneActivate = 3, // Or ApplyRuneSkill?
        TrapAttack = 4,
        ElectricAttack = 5,
        ColdAttack = 6,
        FireAttack = 7,
        BloodAttack = 8,
        PoisonAttack = 9,        
        Parry = 10,
        Heal = 11,
        Snipe = 12,
        Multihit = 13,
        AttackIncrease = 14,
        MaxHPIncrease = 15, // Spring Breeze, Divine Protection
        Reincarnation = 16,
        AttackDecrease = 17,
        Selfdestruct = 18,
        HeroHeal = 19,
        HeroDamage = 20,
        Bloodthirst = 21,
        Craze = 22,
        Plague = 23,
        Teleportation = 24,
        Reflection = 25,
        Exile = 26,
        Destroy = 27,
        Burn = 28,
        IceShield = 29,
        Laceration = 30,
        MagicShield = 31,
        Resistance = 32,
        Immune = 33,
        Guard = 34,
        Dodge = 35,
        Sacrifice = 36,
        Puncture = 37,
        Confusion = 38,
        Leech = 39,
        ManaCorruption = 40,
        SacredFlame = 41,
        WaitTimeDecrease = 42,
        WaitTimeIncrease = 43,
        Roar = 44,
        DeathMarker = 45,
        Silence = 46,

        LastChance = 48,
        HolyLight = 49,
        Naturalize = 50,
        Corruption = 51,
        LavaTrial = 52,
        DivineShield = 53, // The first physical attack against this card will be nullified.
        FrostShock = 54,
        ShieldOfEarth = 55,
        SummonCards = 56,

        DrawCard = 1001,
        PutOnBattlefield = 1002,
        CardDies = 1003,
        CemetaryToDeck = 1004,
        CemetaryToHand = 1005,
        CemetaryToBattlefield = 1006,
        BattlefieldToLibrary = 1007,
        BattlefieldToVoid = 1008,
        /// <summary>1009</summary>
        VoidToBattlefield = 1009,

        CardModifyAttack = 1020,
        ReduceAllWaitTime = 1021, // Komt aan het begin van elke round. Checken of hier nog iets speciaals gebeurd. Gokje: wait time verlagen van alle kaarten.
        HeroModifyHP = 1022,
        SetWaitTime = 1023,
        AttackCardDealDamage = 1030,
        CardModifyHP = 1040,
        RuneExpires = 1050,

        XXXXXXXX1060XXXXXXX = 1060, // 1060 komt tussen 'gooi cards on battlefield' en 'einde gevecht'. Gokje: Battlefield cleanup van lege plekken.

        Trapped = 1101,
        Paralyzed = 1102,
        Frozen = 1103,
        Poisoned = 1104,
        Sealed = 1105,
        Lacerated = 1106,
        Burned = 1107,
        Confused = 1108,
        Roared = 1109,
        DeathMarked = 1110,
        Silenced = 1111,
        Stunned = 1112,
    }

    [JsonConverter(typeof(OpJsonConverter))]
    public class Op
    {
        public string UUID { get; set; }
        public OpType Opp { get; set; }
        public List<string> Target { get; set; }
        public int? Value { get; set; }

        // Optional - in Opp 1040 - current HP of the defending card
        public int HP { get; set; }

        public Op()
        {
            Target = new List<string>();
        }

        public override string ToString()
        {
            string target = "-";
            if (Target != null)
            {
                target = String.Join(",", Target);
            }
            return String.Format("{0} {1} [{2}] --> {3}", Opp, UUID, target, Value);
        }

        /* Round 1 (attacker)
         * {
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "atk_1",
				"Opp": 1001,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			}*/
        /* Round 2 (defender)
        {
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "def_2",
				"Opp": 1001,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			}*/

        /* Round 3 (attacker, wait for input)
         * {
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "atk_3",
				"Opp": 1001,
				"Target": null,
				"Value": 0
			} */
        /* Round 3 - continued
         * {
				"UUID": "atk_1",
				"Opp": 1002,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "atk_1",
				"Opp": 1030,
				"Target": ["def"],
				"Value": 85
			},
			{
				"UUID": "atk_1",
				"Opp": 1022,
				"Target": ["def"],
				"Value": -85
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			}*/

        /* Round 4
         * {
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "def_1",
				"Opp": 1001,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "def_2",
				"Opp": 1002,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "def_2",
				"Opp": 1030,
				"Target": ["atk_1"],
				"Value": 110
			},
			{
				"UUID": "def_2",
				"HP": 300,
				"Opp": 1040,
				"Target": ["atk_1"],
				"Value": -110
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			} */

        /* Round 5
         * {
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "atk_2",
				"Opp": 1001,
				"Target": null,
				"Value": 0
			}
         * */
        /* Round 5 - continued
         * {
				"UUID": "atk_3",
				"Opp": 1002,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "atk_1",
				"Opp": 1030,
				"Target": ["def_2"],
				"Value": 85
			},
			{
				"UUID": "atk_1",
				"HP": 250,
				"Opp": 1040,
				"Target": ["def_2"],
				"Value": -85
			},
			{
				"UUID": "atk_3",
				"Opp": 1030,
				"Target": ["def"],
				"Value": 125
			},
			{
				"UUID": "atk_3",
				"Opp": 1022,
				"Target": ["def"],
				"Value": -125
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			} */

        /* Round 6 (defender)
         * {
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "def_1",
				"Opp": 1002,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "def_2",
				"Opp": 1030,
				"Target": ["atk_1"],
				"Value": 110
			},
			{
				"UUID": "def_2",
				"HP": 190,
				"Opp": 1040,
				"Target": ["atk_1"],
				"Value": -110
			},
			{
				"UUID": "def_1",
				"Opp": 1030,
				"Target": ["atk_3"],
				"Value": 110
			},
			{
				"UUID": "def_1",
				"HP": 370,
				"Opp": 1040,
				"Target": ["atk_3"],
				"Value": -110
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			}*/

        /* Round 7 (attacker - auto)
         * {
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "atk_2",
				"Opp": 1002,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "atk_1",
				"Opp": 1030,
				"Target": ["def_2"],
				"Value": 85
			},
			{
				"UUID": "atk_1",
				"HP": 165,
				"Opp": 1040,
				"Target": ["def_2"],
				"Value": -85
			},
			{
				"UUID": "atk_3",
				"Opp": 1030,
				"Target": ["def_1"],
				"Value": 125
			},
			{
				"UUID": "atk_3",
				"HP": 250,
				"Opp": 1040,
				"Target": ["def_1"],
				"Value": -125
			},
			{
				"UUID": "atk_2",
				"Opp": 1030,
				"Target": ["def"],
				"Value": 125
			},
			{
				"UUID": "atk_2",
				"Opp": 1022,
				"Target": ["def"],
				"Value": -125
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			} */

        /* Round 8
         * {
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "def_2",
				"Opp": 1030,
				"Target": ["atk_1"],
				"Value": 110
			},
			{
				"UUID": "def_2",
				"HP": 80,
				"Opp": 1040,
				"Target": ["atk_1"],
				"Value": -80
			},
			{
				"UUID": "atk_1",
				"Opp": 1003,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "def_1",
				"Opp": 1030,
				"Target": ["atk_3"],
				"Value": 110
			},
			{
				"UUID": "def_1",
				"HP": 260,
				"Opp": 1040,
				"Target": ["atk_3"],
				"Value": -110
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			} */

        /* Round 9
         * {
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "atk_3",
				"Opp": 1030,
				"Target": ["def_2"],
				"Value": 125
			},
			{
				"UUID": "atk_3",
				"HP": 80,
				"Opp": 1040,
				"Target": ["def_2"],
				"Value": -80
			},
			{
				"UUID": "def_2",
				"Opp": 1003,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "atk_2",
				"Opp": 1030,
				"Target": ["def_1"],
				"Value": 125
			},
			{
				"UUID": "atk_2",
				"HP": 125,
				"Opp": 1040,
				"Target": ["def_1"],
				"Value": -125
			},
			{
				"UUID": "def_1",
				"Opp": 1003,
				"Target": null,
				"Value": 0
			} */
    }

    class OpJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {

            JObject jObject = JObject.Load(reader);

            Op obj = new Op
            {
                UUID = jObject["UUID"].Value<string>(),
                Opp = (OpType) jObject["Opp"].Value<int>(),
                Target = new List<string>()
            };

            if (!Enum.IsDefined(typeof(OpType), obj.Opp))
            {
                int opCode = (int)obj.Opp;
                
                Console.WriteLine("Value {0} is not defined", opCode);
            }

            obj.Value = jObject["Value"].Value<int?>();

            var bla = jObject["Target"];
            if (bla.HasValues)
            {
                //Console.WriteLine("Target has values");
                foreach (var blabla in bla)
                {
                    if (blabla.HasValues)
                    {
                        //Console.WriteLine("blabla has values");
                        foreach (var blablabla in blabla)
                        {
                            if (blablabla.HasValues)
                            {
                                Console.WriteLine("Multiple blabla values...");

                            }
                            else
                            {
                                obj.Target.Add(blablabla.Value<string>());
                            }
                        }
                    }
                    else
                    {
                        obj.Target.Add(blabla.Value<string>());
                    }

                }
            }

            //Console.WriteLine(obj.ToString());
            
            return obj;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var op = value as Op;
            writer.WriteStartObject();
            //writer.WritePropertyName("$" + op.Name);
            writer.WritePropertyName("UUID");
            serializer.Serialize(writer, op.UUID);
            writer.WritePropertyName("Opp");
            serializer.Serialize(writer, op.Opp);
            writer.WritePropertyName("Target");
            serializer.Serialize(writer, op.Target);
            writer.WritePropertyName("Value");
            serializer.Serialize(writer, op.Value);
            writer.WriteEndObject();
        }
    }
}
