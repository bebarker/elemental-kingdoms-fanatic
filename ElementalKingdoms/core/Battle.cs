﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class Battle
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public const int MAX_HAND_SIZE = 5;
        public BattleUser AttackPlayer { get; set; }
        public BattleUser DefendPlayer { get; set; }

        public bool Finished { get; private set; }
        public bool Win { get; private set; }

        private BattleCard demon { get; set; }
        public int Merit { 
            get {
                if (demon != null) return demon.BaseHP - demon.CurrentHP;
                return 0;
            }
        }

        public bool StoreReplay { get; set; }
        public List<BattleRound> Rounds { get; set; }
        private BattleRound CurrentRound;

        private BattleUser CurrentPlayer { get; set; }
        private BattleUser TargetPlayer { get; set; }

        private BuffManager Buffs { get; set; }

        public Battle(User attacker, User defender)
        {
            StoreReplay = true;
            Finished = false;
            Rounds = new List<BattleRound>();

            AttackPlayer = new BattleUser(attacker, true);
            DefendPlayer = new BattleUser(defender, false);

            CurrentPlayer = AttackPlayer;
            TargetPlayer = DefendPlayer;

            Buffs = new BuffManager(this);

            if (IsDemon())
            {
                SwitchPlayers();
            }
            else if (defender.HasLegendary())
            {
                SwitchPlayers();
                foreach (var card in DefendPlayer.Deck)
                {
                    if (card.Race == Race.Hydra)
                    {
                        demon = card;
                        break;
                    }
                }
            }
            else if (defender.HasHydra())
            {
                SwitchPlayers();
                foreach (var card in DefendPlayer.Deck)
                {
                    if (card.Race == Race.Hydra && card.Name.StartsWith("Hydra"))
                    {
                        demon = card;
                        break;
                    }
                }

                foreach (var card in AttackPlayer.Deck)
                {
                    if (Game.TheGame.HydraMeritCardIds.Contains(card.Card.CardId))
                    {
                        log.DebugFormat("Merit card {0}", card.Name);
                        if (card.Card.BaseCard.Stars == 3)
                        {
                            card.SetBaseStats((int) (card.CardBaseAttack * 1.5f), (int)(card.CardBaseHP * 1.5f));
                        }
                        else if (card.Card.BaseCard.Stars == 4)
                        {
                            card.SetBaseStats(card.CardBaseAttack * 2, card.CardBaseHP * 2);
                        }
                        else if (card.Card.BaseCard.Stars == 5)
                        {
                            card.SetBaseStats(card.CardBaseAttack * 3, card.CardBaseHP * 3);
                        }
                    }
                }
            }
            else if (IsThief())
            {
                // Thief starts.
                SwitchPlayers();

                bool legendary = IsLegendaryThief();
                // Update health and attack of the cards in the deck
                foreach (BattleCard card in DefendPlayer.Deck)
                {
                    card.SetThief(legendary);
                }
            }
        }

        private bool IsDemon()
        {
            var demons = DefendPlayer.Deck.FindAll(x => x.Race == Race.Demon);
            if (demons.Count == 1)
            {
                demon = demons[0];
                return true;
            }
            return false;
        }

        private bool IsThief()
        {
            return DefendPlayer.NickName != null && (DefendPlayer.NickName.EndsWith(" [thief]") || IsLegendaryThief());
        }

        private bool IsLegendaryThief()
        {
            return DefendPlayer.NickName.EndsWith(" [legendary thief]");
        }

        public void KingdomWarNextFight(User defender)
        {
            Finished = false;
            Win = false;
            Rounds = new List<BattleRound>();

            AttackPlayer.MaxHP = AttackPlayer.CurrentHP;

            foreach (var card in AttackPlayer.Battlefield)
            {
                card.SetBaseStats(Math.Min(card.CardBaseAttack, card.CurrentAttack), Math.Min(card.CardBaseHP, card.CurrentHP));
            }

            AttackPlayer.Cemetary.Clear();
            AttackPlayer.Deck.AddRange(AttackPlayer.Hand);
            AttackPlayer.Deck.AddRange(AttackPlayer.Battlefield);

            AttackPlayer.Hand.Clear();
            AttackPlayer.Battlefield.Clear();

            AttackPlayer.Runes.ForEach(x => x.Reset());

            AttackPlayer.Deck.Shuffle();

            DefendPlayer = new BattleUser(defender, false);

            CurrentPlayer = AttackPlayer;
            TargetPlayer = DefendPlayer;

            Buffs = new BuffManager(this);
        }

        public void Round()
        {
            if (Finished) return;

            CurrentRound = new BattleRound();
            Rounds.Add(CurrentRound);
            CurrentRound.Number = Rounds.Count;
            CurrentRound.isAttack = (CurrentPlayer == AttackPlayer);

            log.DebugFormat("----------- Starting round {0} -----------", CurrentRound.Number);

            // At round 51, the player starts taking unavoidable damage.
            // Source: https://github.com/Jsdemonsim/demonsim/blob/master/sim.c line 1565-1573 (commit e14ecf563190e1e3c1baac7d68ae5f0fff3576e0)
            if (CurrentRound.Number >= 51)
            {
                int dmg = (CurrentRound.Number - 51) * 30 + 50;

                log.DebugFormat("Player takes {0} unavoidable damage due to too many rounds", dmg);
                DamageHero(null, CurrentPlayer, dmg, false);
                if (CheckWinConditions(false)) { return; }
            }

            foreach (var card in CurrentPlayer.Battlefield)
            {
                card.ClearRuneSkills();
            }

            // 1. Reduce current wait time of all cards by 1.
            ReduceWaitTime();

            // 1b. Clear opponent's 'all cards become <Swamp> for 1 turn' (need to check timing)
            foreach (var card in TargetPlayer.Battlefield)
            {
                card.Race = card.BaseRace;
            }

            // 2. Draw a card
            DrawCard();

            // 3. Put all cards with 0 wait time on the field
            PutCardsOnBattlefield();

            if (CheckWinConditions(false)) { return; }

            // 4. Activate runes
            ActivateRunes();

            // Remove dead cards
            CleanupCardIndexes();
            
            // Some cards have skills that trigger at the beginning of the turn (for example Purification on Aries)
            StartOfTurnSkills();

            // store number of attackers in temp variable. An attacker might die due to poison / burn / retal during its own turn
            int numAttackers = CurrentPlayer.Battlefield.Count;

            // 5. For each card on the battlefield, from left to right:
            for (int i = 0; i < numAttackers; i++)
            {
                BattleCard attackingCard = CurrentPlayer.Battlefield[i];
                BattleCard defendingCard = null;

                if (attackingCard == null)
                {
                    log.DebugFormat("Card at position {0} is already dead. Probably due to retaliation after my neighbour attacked.", i);
                    continue;
                }
                log.DebugFormat("Attacking with {0}", attackingCard);

                // If I am currently stunned, I will recover at the end of this turn
                bool clearStun = attackingCard.Stunned;

                if (attackingCard.LastChanceActive)
                {
                    attackingCard.LastChanceActive = false;
                }

                if (attackingCard.Silenced)
                {
                    if (attackingCard.RecentlySilenced)
                    {
                        attackingCard.RecentlySilenced = false;
                    }
                    else
                    {
                        attackingCard.Silenced = false;
                        attackingCard.ClearSilence();
                    }
                }

                if (attackingCard.Confused)
                {
                    log.Debug("Confused, attacking my master");
                    DamageHero(attackingCard.Card.UUID, CurrentPlayer, attackingCard.CurrentTotalAttack, true);
                }
                else
                if (! (attackingCard.Trapped || attackingCard.Frozen || attackingCard.Stunned) )
                {
                    defendingCard = TargetPlayer.Battlefield[i];

                    log.DebugFormat("Defending with {0}", defendingCard);

                    UseSkills(i, attackingCard, defendingCard);

                    if (CheckWinConditions(false)) { return; }

                    // Update defender; it might have been killed with a skill
                    if (TargetPlayer.Battlefield[i] != defendingCard)
                    {
                        log.DebugFormat("Something changed about the defender... {0} {1}", defendingCard, TargetPlayer.Battlefield[i]);
                    }
                    defendingCard = TargetPlayer.Battlefield[i];

                    Attack(i, attackingCard, defendingCard);
                }
                else
                {
                    log.Debug("I was trapped :-(");
                }

                if (clearStun)
                {
                    // Only reset stun status if I was stunned at the start of this turn.
                    attackingCard.Stunned = false;
                    if (StoreReplay) AddOp(OpType.Stunned, attackingCard.Card.UUID, new List<string>() { attackingCard.Card.UUID }, 0);
                }

                EndOfTurn(attackingCard);

                if (CheckWinConditions(false)) { return; }
            }

            CleanupCardIndexes();

            // 6. Win conditions
            if (CheckWinConditions(true)) { return; }
            
            // 7. Swap players
            SwitchPlayers();
        }

        private void StartOfTurnSkills()
        {
            foreach (var card in CurrentPlayer.Battlefield)
            {
                foreach (var skill in card.ActiveSkills().FindAll(x => x.LanchType == LaunchType.OnStartOfTurn))
                {
                    new SkillExecutor(this, skill, CurrentPlayer, TargetPlayer, card);
                }
            }
        }

        public void EndOfTurn(BattleCard card)
        {
            // Clear frozen / paralyze / confuse / trapped
            if (card.Trapped)
            {
                card.Trapped = false;
                if (StoreReplay) AddOp(OpType.Trapped, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
            }
            if (card.Paralyzed)
            {
                card.Paralyzed = false;
                if (StoreReplay) AddOp(OpType.Paralyzed, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
            }
            if (card.Frozen)
            {
                card.Frozen = false;
                if (StoreReplay) AddOp(OpType.Frozen, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
            }
            if (card.Confused)
            {
                card.Confused = false;
                if (StoreReplay) AddOp(OpType.Confused, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
            }

            // Poison damage
            if (card.Poisoned)
            {
                foreach (var p in card.Poisons.ToList())
                {
                    log.DebugFormat("Poison {0}", p);
                    DamageCard(card.Card.UUID, card, p);
                }
                card.Poisons.Clear();
                if (StoreReplay) AddOp(OpType.Poisoned, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
            }

            // Regen (unless lacerated)
            if (!card.Laceration)
            {
                var rejuvenations = card.ActiveSkills().FindAll(x => x.Type == SkillType.Heal && x.AffectType == 26);
                foreach (Skill s in rejuvenations)
                {
                    log.DebugFormat("Rejuvenation: {0} --> {1}", s.Name, s.AffectValue);
                    HealCard(card.Card.UUID, card, s.AffectValue);
                }
            }

            // Burned
            foreach (var burn in card.Burns.Values.ToList())
            {
                log.DebugFormat("Burn {0}", burn);
                DamageCard(card.Card.UUID, card, burn);
            }

            if (card.DeathMark > 0)
            {
                card.DeathMark = 0;
                if (StoreReplay) AddOp(OpType.DeathMarked, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
            }

            // Clear temp attack bonus
            card.BonusRoundAttack = 0;
            card.FirstAttack = false; // not sure if we always do it like this...
        }

        public void DamageHero(string sourceUuid, BattleUser player, int amount, bool canGuardDamage)
        {
            // HeroModifyHP atk_5 [def] --> -385
            if (canGuardDamage)
            {
                // Guards catch damage first, but any overkill damage is still dealt to me
                amount = GuardDamage(sourceUuid, player, amount);
            }

            player.CurrentHP -= amount;
            if (StoreReplay) AddOp(OpType.HeroModifyHP, sourceUuid, new List<string>() { player.UUID }, amount * -1);
        }


        public int GuardDamage(string sourceUuid, BattleUser player, int dmg)
        {
            if (player.Guards.Count > 0)
            {
                foreach (BattleCard card in player.Guards.ToList())
                {
                    int dmgLeftAfterThisTarget = dmg - card.CurrentHP;
                    log.DebugFormat("Guarding {0} dmg ({1} hp left)", dmg, card.CurrentHP);
                    DamageCard(sourceUuid, card, dmg);

                    dmg = dmgLeftAfterThisTarget;
                    if (dmg <= 0)
                    {
                        break;
                    }
                    log.DebugFormat("{0} damage left for other guards", dmg);
                }

                return Math.Max(0, dmg);
            }
            return dmg;
        }

        public void HealHero(string sourceUuid, BattleUser player, int amount)
        {
            // HeroModifyHP atk_5 [atk] --> 120
            if (StoreReplay) AddOp(OpType.HeroModifyHP, sourceUuid, new List<string>() { player.UUID }, amount);
            player.CurrentHP += amount;
        }

        public void Destroy(string sourceUuid, BattleCard card)
        {
            AddOp(OpType.Destroy, sourceUuid, new List<string>() { card.Card.UUID }, 1);
            Die(card);
        }

        public void Sacrifice(string sourceUuid, BattleCard card)
        {
            AddOp(OpType.Sacrifice, sourceUuid, new List<string>() { card.Card.UUID }, 1);
            Die(card);
        }

        private void CleanupCardIndexes()
        {
            if (StoreReplay) AddOp(OpType.XXXXXXXX1060XXXXXXX, null, new List<string>(), 3);
            CurrentPlayer.Battlefield.RemoveDeadCards();
            TargetPlayer.Battlefield.RemoveDeadCards();
        }

        private void ActivateRunes()
        {
            foreach (BattleRune r in CurrentPlayer.Runes)
            {
                Skill s = r.Activate(CurrentPlayer, TargetPlayer, CurrentRound.Number);
                if (s != null)
                {
                    new SkillExecutor(this, s, CurrentPlayer, TargetPlayer, null);
                }
            }
        }

        private bool CheckWinConditions(bool endOfTurn)
        {
            bool currentPlayerLost = false;
            if (TargetPlayer.CurrentHP <= 0
                || (endOfTurn && (TargetPlayer.Deck.Count == 0 && TargetPlayer.Hand.Count == 0 && TargetPlayer.Battlefield.Count == 0)))
            {
                // Current player wins
                Finished = true;
            }

            // Lose conditions
            if (CurrentPlayer.CurrentHP <= 0)
            {
                // Current player loses
                Finished = true;
                currentPlayerLost = true;
            }

            if (Finished)
            {
                // So we are finished. If the current round is an attack, it means the current player is YOU. Otherwise, the currentPlayer is your opponent.
                if (CurrentRound.isAttack)
                {
                    if (!currentPlayerLost)
                    {
                        Win = true;
                    }
                    else
                    {
                        // Painful, you lose due to self-damage or something.
                    }
                }
                else
                {
                    if (currentPlayerLost)
                    {
                        Win = true;
                    }
                }

                if (Win)
                {
                    log.DebugFormat("YOU WIN! That took {0} rounds.", Rounds.Count);
                }
                else
                {
                    log.DebugFormat("You lose :'( That took {0} rounds.", Rounds.Count);
                }
            }

            return Finished;
        }

        private void Attack(int i, BattleCard attackingCard, BattleCard defendingCard)
        {
            //  If not paralyzed:
            //    d. Attack
            if (attackingCard.Paralyzed)
            {
                log.Debug("I was paralyzed :-(");
                return;
            }

            foreach (Skill s in attackingCard.ActiveSkills().FindAll(x => x.LanchType == LaunchType.BeforeAttack && x.Type != SkillType.MultiHit))
            {
                //log.DebugFormat("Going to activate PRE ATTACK skill {0}", s.Name);

                if (s.LanchCondition == LaunchCondition.Always)
                {
                    if (s.AffectType == 30)
                    {
                        // backstab
                        if (attackingCard.FirstAttack)
                        {
                            //log.DebugFormat("Backstab");
                            attackingCard.BonusRoundAttack += s.AffectValue;
                        }
                    }
                    else if (s.AffectType == 68)
                    {
                        if (defendingCard == null)
                        {
                            log.DebugFormat("Slayer");
                            int t = attackingCard.CurrentTotalAttack * s.AffectValue / 100;
                            log.DebugFormat("Increasing attack by {0}", t);
                            attackingCard.BonusRoundAttack += t;
                        }
                    }
                    else if (s.AffectType == 74)
                    {
                        // hot chase
                        attackingCard.BonusRoundAttack += s.AffectValue * TargetPlayer.Cemetary.Count;
                    }
                    else if (s.AffectType == 75)
                    {
                        // vendetta
                        attackingCard.BonusRoundAttack += s.AffectValue * CurrentPlayer.Cemetary.Count;
                    }
                    else if (s.AffectType == 97)
                    {
                        // Inspire
                        attackingCard.BonusRoundAttack += s.AffectValue * CurrentPlayer.Battlefield.Count;
                    }
                    else if (s.AffectType == 98)
                    {
                        // Bloody Battle
                        // This card's attack would be increased by [1.0x] (Base HP loss of this card) when attacking.
                        int hpLoss = Math.Max(0, attackingCard.BaseHP - attackingCard.CurrentHP);
                        int bonusAttack = hpLoss * 100 / s.AffectValue;
                        log.DebugFormat("Bloody Battle - Base HP loss: {0} (base: {1} current: {2}) | percentage of HP loss that will be added as attack: {3} | Final bonus attack: {4}", hpLoss, attackingCard.BaseHP, attackingCard.CurrentHP, s.AffectValue, bonusAttack);
                        attackingCard.BonusRoundAttack += bonusAttack;
                    }
                    else
                    {
                        //log.WarnFormat("UNHANDLED AFFECT TYPE *ALWAYS* PRE ATTACK skill {0} {1} {2} {3}", s.Name, s.LanchCondition, s.SkillCategory, s.Type);
                    }
                }
            }

            if (defendingCard == null || defendingCard.CurrentHP <= 0)
            {
                if (defendingCard != null && defendingCard.CurrentHP <= 0)
                {
                    log.DebugFormat("<= 0 hp, but not null?"); // just checking if this ever happens. Probably not with the healthcheck / die routine.
                }
                log.DebugFormat("No defender. Hitting target player for {0}", attackingCard.CurrentTotalAttack);
                DamageHero(attackingCard.Card.UUID, TargetPlayer, attackingCard.CurrentTotalAttack, true);
                log.DebugFormat("Target player hp down to {0}", TargetPlayer.CurrentHP);
            }
            else
            {
                foreach (Skill s in attackingCard.ActiveSkills().FindAll(x => x.LanchType == LaunchType.BeforeAttack && x.Type != SkillType.MultiHit))
                {
                    // because multihit is actually after hit...
                    if (s.LanchCondition == LaunchCondition.BeforeDamage)
                    {
                        log.DebugFormat("{0} activates against {1}. Defending card {2} race is {3}", s.Name, (Race)s.LanchConditionValue, defendingCard.Name, defendingCard.Race);
                        if (s.LanchConditionValue == (int)defendingCard.Race)
                        {
                            int t = attackingCard.CurrentTotalAttack * s.AffectValue / 100;
                            log.DebugFormat("Increasing attack by {0}", t);
                            attackingCard.BonusRoundAttack += t;
                        }
                    }
                    else if (s.LanchCondition == LaunchCondition.Always)
                    {
                        if (s.AffectType == 57)
                        {
                            // Concentration
                            if (ThreadSafeRandom.ThisThreadsRandom.Next(100) < 50)
                            {
                                int t = (attackingCard.CurrentAttack + attackingCard.BonusSkillAttack) * s.AffectValue / 100;
                                log.DebugFormat("Concentration triggered. Increasing attack by {0}", t);
                                attackingCard.BonusRoundAttack += t;
                            }
                        }
                    }
                    else if (s.LanchCondition == LaunchCondition.TargetHasAffliction)
                    {
                        log.DebugFormat("Blitz");
                        if (defendingCard.HasAffliction)
                        {
                            log.DebugFormat("Target is afflicted by something. Increasing attack");
                            int t = (attackingCard.CurrentAttack + attackingCard.BonusSkillAttack) * s.AffectValue / 100;
                            attackingCard.BonusRoundAttack += t;
                        }
                    }
                    else if (s.LanchCondition == LaunchCondition.TargetHasMoreHealth)
                    {
                        log.DebugFormat("Warpath");
                        if (defendingCard.CurrentHP > attackingCard.CurrentHP)
                        {
                            int t = (attackingCard.CurrentAttack + attackingCard.BonusSkillAttack) * s.AffectValue / 100;
                            attackingCard.BonusRoundAttack += t;
                        }
                    }
                    else
                    {
                        // already handled / not implemented?
                    }
                }

                int roaredReduction = 0;
                if (attackingCard.Roared)
                {
                    attackingCard.Roared = false;
                    roaredReduction = attackingCard.CurrentTotalAttack / 2;
                    attackingCard.BonusRoundAttack -= roaredReduction;
                }

                Attack(i, attackingCard, defendingCard, attackingCard.CurrentTotalAttack, false);

                if (attackingCard.HasDoubleAttack && defendingCard.CurrentHP > 0)
                {
                    if (StoreReplay) AddOp(0, attackingCard.Card.UUID, new List<string>() { attackingCard.Card.UUID }, 0);
                    Attack(i, attackingCard, defendingCard, attackingCard.CurrentTotalAttack, false);
                }

                attackingCard.BonusRoundAttack += roaredReduction;
            }
        }

        public int Attack(int i, BattleCard attacker, BattleCard defendingCard, int attackDamage, bool isMultiHit)
        {
            if (defendingCard.LastChanceActive)
            {
                log.Debug("Last chance is active, not receiving any damage from attack");
                return 0;
            }

            // Skills that trigger on hit rely on the actual damage done. If no damage done, they do not trigger.
            // Most exclude overkill damage (like life-stealing, puncture). Some of them do their thing including overkill damage (clean sweep)
            int actualDamage = attackDamage;
            int overkillDamage = 0;

            // Dodge, parry, etc

            foreach (Skill s in defendingCard.ActiveSkills().FindAll(x => x.LanchType == LaunchType.BeforeDefend))
            {
                log.DebugFormat("Using BeforeDefend skill {0} {1} {2} {3}", s.Name, s.LanchCondition, s.SkillCategory, s.Type);
                if (s.Type == SkillType.Dodge)
                {
                    if (!attacker.HasInfiltrator)
                    {
                        if (ThreadSafeRandom.ThisThreadsRandom.Next(100) < s.AffectValue)
                        {
                            log.DebugFormat("Dodged");
                            actualDamage = 0;
                        }
                    }
                    else
                    {
                        log.DebugFormat("Infiltrator - ignoring dodge");
                    }
                }
                else if (s.Type == SkillType.DamageReduction)
                {
                    if (s.LanchCondition == LaunchCondition.ReceivedDamage)
                    {
                        int reduction = s.AffectValue;
                        if (s.AffectValue2 > 0)
                        {
                            reduction = actualDamage * s.AffectValue2 / 100;
                        }
                        // Parry
                        actualDamage = Math.Max(actualDamage - reduction, 0);
                        log.DebugFormat("Parry triggered. Damage reduced to {0} ({1} blocked)", actualDamage, reduction);
                    }
                    else if (s.LanchCondition == LaunchCondition.BeforeDamage)
                    {
                        // Barriers                            
                        log.DebugFormat("Attacking card race {0}", (int)attacker.Race);
                        if (s.LanchConditionValue == (int)attacker.Race)
                        {
                            // TODO: Verify rounding.
                            log.DebugFormat("Launch condition met {0} - damage before: {1}", s.AffectValue, actualDamage);
                            actualDamage -= actualDamage * s.AffectValue / 100;
                            log.DebugFormat("Damage after: {0}", actualDamage);
                        }
                    }
                }
                else if (s.Type == SkillType.IceShield)
                {
                    if (attacker.HasInfiltrator)
                    {
                        log.DebugFormat("Infiltrator - ignoring ice shield");
                    }
                    else if (actualDamage > s.AffectValue)
                    {
                        actualDamage = s.AffectValue;
                        log.DebugFormat("Ice shield triggered. Damage reduced to {0}", actualDamage);
                    }
                }
                else if (s.Type == SkillType.WaterShield)
                {
                    if (actualDamage > s.AffectValue)
                    {
                        int heal = Math.Min(s.AffectValue2, actualDamage - s.AffectValue);
                        actualDamage = s.AffectValue;
                        log.DebugFormat("Water Shield triggered. Damaged reduced to {0}. Healing hero for {1}", actualDamage, heal);
                        HealHero(defendingCard.Card.UUID, TargetPlayer, heal);
                    }
                }
                else if (s.Type == SkillType.DivineShield)
                {
                    if (defendingCard.DivineShieldAvailable)
                    {
                        log.DebugFormat("Divine Shield triggered. Damage nullified.");
                        actualDamage = 0;
                        defendingCard.DivineShieldAvailable = false;
                    }
                    else
                    {
                        log.DebugFormat("Divine Shield has already been used.");
                    }
                }
                else
                {
                    log.WarnFormat("Unhandled skill {0}", s.Name);
                }
            }

            if (actualDamage > defendingCard.CurrentHP)
            {
                overkillDamage = actualDamage - defendingCard.CurrentHP;
                actualDamage = defendingCard.CurrentHP;
            }

            DamageCard(attacker.Card.UUID, defendingCard, actualDamage);
            
            log.DebugFormat("Did {0} damage to the target", actualDamage);

            if (actualDamage > 0)
            {
                // TODO: Need to consider the following. 
                //  All these skills work on the actual amount done, after stuff like parry, barriers, dodge, etc
                //  Skills like Bloodsucker and Puncture , and ignore the overkill damage.
                //  Skills like Clean Sweep *do* take in account overkill damage.
                // So on a target with 100 hp left getting hit for 500 damage, Bloodsucker etc use '100' to calculate their effects, but Clean Sweep will use 500.
                AfterDamagingAttackSkills(i, attacker, defendingCard, actualDamage);

                if (!isMultiHit)
                {
                    foreach (Skill s in attacker.ActiveSkills().FindAll(x => x.LanchType == LaunchType.BeforeAttack && x.Type == SkillType.MultiHit))
                    {
                        MultiHit(i, attacker, defendingCard, actualDamage, s);
                    }
                }
            }

            // should return {damage, overkillDamage}
            return actualDamage;
        }

        private void AfterDamagingAttackSkills(int i, BattleCard attackingCard, BattleCard defendingCard, int dmgDone)
        {
            foreach (Skill s in attackingCard.ActiveSkills().FindAll(x => x.LanchType == LaunchType.AfterAttack))
            {
                if (s.Type == SkillType.AttackIncreaseAfterDealingDamage)
                {
                    log.DebugFormat("Increasing attack by {0}", s.AffectValue);
                    attackingCard.CurrentAttack += s.AffectValue;
                }
                else if (s.Type == SkillType.Puncture)
                {
                    // TODO Verify rounding ingame.
                    int dmgToHero = dmgDone * s.AffectValue / 100;
                    log.DebugFormat("Also dealing {0}% (={1}) damage to the hero", s.AffectValue, dmgToHero);
                    DamageHero(attackingCard.Card.UUID, TargetPlayer, dmgToHero, true);
                }
                else if (s.Type == SkillType.Laceration)
                {
                    defendingCard.Laceration = true;
                }
                else if (s.Type == SkillType.AttackAndHealthDecrease)
                {
                    log.DebugFormat("Reducing attack and health by {0}", s.AffectValue);
                    defendingCard.CurrentAttack -= s.AffectValue;
                    DamageCard(attackingCard.Card.UUID, defendingCard, s.AffectValue);
                }
                else if (s.Type == SkillType.AttackDecrease)
                {
                    log.DebugFormat("Reducing attack by {0}", s.AffectValue);
                    defendingCard.CurrentAttack -= s.AffectValue;
                }
                else if (s.Type == SkillType.PercentageDamageHeal)
                {
                    // TODO Verify rounding ingame.
                    // 90% leech
                    // CardModifyHP atk_5 [def_3] --> -285
                    // Bloodsucker atk_5 [atk_5] --> 256
                    // CardModifyHP atk_5 [atk_5] --> 256 
                    // Seems rounded down, like I already did.
                    int healthLeeched = dmgDone * s.AffectValue / 100;
                    log.DebugFormat("Leeching {0}% ({1})", s.AffectValue, healthLeeched);
                    HealCard(attackingCard.Card.UUID, attackingCard, healthLeeched);
                    
                }
                else
                {
                    log.WarnFormat("UNHANDLED AFTER ATTACK skill {0} {1} {2} {3}", s.Name, s.LanchCondition, s.SkillCategory, s.Type);
                }
            }

            foreach (Skill s in defendingCard.ActiveSkills().FindAll(x => x.LanchType == LaunchType.AfterDefend))
            {
                log.DebugFormat("Using AfterDefend skill {0} {1} {2} {3}", s.Name, s.LanchCondition, s.SkillCategory, s.Type);
                if (s.Type == SkillType.CounterAttack)
                {
                    CounterAttack(defendingCard.Card.UUID, i, attackingCard, s, dmgDone);
                }
                else if (s.Type == SkillType.Leech)
                {
                    int leech = attackingCard.CurrentAttack * s.AffectValue / 100;
                    log.DebugFormat("Leeched {0} attack", leech);
                    attackingCard.CurrentAttack -= leech;
                    defendingCard.CurrentAttack += leech;
                }
                else if (s.Type == SkillType.AttackIncreaseAfterReceivingDamage)
                {
                    log.DebugFormat("Craze - increasing damage by {0} (skill id: {1})", s.AffectValue, s.GetHashCode());
                    defendingCard.CrazeSources.Add(s.GetHashCode());
                    defendingCard.CurrentAttack += s.AffectValue;
                }
                else if (s.Type == SkillType.Burn)
                {
                    //new SkillExecutor(s, TargetPlayer, CurrentPlayer, defendingCard);

                    string src = defendingCard.Name + defendingCard.GetHashCode() + "_" + s.Name;
                    attackingCard.AddBurn(src, s.AffectValue);
                }
                else if (s.Type == SkillType.ShieldOfEarth)
                {
                    if (attackingCard.HasEvasion || attackingCard.Race == Race.Hydra || attackingCard.Race == Race.Demon)
                    {
                        log.DebugFormat("Immune to Shield of Earth");
                    }
                    else
                    {
                        log.DebugFormat("Shield of Earth - stunned!");
                        attackingCard.Stunned = true;
                    }
                }
                else
                {
                    log.ErrorFormat("Unhandled AfterDefend skill {0}", s.Name);
                }
            }
        }

        private void UseSkills(int i, BattleCard attackingCard, BattleCard defendingCard)
        {
            //  If not frozen:
            //    a. Use skill 1
            //    b. Use skill 2
            //    c. Use skill 3
            foreach (Skill s in attackingCard.ActiveSkills().FindAll(x => x.LanchType == LaunchType.OnSkillExecutePhase))
            {
                UseSkill(i, attackingCard, defendingCard, s);
            }
        }

        private void UseSkill(int i, BattleCard attackingCard, BattleCard defendingCard, Skill s)
        {
            log.DebugFormat("Using skill {0} {1} {2} {3}", s.Name, s.LanchCondition, s.SkillCategory, s.Type);
            var skillExecute = new SkillExecutor(this, s, CurrentPlayer, TargetPlayer, attackingCard);
            
            if (s.Type == SkillType.Resurrection)
            {
                if (s.AffectType == 39)
                {
                    Reanimate(CurrentPlayer);
                }
            }

            if (TargetPlayer.Battlefield.Count > 0)
            {
                if (s.Type == SkillType.Exile)
                {
                    Exile(attackingCard.Card.UUID, i, defendingCard, TargetPlayer);
                }
            }
        }

        private void Reanimate(BattleUser player)
        {
            if (player.Cemetary.Count > 0)
            {
                var validReanimationTargets = player.Cemetary.FindAll(x => !x.HasImmunity && !x.HasReanimation);
                if (validReanimationTargets.Count > 0)
                {
                    BattleCard bc = validReanimationTargets.TakeRandom(1).ElementAt(0);
                    log.DebugFormat("Reanimating {0}", bc);
                    bc.CurrentWaitTime = 0;
                    ToBattlefield(bc, player);
                    if (StoreReplay) AddOp(OpType.CemetaryToBattlefield, bc.Card.UUID, new List<string>() { bc.Card.UUID }, 0);
                }
            }
        }

        private void CounterAttack(string sourceUUID, int i, BattleCard target, Skill s, int damageDone)
        {
            int counterDamage = s.AffectValue;

            if (s.AffectValue2 > 0)
            {
                int percentageCounterDamage = damageDone * s.AffectValue2 / 100;
                counterDamage += percentageCounterDamage;
            }

            // Counterattack / Retaliation
            CounterAttack(sourceUUID, target, counterDamage);

            if (s.AffectType == 4)
            {
                // Retal against adjenct cards
                if (CurrentPlayer.Battlefield[i - 1] != null)
                {
                    // left neighbour
                    BattleCard neighbour = CurrentPlayer.Battlefield[i - 1];
                    log.DebugFormat("Retaliating left target");
                    CounterAttack(sourceUUID, neighbour, counterDamage);
                }

                // TODO: Check if this works properly if a card has died, but has not been removed yet.
                if (CurrentPlayer.Battlefield[i + 1] != null)
                {
                    // right neighbour
                    BattleCard neighbour = CurrentPlayer.Battlefield[i + 1];
                    log.DebugFormat("Retaliating right target");
                    CounterAttack(sourceUUID, neighbour, counterDamage);
                }
            }
        }

        private void CounterAttack(string sourceUUID, BattleCard target, int damage)
        {
            Skill dexterity = target.ActiveSkills().Find(x => x.AffectType == 82);
            if (dexterity != null)
            {
                if (Util.PercentChance(dexterity.AffectValue))
                {
                    log.DebugFormat("Dexterity: dodging counterattack / retaliate");
                    return;
                }
            }
            log.DebugFormat("Countering {0} with {1} damage", target.Name, damage);
            DamageCard(sourceUUID, target, damage);
        }

        private void MultiHit(int i, BattleCard attackingCard, BattleCard defendingCard, int dmgDone, Skill s)
        {
            // because multihit is actually after hit...
            log.DebugFormat("Going to activate 'pre'-attack MultiHit skill {0}", s.Name);
            if (s.AffectType == 23)
            {
                // Clean sweep
                if (i > 0 && TargetPlayer.Battlefield[i - 1] != null)
                {
                    // left neighbour
                    BattleCard neighbour = TargetPlayer.Battlefield[i - 1];
                    int d = Attack(i, attackingCard, neighbour, dmgDone, true);
                    log.DebugFormat("Did {0} damage to left target", d);
                    AfterDamagingAttackSkills(i, attackingCard, neighbour, d);
                }

                if (i < TargetPlayer.Battlefield.Count - 1 && TargetPlayer.Battlefield[i + 1] != null)
                {
                    // right neighbour
                    BattleCard neighbour = TargetPlayer.Battlefield[i + 1];
                    int d = Attack(i, attackingCard, neighbour, dmgDone, true);
                    log.DebugFormat("Did {0} damage to right target", d);
                    AfterDamagingAttackSkills(i, attackingCard, neighbour, d);
                }
            }
            else if (s.AffectType == 70)
            {
                // Chain attack
                foreach (var c in TargetPlayer.Battlefield.ToList().FindAll(x => x != defendingCard && x.Name == defendingCard.Name))
                {
                    int d = dmgDone * s.AffectValue / 100;
                    log.DebugFormat("Chain attack {0} damage to {1}", d, c);
                    Attack(i, attackingCard, c, d, true);
                }
            }
        }


        private void Exile(string sourceUUID, int i, BattleCard defendingCard, BattleUser player)
        {
            if (defendingCard != null)
            {
                log.DebugFormat("Exiling {0}", defendingCard);
                if (StoreReplay) AddOp(OpType.Exile, sourceUUID, new List<string>() { defendingCard.Card.UUID }, 0);
                if (defendingCard.HasImmunity || defendingCard.HasResistance)
                {
                    log.DebugFormat("NOT");
                }
                else
                {
                    Buffs.RemoveBuffs(defendingCard, player);
                    if (StoreReplay) AddOp(OpType.BattlefieldToLibrary, defendingCard.Card.UUID, new List<string>() { sourceUUID }, 0);
                    player.Battlefield.Remove(defendingCard);
                    if (!defendingCard.IsSummoned)
                    {
                        player.Deck.Add(defendingCard);
                    }
                }
            }
        }

        private void SwitchPlayers()
        {
            if (CurrentPlayer == AttackPlayer)
            {
                CurrentPlayer = DefendPlayer;
                TargetPlayer = AttackPlayer;
            }
            else
            {
                CurrentPlayer = AttackPlayer;
                TargetPlayer = DefendPlayer;
            }
        }

        private void PutCardsOnBattlefield()
        {
            // Use a for-loop so we can safely remove items from the list.
            for (int i = 0; i < CurrentPlayer.Hand.Count; )
            {
                BattleCard b = CurrentPlayer.Hand[i];
                if (b.CurrentWaitTime <= 0)
                {
                    if (StoreReplay) AddOp(OpType.PutOnBattlefield, b.Card.UUID, new List<string>() { b.Card.UUID }, 0);
                    ToBattlefield(b, CurrentPlayer);
                }
                else
                {
                    i++;
                }
            }
        }

        public Dictionary<string, UserCard> summonedCards = new Dictionary<string, UserCard>();

        public void SummonCard(BattleCard b, BattleUser player)
        {
            // The SkillExecutor gives the summoned card an UUID based on the card that summons him and some more to prevent collisions. 
            // But for the history, we need to use UUIDs atk_11+ and also add the summoned cards to the initial card list.
            // Determine a new name for the summoned card, and store the summoned card so when we convert to history, we have these correct names.

            // ok screw this for now. Just add the summoned card to the list and maybe convert it later
            summonedCards[b.Card.UUID] = b.Card;
            /*
             * 
            string prefix = "atk_";
            if (player == DefendPlayer)
            {
                prefix = "def_";
            }

            var mine = summonedCards.Keys.ToList().FindAll(x => x.StartsWith(prefix));
            */

            if (StoreReplay) AddOp(OpType.VoidToBattlefield, b.Card.UUID, new List<string>() { b.Card.UUID }, 0);
            ToBattlefield(b, player);
        }

        public void ToBattlefield(BattleCard b, BattleUser player)
        {
            player.Battlefield.Add(b);
            player.Hand.Remove(b);
            player.Cemetary.Remove(b);
            b.FirstAttack = true;

            if (b.HasGuard)
            {
                player.Guards.Add(b);
            }

            BattleUser other = (player == TargetPlayer ? CurrentPlayer : TargetPlayer);

            // Perform 'Upon entering battlefield' skill now
            foreach (Skill s in b.ActiveSkills().FindAll(x => x.LanchType == LaunchType.OnEnterBattlefield))
            {
                log.DebugFormat("Activating OnEnterBattlefield skill {0} {1} {2} {3} {4}", s.Name, s.LanchCondition, s.SkillCategory, s.Type, s.AffectValue);
                new SkillExecutor(this, s, player, other, b);
                if (s.Type == SkillType.Exile)
                {
                    int myPos = player.Battlefield.Count - 1;
                    var target = other.Battlefield[myPos];
                    log.DebugFormat("{0} -> pos: {1} target: {2}", s.Name, myPos, target);
                    Exile(b.Card.UUID, myPos, target, other);
                }
            }

            Buffs.AddBuffs(b, player);
        }

        private void ReduceWaitTime()
        {
            if (StoreReplay) AddOp(OpType.ReduceAllWaitTime, null, null, -1);
            foreach (BattleCard b in AttackPlayer.Hand)
            {
                b.CurrentWaitTime--;
            }
            foreach (BattleCard b in DefendPlayer.Hand)
            {
                b.CurrentWaitTime--;
            }
        }

        private void DrawCard()
        {

            if (CurrentPlayer.Deck.Count > 0 && CurrentPlayer.Hand.Count <= MAX_HAND_SIZE)
            {
                BattleCard card = CurrentPlayer.Deck[0];
                card.Reset();
                CurrentPlayer.Hand.Add(card);
                if (StoreReplay) AddOp(OpType.DrawCard, card.Card.UUID, new List<string>(), 0);
                CurrentPlayer.Deck.RemoveAt(0);
            }
        }

        public void DamageCard(string sourceUUID, BattleCard target, int damage)
        {
            if (target.LastChanceActive)
            {
                log.Debug("Should not be able to deal damage here due to last chance being active");
                return;
            }
            // Can never deal more damage then the card has health
            damage = Math.Min(target.CurrentHP, damage);
            if (StoreReplay) AddOp(OpType.CardModifyHP, sourceUUID, new List<string>() { target.Card.UUID }, damage * -1);
            target.CurrentHP -= damage;

            if (target.CurrentHP == 0)
            {
                Die(target);
            }
        }

        public void HealCard(string sourceUUID, BattleCard target, int amount)
        {
            log.DebugFormat("{0} is healing card {1} (HP: {2}/{3}) for {4} hp", sourceUUID, target.Name, target.CurrentHP, target.BaseHP, amount);

            // Ensure the card will not be healed above its maximum hp including bonusses. 
            int maxHp = target.BaseHP + target.BonusSkillHP;
            int missingHp = maxHp - target.CurrentHP;

            amount = Math.Min(missingHp, amount);

            if (StoreReplay) AddOp(OpType.CardModifyHP, sourceUUID, new List<string>() { target.Card.UUID }, amount);
            target.CurrentHP += amount;
            log.DebugFormat("Healed {0} hp. Current hp: {1}", amount, target.CurrentHP);
        }

        private void Die(BattleCard defendingCard)
        {
            if (defendingCard.IsDieing)
            {
                return;
            }
            defendingCard.IsDieing = true;
            var index = TargetPlayer.Battlefield.IndexOf(defendingCard);
            bool isAttacker = false;
            BattleUser player;
            BattleUser other;

            if (index != -1)
            {
                log.DebugFormat("Defending card {0} died", defendingCard.Name);
                player = TargetPlayer;
                other = CurrentPlayer;
            }
            else
            {
                index = CurrentPlayer.Battlefield.IndexOf(defendingCard);
                if (index != -1)
                {
                    log.DebugFormat("Attacking card {0} died", defendingCard.Name);
                    isAttacker = true;
                    player = CurrentPlayer;
                    other = TargetPlayer;
                }
                else
                {
                    log.DebugFormat("Did not find card {0} in either attackers or defenders. Probably dead already?", defendingCard);
                    return;
                }
            }

            if (defendingCard.HasGuard)
            {
                player.Guards.Remove(defendingCard);
            }

            if (defendingCard.DeathMark > 0)
            {
                if (TargetPlayer.Battlefield[index - 1] != null) DamageCard(defendingCard.Card.UUID, TargetPlayer.Battlefield[index - 1], defendingCard.DeathMark);
                if (TargetPlayer.Battlefield[index + 1] != null) DamageCard(defendingCard.Card.UUID, TargetPlayer.Battlefield[index + 1], defendingCard.DeathMark);
            }

            foreach (Skill s in defendingCard.ActiveSkills().FindAll(x => x.LanchType == LaunchType.OnDeath))
            {
                log.DebugFormat("Fireing OnDeath skill {0}", s.Name);
                new SkillExecutor(this, s, player, other, defendingCard);
                if (s.Type == SkillType.Resurrection)
                {
                    if (s.AffectType == 53)
                    {
                        Reanimate(player);
                    }
                }
                else if (s.Type == SkillType.LastChance)
                {
                    // Check if it is the first time, and that I died due to damage (not destroy). If it is, activate last chance
                    if (defendingCard.CurrentHP == 0)
                    {
                        if (defendingCard.LastChanceAvailable)
                        {
                            // 48 def_6 [def_6] --> 0
                            // CardModifyHP def_6 [def_6] --> 1
                            log.DebugFormat("Last chance activated...");

                            if (StoreReplay) AddOp(OpType.LastChance, defendingCard.Card.UUID, new List<string>() { defendingCard.Card.UUID }, 0);
                            defendingCard.LastChanceActive = true;
                            defendingCard.LastChanceAvailable = false;
                            defendingCard.IsDieing = false;

                            HealCard(defendingCard.Card.UUID, defendingCard, 1);

                            return;
                        }
                        else
                        {
                            log.Debug("Last chance has already been activated");
                        }
                    }
                }
            }

            Buffs.RemoveBuffs(defendingCard, player);

            player.Battlefield.Remove(defendingCard);
            if (defendingCard.IsSummoned)
            {
                if (StoreReplay) AddOp(OpType.BattlefieldToVoid, defendingCard.Card.UUID, new List<string>(), 0);
            }
            else
            {
                if (StoreReplay) AddOp(OpType.CardDies, defendingCard.Card.UUID, new List<string>(), 0);
                player.Cemetary.Add(defendingCard);

                foreach (Skill s in defendingCard.ActiveSkills().FindAll(x => x.LanchType == LaunchType.AfterDeath))
                {
                    log.DebugFormat("Executing AfterDeath skill {0}", s.Name);
                    new SkillExecutor(this, s, player, other, defendingCard);
                }
            }

            defendingCard.Reset();
        }

        public void Step()
        {

        }

        public void AddOp(core.OpType opp, string uuid, List<string> target, int? value)
        {
            // CurrentRound.Opps.Add(new Op() { Opp = OpType.CemetaryToBattlefield, UUID = bc.Card.UUID, Target = { bc.Card.UUID }, Value = 0 });
            CurrentRound.Opps.Add(new Op() { Opp = opp, UUID = uuid, Value = value, Target = target });
        }
    }
}
