﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdomsGui.Properties;
using ElementalKingdomsGui.ui.controls;
using ElementalKingdoms.core;

namespace ElementalKingdoms.ui
{
    public partial class BattleFormOptionsDialog : Form
    {
        public BattleFormOptionsDialog()
        {
            InitializeComponent();
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);

            SetCardId(editHydraMeritCardButton1, Settings.Default.hydraMeritCardId1);
            SetCardId(editHydraMeritCardButton2, Settings.Default.hydraMeritCardId2);
            SetCardId(editHydraMeritCardButton3, Settings.Default.hydraMeritCardId3);
        }

        private void SetCardId(EditCardButton btn, int cardId)
        {
            if (cardId > 0)
            {
                var card = Card.Get(cardId);
                if (card != null)
                {
                    btn.EditCard = new UserCard() { CardId = card.Id };
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Settings.Default.hydraMeritCardId1 = editHydraMeritCardButton1.EditCard.CardId;
            Settings.Default.hydraMeritCardId2 = editHydraMeritCardButton2.EditCard.CardId;
            Settings.Default.hydraMeritCardId3 = editHydraMeritCardButton3.EditCard.CardId;
            Settings.Default.Save();

            Game.TheGame.HydraMeritCardIds.Clear();
            Game.TheGame.HydraMeritCardIds.AddRange(new[] { Settings.Default.hydraMeritCardId1, Settings.Default.hydraMeritCardId2, Settings.Default.hydraMeritCardId3 }.Where(x => x != 0));
        }
    }
}
