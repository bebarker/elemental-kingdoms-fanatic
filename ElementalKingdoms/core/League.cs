﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class League
    {
        public static League CurrentLeague;
        public static League NextLeague;

        [Newtonsoft.Json.JsonProperty("LeagueId")]
        public int LeagueId { get; set; }
        public int ConditionId { get; set; }
        public int RoundNow { get; set; }
        public int Status { get; set; }
        public long CreateTime { get; set; }
        public LeagueCondition Condition { get; set; }
        public int RemainTime { get; set; }

        public List<List<LeagueRoundResult>> RoundResult { get; set; }

        public class LeagueCondition
        {
            public string Type { get; set; }
            //public int Value { get; set; }
            public string Desc { get; set; }
        }

        public class LeagueRoundResult
        {
            public int Uid { get; set; }
            public int Opponent { get; set; }
            public string BattleId { get; set; }
            public float BetOdds { get; set; }
            public long BetTotal { get; set; }

            public LeagueBattleInfo BattleInfo { get; set; }
        }

        public class LeagueBattleInfo
        {
            public User User { get; set; }
            public List<UserCard> Cards { get; set; }
            public List<UserRune> Runes { get; set; }
        }
    }
}
