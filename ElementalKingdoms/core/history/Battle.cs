﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ElementalKingdoms.core.history
{
    public enum BattleResult
    {
        InProgress = 0,
        Won = 1,
        Lost = 2
    }

    public class Battle
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string BattleId { get; set; }
        public BattleResult Win { get; set; }
        //public ExtData ExtData { get; set; }

        //[JsonProperty("prepare")]
        public string[] Prepare { get; set; }

        public BattlePlayer AttackPlayer { get; set; }
        public BattlePlayer DefendPlayer { get; set; }

        [JsonProperty("Battle")]
        public List<BattleRound> Rounds { get; set; }

        private Dictionary<int, BattleState> states;

        public Battle()
        {
            states = new Dictionary<int, BattleState>();
        }

        public BattleState GetState(int round)
        {
            if (round < 1 || round > Rounds.Count) throw new ArgumentException("round must be between 1 and " + Rounds.Count);

            return calculateRoundState(round);
        }

        private BattleState calculateRoundState(int round)
        {
            if (states.ContainsKey(round))
            {
                return states[round];
            }

            StringBuilder sb = new StringBuilder();
            for (int i = 1; i <= round; i++)
            {
                if (states.ContainsKey(i))
                {
                    continue;
                }
                BattleState s;
                if (i == 1)
                {
                    s = new BattleState();

                    s.Attacker.HP = int.Parse(AttackPlayer.HP);
                    s.Attacker.MaxHP = s.Attacker.HP;
                    s.Defender.HP = int.Parse(DefendPlayer.HP);
                    s.Defender.MaxHP = s.Defender.HP;


                    foreach (var card in AttackPlayer.Cards)
                    {
                        s.Attacker.Cards.Add(new BattleCard(card));
                    }
                    foreach (var card in DefendPlayer.Cards)
                    {
                        s.Defender.Cards.Add(new BattleCard(card));
                    }
                }
                // continue where we have left
                else
                {
                    s = states[i - 1].Clone();
                }
                // calculate details about the current state

                s.Round = i;

                BattleRound r = Rounds[i - 1];
                sb.AppendLine("------------------------");
                sb.AppendFormat("Round {0} ({1})", r.Number, r.isAttack ? "attacker" : "defender").AppendLine();
                sb.AppendLine("------------------------");
                
                s.IsAttack = r.isAttack;

                foreach (Op op in r.Opps)
                {
                    sb.AppendLine(op.ToString());
                    s.Ops.Add(op.ToString());

                    switch (op.Opp)
                    {
                        case OpType.Bloodsucker:
                            break;
                        case OpType.RuneActivate:
                            break;
                        case OpType.TrapAttack:
                            break;
                        case OpType.ElectricAttack:
                            break;
                        case OpType.ColdAttack:
                            break;
                        case OpType.FireAttack:
                            break;
                        case OpType.BloodAttack:
                            break;
                        case OpType.PoisonAttack:
                            break;
                        case OpType.Parry:
                            break;
                        case OpType.Heal:
                            break;
                        case OpType.Snipe:
                            break;
                        case OpType.Multihit:
                            break;
                        case OpType.AttackIncrease:
                            break;
                        case OpType.MaxHPIncrease:
                            break;
                        case OpType.Reincarnation:
                            break;
                        case OpType.AttackDecrease:
                            break;
                        case OpType.Selfdestruct:
                            break;
                        case OpType.HeroHeal:
                            break;
                        case OpType.HeroDamage:
                            break;
                        case OpType.Bloodthirst:
                            break;
                        case OpType.Craze:
                            break;
                        case OpType.Plague:
                            break;
                        case OpType.Teleportation:
                            break;
                        case OpType.Reflection:
                            break;
                        case OpType.Exile:
                            break;
                        case OpType.Destroy:
                            break;
                        case OpType.Burn:
                            break;
                        case OpType.IceShield:
                            break;
                        case OpType.Laceration:
                            break;
                        case OpType.MagicShield:
                            break;
                        case OpType.Resistance:
                            break;
                        case OpType.Immune:
                            break;
                        case OpType.Guard:
                            break;
                        case OpType.Dodge:
                            break;
                        case OpType.Sacrifice:
                            break;
                        case OpType.Puncture:
                            break;
                        case OpType.Confusion:
                            break;
                        case OpType.Leech:
                            break;
                        case OpType.ManaCorruption:
                            break;
                        case OpType.SacredFlame:
                            break;
                        ///
                        /// Card location related events
                        /// 
                        case OpType.DrawCard:
                            MoveCard(s, op.UUID, Location.Hand);
                            break;
                        case OpType.PutOnBattlefield:
                            MoveCard(s, op.UUID, Location.Battlefield);
                            break;
                        case OpType.CardDies:
                            MoveCard(s, op.UUID, Location.Cemetary);
                            break;
                        case OpType.CemetaryToDeck:
                            MoveCard(s, op.UUID, Location.Deck);
                            break;
                        case OpType.CemetaryToHand:
                            MoveCard(s, op.UUID, Location.Hand);
                            break;
                        case OpType.CemetaryToBattlefield:
                            MoveCard(s, op.UUID, Location.Battlefield);
                            break;
                        case OpType.BattlefieldToLibrary:
                            MoveCard(s, op.UUID, Location.Deck);
                            break;
                        case OpType.BattlefieldToVoid:
                            MoveCard(s, op.UUID, Location.None);
                            break;
                        case OpType.VoidToBattlefield:
                            MoveCard(s, op.UUID, Location.Battlefield);
                            break;
                        /// end of location related events
                        case OpType.CardModifyAttack:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.CurrentTotalAttack += op.Value.GetValueOrDefault();
                                log.DebugFormat("Changed attack of {0} to {1} ({2})", x.Name, x.CurrentTotalAttack, op.Value.GetValueOrDefault());
                            });
                            break;
                        case OpType.ReduceAllWaitTime:
                            foreach (var card in s.Attacker.Cards.FindAll(x => x.Location == Location.Hand))
                            {
                                card.CurrentWaitTime += op.Value.GetValueOrDefault();
                            }
                            foreach (var card in s.Defender.Cards.FindAll(x => x.Location == Location.Hand))
                            {
                                card.CurrentWaitTime += op.Value.GetValueOrDefault();
                            }
                            break;
                        case OpType.HeroModifyHP:
                            if (op.Target[0] == "atk")
                            {
                                s.Attacker.HP = Math.Min(s.Attacker.HP + op.Value.GetValueOrDefault(), s.Attacker.MaxHP);
                            }
                            else if (op.Target[0] == "def")
                            {
                                s.Defender.HP = Math.Min(s.Defender.HP + op.Value.GetValueOrDefault(), s.Defender.MaxHP);
                            }
                            break;
                        case OpType.SetWaitTime:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.CurrentWaitTime = op.Value.GetValueOrDefault();
                                log.DebugFormat("Set waittime of {0} to {1}", x.Name, x.CurrentWaitTime);
                            });
                            break;
                        case OpType.AttackCardDealDamage:
                            break;
                        case OpType.CardModifyHP:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.CurrentHP += op.Value.GetValueOrDefault();
                                log.DebugFormat("Changed HP of {0} to {1} ({2})", x.Name, x.CurrentHP, op.Value.GetValueOrDefault());
                            });
                            break;
                        case OpType.RuneExpires:
                            break;
                        case OpType.XXXXXXXX1060XXXXXXX:
                            CleanEmptyCardIndexes(s);
                            break;
                        case OpType.Trapped:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.Trapped = op.Value.GetValueOrDefault() == 1;
                                log.DebugFormat("{0} is {1}Trapped!", x.Name, op.Value.GetValueOrDefault() == 1 ? "no longer " : "");
                            });   
                            break;
                        case OpType.Paralyzed:
                            DoSomethingWithAllCards(s, op.Target, (x) => 
                            { 
                                x.Paralyzed = op.Value.GetValueOrDefault() == 1;
                                log.DebugFormat("{0} is {1}Paralyzed!", x.Name, op.Value.GetValueOrDefault() == 1 ? "no longer " : "");
                            });                            
                            break;
                        case OpType.Frozen:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.Frozen = op.Value.GetValueOrDefault() == 1;
                                log.DebugFormat("{0} is {1}Frozen!", x.Name, op.Value.GetValueOrDefault() == 1 ? "no longer " : "");
                            });    
                            break;
                        case OpType.Poisoned:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.Poisoned = op.Value.GetValueOrDefault() == 1;
                                log.DebugFormat("{0} is {1}Poisoned!", x.Name, op.Value.GetValueOrDefault() == 1 ? "no longer " : "");
                            });   
                            break;
                        case OpType.Sealed:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.Trapped = op.Value.GetValueOrDefault() == 1;
                                log.DebugFormat("{0} is {1}Sealed!", x.Name, op.Value.GetValueOrDefault() == 1 ? "no longer " : "");
                            });   
                            break;
                        case OpType.Lacerated:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.Laceration = op.Value.GetValueOrDefault() == 1;
                                log.DebugFormat("{0} is {1}Lacerated!", x.Name, op.Value.GetValueOrDefault() == 1 ? "no longer " : "");
                            });   
                            break;
                        case OpType.Burned:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.Burned = op.Value.GetValueOrDefault() == 1;
                                log.DebugFormat("{0} is {1}Burned!", x.Name, op.Value.GetValueOrDefault() == 1 ? "no longer " : "");
                            });
                            break;
                        case OpType.Confused:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.Confused = op.Value.GetValueOrDefault() == 1;
                                log.DebugFormat("{0} is {1}Confused!", x.Name, op.Value.GetValueOrDefault() == 1 ? "no longer " : "");
                            });
                            break;
                        case OpType.Silenced:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.Silenced = op.Value.GetValueOrDefault() == 1;
                                log.DebugFormat("{0} is {1}Silenced!", x.Name, op.Value.GetValueOrDefault() == 1 ? "no longer " : "");
                            });
                            break;
                        case OpType.Stunned:
                            DoSomethingWithAllCards(s, op.Target, (x) =>
                            {
                                x.Stunned = op.Value.GetValueOrDefault() == 1;
                                log.DebugFormat("{0} is {1}Stunned!", x.Name, op.Value.GetValueOrDefault() == 1 ? "no longer " : "");
                            });
                            break;
                        /// Only for animations
                        case OpType.WaitTimeDecrease:
                        case OpType.WaitTimeIncrease:
                            break;
                        default:
                            log.WarnFormat("Unknown op: {0}", op.Opp);
                            break;
                    }
                }
                states.Add(i, s);
            }

            //Console.WriteLine(sb.ToString());


            return states[round];
        }

        private void DoSomethingWithAllCards(BattleState s, List<string> opTargets, Action<BattleCard> something)
        {
            foreach (string tar in opTargets)
            {
                foreach (var card in s.Attacker.Cards.FindAll(x => x.Card.UUID == tar))
                {
                    something(card);
                }
                foreach (var card in s.Defender.Cards.FindAll(x => x.Card.UUID == tar))
                {
                    something(card);
                }
            }
        }

        private void CleanEmptyCardIndexes(BattleState s)
        {
            CleanEmptyCardIndexes(s.Attacker.Cards.FindAll(x => x.Location == Location.Hand).OrderBy(x => x.LocationIndex).ToList());
            CleanEmptyCardIndexes(s.Attacker.Cards.FindAll(x => x.Location == Location.Battlefield).OrderBy(x => x.LocationIndex).ToList());

            CleanEmptyCardIndexes(s.Defender.Cards.FindAll(x => x.Location == Location.Hand).OrderBy(x => x.LocationIndex).ToList());
            CleanEmptyCardIndexes(s.Defender.Cards.FindAll(x => x.Location == Location.Battlefield).OrderBy(x => x.LocationIndex).ToList());
        }

        private void CleanEmptyCardIndexes(List<BattleCard> cards)
        {
            for (int i = 0; i < cards.Count; i++)
            {
                if (cards[i].LocationIndex != i)
                {
                    log.DebugFormat("Settings location for {0} in {1} from {2} to {3}", cards[i].Name, cards[i].Location, cards[i].LocationIndex, i);
                }
                cards[i].LocationIndex = i;
            }
        }

        private void MoveCard(BattleState s, string UUID, Location location)
        {
            BattleCard card = null;
            List<BattleCard> allCards = null;
            if (UUID.StartsWith("atk"))
            {
                allCards = s.Attacker.Cards;
            }
            else if (UUID.StartsWith("def"))
            {
                allCards = s.Defender.Cards;
            }
            else
            {
                throw new Exception(String.Format("UUID {0} is not recognized as attacker or defender!", UUID));
            }

            card = allCards.Find(x => x.Card.UUID == UUID);
            if (card == null)
            {
                throw new Exception(String.Format("Op for UUID {0} was not found!", UUID));
            }
            Location prevLocation = card.Location; //just for debug logging
            card.Location = location;
            card.LocationIndex = -1;
            if (location == Location.Hand || location == Location.Battlefield || location == Location.Cemetary)
            {
                card.LocationIndex = allCards.FindAll(x => x.Location == location).Max(x => x.LocationIndex) + 1;
            }

            if (card.Location != Location.Battlefield && card.Location != Location.Hand)
            {
                card.Reset();
            }

            log.DebugFormat("Moving card {0} from {1} to {2} ({3})", card.Name, prevLocation, location, card.LocationIndex);
        }

        public string GetLog()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Battle ID: {0}", BattleId).AppendLine();
            sb.AppendFormat("{0} (level {1}) vs {2} (level {3})", AttackPlayer.NickName, AttackPlayer.Level, DefendPlayer.NickName, DefendPlayer.Level).AppendLine();
            sb.AppendFormat("Battle {0}!", this.Win).AppendLine();

            foreach (BattleRound round in Rounds)
            {
                sb.AppendLine("------------------------");
                sb.AppendFormat("Round {0} ({1})", round.Number, round.isAttack ? "attacker" : "defender").AppendLine();
                sb.AppendLine("------------------------");

                foreach (Op op in round.Opps)
                {
                    sb.AppendLine(op.ToString());
                }
            }

            return sb.ToString();
        }

        public void Print()
        {
            log.InfoFormat("Battle ID: {0}", BattleId);
            log.InfoFormat("{0} (level {1}) vs {2} (level {3})", AttackPlayer.NickName, AttackPlayer.Level, DefendPlayer.NickName, DefendPlayer.Level);
            log.InfoFormat("Battle {0}!", this.Win);
            foreach (BattleRound round in Rounds)
            {
                log.InfoFormat("------------------------");
                log.InfoFormat("Round {0} ({1})", round.Number, round.isAttack ? "attacker" : "defender");
                log.InfoFormat("------------------------");

                foreach (Op op in round.Opps)
                {
                    log.InfoFormat(op.ToString());
                }

            }
        }
        /*
		"Battle": [{
			"Round": 5,
			"isAttack": true,
			"Opps": [{
				"UUID": "atk_3",
				"Opp": 1002,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "atk_1",
				"Opp": 1030,
				"Target": ["def_2"],
				"Value": 85
			},
			{
				"UUID": "atk_1",
				"HP": 250,
				"Opp": 1040,
				"Target": ["def_2"],
				"Value": -85
			},
			{
				"UUID": "atk_3",
				"Opp": 1030,
				"Target": ["def"],
				"Value": 125
			},
			{
				"UUID": "atk_3",
				"Opp": 1022,
				"Target": ["def"],
				"Value": -125
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			}]
		},
		{
			"Round": 6,
			"isAttack": false,
			"Opps": [{
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "def_1",
				"Opp": 1002,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "def_2",
				"Opp": 1030,
				"Target": ["atk_1"],
				"Value": 110
			},
			{
				"UUID": "def_2",
				"HP": 190,
				"Opp": 1040,
				"Target": ["atk_1"],
				"Value": -110
			},
			{
				"UUID": "def_1",
				"Opp": 1030,
				"Target": ["atk_3"],
				"Value": 110
			},
			{
				"UUID": "def_1",
				"HP": 370,
				"Opp": 1040,
				"Target": ["atk_3"],
				"Value": -110
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			}]
		},
		{
			"Round": 7,
			"isAttack": true,
			"Opps": [{
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "atk_2",
				"Opp": 1002,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "atk_1",
				"Opp": 1030,
				"Target": ["def_2"],
				"Value": 85
			},
			{
				"UUID": "atk_1",
				"HP": 165,
				"Opp": 1040,
				"Target": ["def_2"],
				"Value": -85
			},
			{
				"UUID": "atk_3",
				"Opp": 1030,
				"Target": ["def_1"],
				"Value": 125
			},
			{
				"UUID": "atk_3",
				"HP": 250,
				"Opp": 1040,
				"Target": ["def_1"],
				"Value": -125
			},
			{
				"UUID": "atk_2",
				"Opp": 1030,
				"Target": ["def"],
				"Value": 125
			},
			{
				"UUID": "atk_2",
				"Opp": 1022,
				"Target": ["def"],
				"Value": -125
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			}]
		},
		{
			"Round": 8,
			"isAttack": false,
			"Opps": [{
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "def_2",
				"Opp": 1030,
				"Target": ["atk_1"],
				"Value": 110
			},
			{
				"UUID": "def_2",
				"HP": 80,
				"Opp": 1040,
				"Target": ["atk_1"],
				"Value": -80
			},
			{
				"UUID": "atk_1",
				"Opp": 1003,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "def_1",
				"Opp": 1030,
				"Target": ["atk_3"],
				"Value": 110
			},
			{
				"UUID": "def_1",
				"HP": 260,
				"Opp": 1040,
				"Target": ["atk_3"],
				"Value": -110
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			}]
		},
		{
			"Round": 9,
			"isAttack": true,
			"Opps": [{
				"UUID": "",
				"Opp": 1021,
				"Target": null,
				"Value": -1
			},
			{
				"UUID": "",
				"Opp": 1060,
				"Target": [],
				"Value": 3
			},
			{
				"UUID": "atk_3",
				"Opp": 1030,
				"Target": ["def_2"],
				"Value": 125
			},
			{
				"UUID": "atk_3",
				"HP": 80,
				"Opp": 1040,
				"Target": ["def_2"],
				"Value": -80
			},
			{
				"UUID": "def_2",
				"Opp": 1003,
				"Target": null,
				"Value": 0
			},
			{
				"UUID": "atk_2",
				"Opp": 1030,
				"Target": ["def_1"],
				"Value": 125
			},
			{
				"UUID": "atk_2",
				"HP": 125,
				"Opp": 1040,
				"Target": ["def_1"],
				"Value": -125
			},
			{
				"UUID": "def_1",
				"Opp": 1003,
				"Target": null,
				"Value": 0
			}]
		}]
	},
	"version": {
		"http": "20130546",
		"stop": "",
		"appversion": "version_1",
		"appurl": "ios:\/\/xxx"
	},
	"AchievementInfos": [{
		"AchievementId": "24",
		"AchieveOrder": "24",
		"Firstid": "24",
		"Lastid": "24",
		"Condition": "Win your first match.",
		"Name": "First Victory",
		"Amount": "1",
		"Pic": "",
		"RewardValue": "10000",
		"RewardType": "Coins",
		"Point": "10"
	},
	{
		"AchievementId": "32",
		"AchieveOrder": "32",
		"Firstid": "32",
		"Lastid": "112",
		"Condition": "Complete 1-1.",
		"Name": "Ebon Hold 1-Star",
		"Amount": "1",
		"Pic": "",
		"RewardValue": "1000",
		"RewardType": "Coins",
		"Point": "10"
	}]
}*/
    }
}
