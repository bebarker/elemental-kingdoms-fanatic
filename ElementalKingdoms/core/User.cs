﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public enum Gender
    {
        Male = 0,
        Female = 1
    }
    public class User
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int Uid { get; set; }
        public string NickName { get; set; }
        public Gender Sex { get; set; }
        public int Avatar { get; set; }
        public int HP { get; private set; }

        [Newtonsoft.Json.JsonProperty("Coins")]
        public int Gold { get; set; }
        [Newtonsoft.Json.JsonProperty("Cash")]
        public int Gems { get; set; }
        [Newtonsoft.Json.JsonProperty("Ticket")]
        public int FireTokens { get; set; }

        public int Energy { get; set; }
        public int EnergyMax { get; set; }

        // HP = 1000 + (Level-1) * 70 + (ROUNDDOWN((Level-1)/10;0) * (Level-1) * 10)
        private int level;
        public int Level 
        {
            get { return level; }
            set { 
                level = value;
                if (level == 0)
                {
                    HP = 100000000;
                }
                else
                {
                    if (level <= 100)
                    {
                        HP = 1000 + (level - 1) * 70 + ((level - 1) / 10) * (level - 1) * 10;
                    }
                    else
                    {
                        // After level 100, you do not get the 10th level hp boost anymore. Just calculate again.
                        HP = 17000 + (level - 101) * 160;
                    }
                }
            }
        }
        public int Cost 
        { 
            get 
            {
                if (Level < 21) return 10 + 3 * Level;
                if (Level < 51) return 70 + 2 * (Level - 20);
                return 80 + Level;
            } 
        }

        public List<UserCard> UserCards { get; set; }

        public int CurrentDeckIndex { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public Deck UserDeck {
            get {
                if (AllDecks.Count == 0) { AllDecks.Add(new Deck()); }
                return AllDecks[CurrentDeckIndex];
            }
            set
            {
                for (int i = 0; i < AllDecks.Count; i++)
                {
                    
                    if (AllDecks[i].Equals(value))
                    {
                        CurrentDeckIndex = i;
                        log.DebugFormat("Current deck: {0}", CurrentDeckIndex);
                        return;
                    }

                }
                CurrentDeckIndex = AllDecks.Count;
                log.DebugFormat("New deck {0}", CurrentDeckIndex);
                AllDecks.Add(value);
            }
        }
        public List<Deck> AllDecks { get; set; }

        public Dictionary<int, UserMapStage> UserMapStages { get; set; }

        public User()
        {
            Level = 1;
            UserCards = new List<UserCard>();

            AllDecks = new List<Deck>();
            //AllDecks.Add(new Deck());
        }

        public static User Load()
        {
            User u = new User();
            //string userInfoPath = @"D:\projects\Games\ElementalKingdoms\replies\13\GetUserInfo.json";
            string userInfoPath = @"cache/userinfo.json";

            using (StreamReader streamReader = new StreamReader(userInfoPath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                JObject o = JObject.Parse(response);

                var dataPrt = o.GetValue("data");
                u = dataPrt.ToObject<User>();
            }

            //string userCardsPath = @"D:\projects\Games\ElementalKingdoms\replies\30\GetUserCards.json";
            string userCardsPath = @"cache/usercards.json";
            using (StreamReader streamReader = new StreamReader(userCardsPath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                JObject o = JObject.Parse(response);

                var dataPrt = o.GetValue("data");
                u.UserCards = dataPrt["Cards"].ToObject<List<UserCard>>();
            }

            //string getCardGroupsPath = @"D:\projects\Games\ElementalKingdoms\replies\13\GetCardGroups.json";
            string getCardGroupsPath = @"cache/cardgroup.json";
            using (StreamReader streamReader = new StreamReader(getCardGroupsPath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                JObject o = JObject.Parse(response);

                var dataPrt = o.GetValue("data");
                u.AllDecks = dataPrt["Groups"].ToObject<List<Deck>>();
            }

            string userMapStagesPath = @"cache/usermapstages.json";
            using (StreamReader streamReader = new StreamReader(userMapStagesPath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                JObject o = JObject.Parse(response);

                var dataPrt = o.GetValue("data");
                u.UserMapStages = dataPrt.ToObject<Dictionary<int, UserMapStage>>();
            }

            return u;
        }
        
        public override bool Equals(object obj)
        {
            if (this == null)                        //this is necessary to guard against reverse-pinvokes and
                throw new NullReferenceException();  //other callers who do not use the callvirt instruction

            User other = obj as User;
            if (other == null)
                return false;

            if (Object.ReferenceEquals(this, obj))
                return true;

            return EqualsHelper(this, other);
        }

        private bool EqualsHelper(User userA, User userB)
        {
            if (userA.HP != userB.HP)
                return false;

            return userA.UserDeck.Equals(userB.UserDeck);
        }

        public List<UserRune> UserRunes { get; set; }

        public long GetFohMagicNumber()
        {
            long attackSum = 0;
            long hpSum = 0;
            
            foreach (var card in UserDeck.Cards)
            {
                int attack = card.BaseCard.AttackArray[card.Level];
                int hp = card.BaseCard.HpArray[card.Level];
                attackSum += attack;
                hpSum += hp;
            }

            long magic = HP + attackSum + hpSum;

            log.DebugFormat("{0} | FoH magic number: {1} (CHP {2} + CAT {3} + HP {4})", NickName, magic, hpSum, attackSum, HP);

            return magic;
        }
        
        public bool HasLegendary()
        {
            foreach (var card in UserDeck.Cards)
            {
                if (card.Name.Contains("(Legendary)"))
                {
                    return true;
                }
            }

            return false;
        }

        public bool HasHydra()
        {
            foreach (var card in UserDeck.Cards)
            {
                // Not only Hydra's have race Hydra, also Legendary cards and the Treasure Goblin.
                if (card.BaseCard.Race == Race.Hydra && card.Name.StartsWith("hydra", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
