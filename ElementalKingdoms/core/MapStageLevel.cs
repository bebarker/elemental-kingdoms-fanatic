﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class MapStageLevel
    {
        public int MapStageDetailId { get; set; }
        public int Level { get; set; }
        public string CardList { get; set; }
        public string RuneList { get; set; }
        public int HeroLevel { get; set; }
        public int AchievementId { get; set; }
        public int AchievementId2 { get; set; }
        public int EnergyExpend { get; set; }
        public string BonusWin { get; set; }
        public string FirstBonusWin { get; set; }
        public string BonusLose { get; set; }
        public string AddedBonus { get; set; }
        public int EnergyExplore { get; set; }
        public string BonusExplore { get; set; }
        public int Hint { get; set; }
        public string AchievementText { get; set; }

        public List<UserCard> Cards
        {
            get 
            {
                List<UserCard> cards = new List<UserCard>();

                foreach (string s in CardList.Split(','))
                {
                    var x = s.Split('_');
                    int cardId = int.Parse(x[0]);
                    int level = int.Parse(x[1]);
                    int skillNew = 0;
                    if (x.Length == 3)
                    {
                        skillNew = int.Parse(x[2]);
                    }

                    cards.Add(new UserCard() { CardId = cardId, Level = level, SkillNew = skillNew });
                }

                return cards;
            }
        }

        public List<UserRune> Runes
        {
            get
            {
                List<UserRune> runes = new List<UserRune>();

                foreach (string s in RuneList.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    var x = s.Split('_');
                    runes.Add(new UserRune() { RuneId = int.Parse(x[0]), Level = int.Parse(x[1]) });
                }

                return runes;
            }
        }
    }
}
