﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElementalKingdoms.core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ElementalKingdoms.util
{
    public class JsonLoader
    {

        public static void LoadSkills()
        {
            //string allSkillsFilePath = @"D:\projects\Games\ElementalKingdoms\replies\GetAllSkill.json";
            string allSkillsFilePath = "cache/skills.json";
            using (StreamReader streamReader = new StreamReader(allSkillsFilePath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                JObject o = JObject.Parse(response);

                var dataPrt = o.GetValue("data");
                Skill.Skills = dataPrt["Skills"].ToObject<List<Skill>>();

                Skill.Get(1); // ensure data is cached.

                //var result = t.Find(z => z.SkillId == 514);
                /* // print all skills in tabulated format
                bool first = true;
                foreach (Skill result in t)
                {
                    if (first)
                    {
                        foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(result))
                        {
                            string name = descriptor.Name;
                            sb.AppendFormat("{0}\t", name);
                        }
                        sb.AppendLine();
                        first = false;
                    }

                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(result))
                    {
                        object value = descriptor.GetValue(result);
                        sb.AppendFormat("{0}\t", value);
                    }
                    sb.AppendLine();
                }
                 */

                //var skillz = JsonConvert.DeserializeObject<Skill>(skills);
            }
        }

        public static void LoadLeague()
        {
            //string leagueInfoPath = @"D:\projects\Games\ElementalKingdoms\replies\FieldOfHonor\s10r2.json";
            string leagueInfoPath = "cache/leagueinfo.json";
            if (File.Exists(leagueInfoPath))
            {
                using (StreamReader streamReader = new StreamReader(leagueInfoPath, Encoding.UTF8))
                {
                    var response = streamReader.ReadToEnd();

                    JObject o = JObject.Parse(response);

                    var dataPrt = o["data"];
                    League.CurrentLeague = dataPrt["LeagueNow"].ToObject<League>();
                    League.NextLeague = dataPrt["LeagueNext"].ToObject<League>();
                }
            }
        }

        public static void LoadCards()
        {
            //string allCardsFilePath = @"D:\projects\Games\ElementalKingdoms\replies\GetAllCard.json";
            string allCardsFilePath = "cache/cards.json";
            using (StreamReader streamReader = new StreamReader(allCardsFilePath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                JObject o = JObject.Parse(response);

                var dataPrt = o.GetValue("data");
                Card.Cards = dataPrt["Cards"].ToObject<List<Card>>();

                /* // print all skills in tabulated format
                bool first = true;
                foreach (Card result in cards)
                {
                    if (first)
                    {
                        foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(result))
                        {
                            string name = descriptor.Name;
                            sb.AppendFormat("{0}\t", name);
                        }
                        sb.AppendLine();
                        first = false;
                    }

                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(result))
                    {
                        object value = descriptor.GetValue(result);
                        sb.AppendFormat("{0}\t", value);
                    }
                    sb.AppendLine();
                }
                // */

                //var skillz = JsonConvert.DeserializeObject<Skill>(skills);
            }
        }

        public static void LoadRunes()
        {
            //string allRunesFilePath = @"D:\projects\Games\ElementalKingdoms\replies\GetAllRune.json";
            string allRunesFilePath = "cache/allrunes.json";
            using (StreamReader streamReader = new StreamReader(allRunesFilePath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                JObject o = JObject.Parse(response);

                var dataPrt = o.GetValue("data");
                Rune.Runes = dataPrt["Runes"].ToObject<List<Rune>>();

                //Rune.PrintAll();
            }
        }

        internal static void LoadMapstages()
        {
            //string mapStagesPath = @"D:\projects\Games\ElementalKingdoms\replies\GetMapStageAll.json";
            string mapStagesPath = "cache/mapstages.json";

            using (StreamReader streamReader = new StreamReader(mapStagesPath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                JObject o = JObject.Parse(response);

                var dataPrt = o.GetValue("data");
                MapStage.AllMapStages = dataPrt.ToObject<List<MapStage>>();
            }
        }

        public static core.kingdomwar.KingdomWar LoadKingdomWar()
        {
            string kingdomwarPath = "cache/kingdomwar_map.json";

            using (StreamReader streamReader = new StreamReader(kingdomwarPath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                JObject o = JObject.Parse(response);

                var dataPrt = o.GetValue("data");
                return dataPrt.ToObject<core.kingdomwar.KingdomWar>();
            }

            return null;
        }

        public static core.history.Battle LoadBattle(string battleJsonPath)
        {
            using (StreamReader streamReader = new StreamReader(battleJsonPath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                JObject o = JObject.Parse(response);

                var dataPrt = o.GetValue("data");
                if (dataPrt == null)
                {
                    return null;
                }
                core.history.Battle b = dataPrt.ToObject<core.history.Battle>();

                //b.Print();
                return b;
            }
        }

        public static void SaveBattle(core.history.Battle battle, string file)
        {
            System.IO.Directory.CreateDirectory("cache/battle");
            bla bla = new bla();
            bla.status = 1;
            bla.data = battle;
            string json = JsonConvert.SerializeObject(bla, Formatting.Indented);

            File.WriteAllText(file, json);
        }

        class bla
        {
            public int status { get; set; }
            public core.history.Battle data { get; set; }
        }
    }
}
