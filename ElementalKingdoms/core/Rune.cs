﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public enum Element
    {
        Earth = 1,
        Water = 2,
        Air = 3,
        Fire = 4
    }

    public class Rune
    {
        public static List<Rune> Runes = new List<Rune>();

        public static void PrintAll()
        {
            StringBuilder sb = new StringBuilder();
            
            bool first = true;
            foreach (Rune r in Runes)
            {
                if (first)
                {
                    // Print header row (property names)
                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(r))
                    {
                        string name = descriptor.Name;
                        sb.AppendFormat("{0}\t", name);
                    }
                    sb.AppendLine();
                    first = false;
                }

                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(r))
                {
                    object value = descriptor.GetValue(r);
                    if (value.GetType().IsArray)
                    {
                        int[] a = (int[])value;
                        sb.AppendFormat("{0}\t", String.Join(", ", a));
                    }
                    else
                    {
                        sb.AppendFormat("{0}\t", value);
                    }
                }
                sb.AppendLine();
            }

            System.IO.File.WriteAllText(@"cache/all_runes.csv", sb.ToString());
        }

        [JsonProperty("RuneId")]
        public int Id { get; set; }
        [JsonProperty("RuneName")]
        public string Name { get; set; }
        [JsonProperty("Property")]
        public Element Element { get; set; }
        [JsonProperty("Color")]
        public int Stars { get; set; }
        public int LockSkill1 { get; set; }
        public int LockSkill2 { get; set; }
        public int LockSkill3 { get; set; }
        public int LockSkill4 { get; set; }
        public int LockSkill5 { get; set; }

        public int Price { get; set; }
        public int SkillTimes { get; set; }
        public string Condition { get; set; }
        public int SkillConditionSlide { get; set; }
        public int SkillConditionType { get; set; }
        public int SkillConditionRace { get; set; }
        public int SkillConditionColor { get; set; }
        public int SkillConditionCompare { get; set; }
        public int SkillConditionValue { get; set; }

        public int ThinkGet { get; set; }
        public int Fragment { get; set; }
        public int BaseExp { get; set; }
        public int[] ExpArray { get; set; }

        public static int GetIdFromName(string name)
        {
            Rune r = Runes.Find(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            if (r == null)
            {
                throw new ArgumentException(String.Format("Could not find a rune with name {0}", name));
            }
            return r.Id;
        }

        /*
        {
			"RuneId": "1",
			"RuneName": "Fire Fist",
			"Property": "4",
			"Color": "1",
			"LockSkill1": "168",
			"LockSkill2": "169",
			"LockSkill3": "170",
			"LockSkill4": "171",
			"LockSkill5": "172",
			"Price": "800",
			"SkillTimes": "3",
			"Condition": "your opponent has more than 2 cards on the battlefield ",
			"SkillConditionSlide": "2",
			"SkillConditionType": "4",
			"SkillConditionRace": "0",
			"SkillConditionColor": "0",
			"SkillConditionCompare": "1",
			"SkillConditionValue": "2",
			"ThinkGet": "1",
			"Fragment": "0",
			"BaseExp": "20",
			"ExpArray": ["0",
			"40",
			"200",
			"560",
			"1200"]
		}*/
    }
}
