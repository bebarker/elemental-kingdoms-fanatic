﻿namespace ElementalKingdoms.ui
{
    partial class LeaguePlayer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pbAvatar = new System.Windows.Forms.PictureBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblOddsGame = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.lblOddsSim = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbAvatar)).BeginInit();
            this.SuspendLayout();
            // 
            // pbAvatar
            // 
            this.pbAvatar.Location = new System.Drawing.Point(3, 3);
            this.pbAvatar.Name = "pbAvatar";
            this.pbAvatar.Size = new System.Drawing.Size(50, 50);
            this.pbAvatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbAvatar.TabIndex = 0;
            this.pbAvatar.TabStop = false;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(59, 3);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 17);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Name";
            // 
            // lblOddsGame
            // 
            this.lblOddsGame.AutoSize = true;
            this.lblOddsGame.Location = new System.Drawing.Point(59, 20);
            this.lblOddsGame.Name = "lblOddsGame";
            this.lblOddsGame.Size = new System.Drawing.Size(80, 17);
            this.lblOddsGame.TabIndex = 2;
            this.lblOddsGame.Text = "OddsGame";
            // 
            // lblOddsSim
            // 
            this.lblOddsSim.AutoSize = true;
            this.lblOddsSim.Location = new System.Drawing.Point(59, 37);
            this.lblOddsSim.Name = "lblOddsSim";
            this.lblOddsSim.Size = new System.Drawing.Size(65, 17);
            this.lblOddsSim.TabIndex = 4;
            this.lblOddsSim.Text = "OddsSim";
            // 
            // LeaguePlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblOddsSim);
            this.Controls.Add(this.lblOddsGame);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.pbAvatar);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "LeaguePlayer";
            this.Size = new System.Drawing.Size(159, 58);
            ((System.ComponentModel.ISupportInitialize)(this.pbAvatar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbAvatar;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblOddsGame;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label lblOddsSim;
    }
}
