﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core.kingdomwar
{
    public class KingdomWar
    {
        /*
        "map": {
            "1": {
                "point_id": "1",
                "point_name": "Fire Furnace",
                "force_id": "4",
                "value": 1000,
                "resource": 10,
                "ishome": 2,
                "link": "2,3,5",
                "around": {
                    "1": 0,
                    "2": 0,
                    "3": 0,
                    "4": 0
                }
            },
            "2": {
                "point_id": "2",
                "point_name": "Sunset Hills",
                "force_id": "4",
                "value": 200,
                "resource": 2,
                "ishome": 1,
                "link": "1,5,6",
                "around": {
                    "1": "105",
                    "2": 0,
                    "3": 0,
                    "4": 0
                }
            },
        },
        */
        public Dictionary<string, Point> Map { get; set; }

        [JsonProperty("force_id")]
        public Race ForceId { get; set; }
        [JsonProperty("legion_id")]
        public int LegionId { get; set; }
        public int Medal { get; set; }
        [JsonProperty("battle_win")]
        public int BattleWin { get; set; }
        [JsonProperty("battle_dead")]
        public int BattleDeath { get; set; }
        public Dictionary<string, int> Resource { get; set; }
        public int Duty { get; set; }
        public int Wealth { get; set; }
        [JsonProperty("cd_time")]
        public int CooldownTime { get; set; }
        /*
        "force_id": 2,
        "legion_id": 85,
        "medal": 203236,
        "battle_win": 11,
        "battle_dead": 1,
        "resource": {
            "1": 187,
            "2": 214,
            "3": 126,
            "4": 160
        },
        "duty": 0,
        "wealth": 8999095,
        "cd_time": 0,
        "buff_info": {
            "1": {
                "Id": 1,
                "Name": "Clan HP",
                "Type": 1,
                "CurrencyType": "1,2",
                "Currency1": 300,
                "Currency2": 100000,
                "AdditionType": 1,
                "AdditionValue": 1.1,
                "Duration": 600,
                "AddResources": 4000
            },
            "2": {
                "Id": 2,
                "Name": "Clan ATK",
                "Type": 1,
                "CurrencyType": "1,2",
                "Currency1": 300,
                "Currency2": 100000,
                "AdditionType": 2,
                "AdditionValue": 1.1,
                "Duration": 600,
                "AddResources": 4000
            },
            "3": {
                "Id": 3,
                "Name": "My HP",
                "Type": 2,
                "CurrencyType": "1",
                "Currency1": 150,
                "Currency2": 0,
                "AdditionType": 1,
                "AdditionValue": 1.1,
                "Duration": 600,
                "AddResources": 0
            },
            "4": {
                "Id": 4,
                "Name": "My ATK",
                "Type": 2,
                "CurrencyType": "1",
                "Currency1": 150,
                "Currency2": 0,
                "AdditionType": 2,
                "AdditionValue": 1.1,
                "Duration": 600,
                "AddResources": 0
            }
        },
        "my_buff_info": {
            "1": 0,
            "2": 0,
            "3": 0,
            "4": 0
        },
        "default_life": 3,
        "left_life": 2,
        "max_resource": 2000,
        "default_cd_time": 6,
        "current_id": 0,
        "fight_status": 0,
        "surplustime": 2865
        */


        public List<Point> GetAttackableTargets()
        {
            List<Point> targets = new List<Point>();
            foreach (var point in Map.Values.Where(x => x.Ruler == ForceId))
            {
                //Console.WriteLine("We own point {0}", point.Name);
                foreach (string s in point.Link.Split(','))
                {
                    if (Map[s].Ruler == ForceId)
                    {
                        //Console.WriteLine("     {0} already conquered", Map[s].Name);
                    }
                    else
                    {
                        //Console.WriteLine("  -> Point owned by other faction: {0} {1}", Map[s].Name, Map[s].Ruler);

                        if (!targets.Contains(Map[s]))
                        {
                            //Console.WriteLine("     -> Point was already in list {0}", Map[s].Name);
                            targets.Add(Map[s]);
                        }
                        else
                        {
                            //Console.WriteLine("     -> Point was already in list {0}", Map[s].Name);
                        }
                    }
                }
            }

            return targets;
        }
    }
}
