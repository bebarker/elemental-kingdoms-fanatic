﻿using ElementalKingdoms.core;
using ElementalKingdoms.util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElementalKingdomsGui.ui
{
    public partial class DeckExporter : Form
    {
        private Dictionary<string, User> customDecks;

        public DeckExporter(Dictionary<string, User> decks)
        {
            InitializeComponent();
            customDecks = decks;
            foreach (string s in customDecks.Keys)
            {
                listBox1.Items.Add(s);
            }
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath + "\\";
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {

            Dictionary<string, User> selectedDecks = new Dictionary<string, User>();

            foreach (string item in listBox1.SelectedItems)
            {
                Console.WriteLine("Exporting {0}", item);
                selectedDecks.Add(item, customDecks[item]);
            }
            
            if (listBox1.SelectedItems.Count == 0)
            {
                selectedDecks = customDecks;
            }

            if (rbPeppa.Checked)
            {
                string json = JsonConvert.SerializeObject(selectedDecks, Formatting.Indented);
                string fileName = textBox1.Text + "custom_decks.json";

                int i = 1;
                while (File.Exists(fileName))
                {
                    fileName = String.Format("{0}custom_decks_{1}.json", textBox1.Text, i);
                    i++;
                }

                File.WriteAllText(fileName, json);
            }
            else if (rbMitzi.Checked)
            {
                string filename = "decks_Peppa_export.txt";
                new MitziLoader().Write(selectedDecks.Values.ToList(), filename);
            }
            else if (rbCrystark.Checked)
            {
                new CrystarkLoader().Write(selectedDecks, textBox1.Text);
            }
        }
    }
}
