﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.server;

namespace ElementalKingdoms.ui
{
    public partial class ServerLoginForm : Form
    {
        private const int EM_SETCUEBANNER = 0x1501;

        public bool IsGuestUser { get; internal set; }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern Int32 SendMessage(IntPtr hWnd, int msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)]string lParam);

        public ServerLoginForm()
        {
            InitializeComponent();

            lblErrors.Text = "";

            SendMessage(tbEmail.Handle, EM_SETCUEBANNER, 0, "Email");
            SendMessage(tbPassword.Handle, EM_SETCUEBANNER, 0, "Password");

            SendMessage(tbUin.Handle, EM_SETCUEBANNER, 0, "UIN");

            // AHRIMAN7598 lvl 16
            tbEmail.Text = "g1095766@trbvm.com";
            tbPassword.Text = "g1095766@trbvm";

            // CRNOBOG7138 lvl4
            //tbEmail.Text = "g1101310@trbvm.com";
            //tbPassword.Text = "g1101310@trbvm";

            // MurielNicola
            //tbEmail.Text = "g1108241@trbvm.com";
            //tbPassword.Text = "qwerty";
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            ServerCommunicator com = ServerCommunicator.Create(ElementalKingdomsGui.Properties.Settings.Default.DeviceId, false);

            if (!rbUin.Checked)
            {
                com.Connect();
            }

            try
            {
                IsGuestUser = false;
                if (rbGuest.Checked)
                {
                    com.PlayAsGuest();
                    IsGuestUser = true;
                }
                else if (rbArc.Checked)
                {
                    com.ArcLogin(tbEmail.Text, tbPassword.Text);
                }
                else if (rbUin.Checked)
                {
                    com.Uin = tbUin.Text;
                    //com.DeviceToken = tbDeviceToken.Text;
                }
                com.EKLogin(ElementalKingdomsGui.Properties.Settings.Default.selectedServerId);
                //com.Tutorial();

                DialogResult = DialogResult.OK;
                Close();
            } 
            catch (Exception ex)
            {
                lblErrors.Text = ex.Message;
                lblErrors.Visible = true;
            }

            //new MainForm().Show();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            tbEmail.Visible = rbArc.Checked;
            tbPassword.Visible = rbArc.Checked;
        }

        private void rbUin_CheckedChanged(object sender, EventArgs e)
        {
            tbUin.Visible = rbUin.Checked;
        }
    }
}
