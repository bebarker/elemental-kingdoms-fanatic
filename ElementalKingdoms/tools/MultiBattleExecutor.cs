﻿using ElementalKingdoms.core;
using ElementalKingdoms.util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.tools
{
    public enum BattleMode
    {
        /// <summary>
        /// Auto determines which mode should be used based on defender deck
        /// </summary>
        Default,
        Thief,
        Arena,
        Demon,
        KingdomWars,
        Hydra,
        ElementalWars,
    }
    public class MultiBattleExecutor
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        List<User> Defenders = new List<User>();
        User Defender;
        User Attacker;

        public BattleMode Mode { get; set; }

        public static List<BattleResult> Results = new List<BattleResult>();

        public MultiBattleExecutor(User attacker, User defender)
        {
            Attacker = attacker;
            AddDefender(defender);
            Mode = BattleMode.Default;
            if (defender.HasLegendary())
            {
                Mode = BattleMode.ElementalWars;
            }
        }

        public MultiBattleExecutor(User attacker, params User[] defenders)
        {
            Attacker = attacker;
            AddDefender(defenders);
        }

        public void AddDefender(params User[] defenders)
        {
            if (defenders.Count() > 0)
            {
                if (Defenders.Count == 0)
                {
                    Defender = defenders[0];
                }
                Defenders.AddRange(defenders);
            }
        }

        private Battle InitializeBattle()
        {
            return new Battle(Attacker, Defender);
        }

        public BattleResult Execute(int numberOfBattles)
        {
            if (Defenders.Count == 0)
            {
                throw new Exception("No defenders!");
            }

            if (Mode == BattleMode.KingdomWars)
            {
                return ExecuteKingdomWar(numberOfBattles);
            }
            else
            {
                return ExecuteNormal(numberOfBattles);
            }
        }

        public BattleResult ExecuteKingdomWar(int numberOfBattles)
        {
            foreach (var user in Defenders)
            {
                log.DebugFormat("Found player {0}", user.NickName);
            }

            BattleResult result = new BattleResult(numberOfBattles, Attacker, Defender);

            if (Defenders.Count == 0)
            {
                result.Iterations = 0;
                return result;
            }
            Defender = Defenders[ThreadSafeRandom.ThisThreadsRandom.Next(Defenders.Count)];

            log4net.Core.Level currentLogLevel = Util.SetLoglevel(log4net.Core.Level.Error);

            var watch = Stopwatch.StartNew();

            Object locker = new object();
            // Parallel execution example found on http://stackoverflow.com/a/3643365
            Parallel.For(0, numberOfBattles, new ParallelOptions() { /*MaxDegreeOfParallelism = 64*/ }, (i, loopState) =>
            //for (int i = 0; i < numberOfBattles; i++)
            {
                Battle battle = InitializeBattle();

                //if (skipReplays)
                {
                    battle.StoreReplay = false;
                }

                int numFights = 0;

                do
                {
                    numFights++;

                    while (!battle.Finished)
                    {
                        battle.Round();
                    }

                    //if (battle.StoreReplay) result.Battles.Add(battle);

                    if (battle.Win)
                    {
                        var newDefender = Defenders[ThreadSafeRandom.ThisThreadsRandom.Next(Defenders.Count)];
                        battle.KingdomWarNextFight(newDefender);
                    }
                    else
                    {
                        break;
                    }
                } while (numFights < 500);

                lock (locker)
                {

                    //if (result.Battles.Count > 100)
                    //{
                    //    skipReplays = true;
                    //}

                    result.FightsPerLive.Add(numFights);

                    if (battle.Win)
                    {
                        result.RoundsWon += numFights;
                    }
                }
            }
            );

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;

            Util.SetLoglevel(currentLogLevel);
            log.InfoFormat("Finished {0} attacks in {1} ms.", result.Iterations, elapsedMs);
            log.Info(result);

            result.Battles.Clear();

            return result;
        }

        public BattleResult ExecuteNormal(int numberOfBattles)
        {
            BattleResult result = new BattleResult(numberOfBattles, Attacker, Defender);

            log4net.Core.Level currentLogLevel = Util.SetLoglevel(log4net.Core.Level.Error);

            var watch = Stopwatch.StartNew();
            bool skipReplays = false;

            Object locker = new object();
            // Parallel execution example found on http://stackoverflow.com/a/3643365
            Parallel.For(0, numberOfBattles, new ParallelOptions() { /*MaxDegreeOfParallelism = 64*/ }, (i, loopState) =>
            {
                Battle battle = InitializeBattle();

                if (skipReplays)
                {
                    battle.StoreReplay = false;
                }

                while (!battle.Finished)
                {
                    battle.Round();
                }

                lock (locker)
                {
                    if (battle.StoreReplay) result.Battles.Add(battle);
                    result.SumMerit += battle.Merit;
                    result.MaxMerit = Math.Max(battle.Merit, result.MaxMerit);
                    result.MinMerit = Math.Min(battle.Merit, result.MinMerit);
                    result.Merit.Add(battle.Merit);

                    if (result.Battles.Count > 100)
                    {
                        skipReplays = true;
                    }

                    if (battle.Rounds.Count >= 50)
                    {
                        result.NumberOfTimesReachedRoundFifty++;
                    }
                    if (battle.Rounds.Count >= 100)
                    {
                        result.NumberOfTimesReachedRoundHundred++;
                    }
                    result.SumRounds += battle.Rounds.Count;
                    result.MaxRounds = Math.Max(battle.Rounds.Count, result.MaxRounds);
                    result.MinRounds = Math.Min(battle.Rounds.Count, result.MinRounds);

                    if (battle.Win)
                    {
                        result.RoundsWon++;
                    }
                }
            }
            );
            
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;

            Util.SetLoglevel(currentLogLevel);
            log.InfoFormat("Finished {0} attacks in {1} ms.", result.Iterations, elapsedMs);
            log.Info(result);

            if (result.SumMerit > 0)
            {
                var orderedList = result.Battles.OrderBy(x => x.Merit);

                SaveReplay(orderedList.Last(), "GOOD");
                SaveReplay(orderedList.First(), "BAD");
            }
            else
            {
                SaveReplay(result.Battles.Find(x => x.Win), "GOOD");
                SaveReplay(result.Battles.Find(x => !x.Win), "BAD");
            }

            result = AddResult(result);
            result.Battles.Clear();

            return result;
        }

        private BattleResult AddResult(BattleResult result)
        {
            BattleResult ret = result;
            bool resultsMerged = false;
            foreach (BattleResult r in Results)
            {
                if (result.IsSameFight(r))
                {
                    // merge results
                    r.Iterations += result.Iterations;
                    r.MaxMerit = Math.Max(r.MaxMerit, result.MaxMerit);
                    r.MaxRounds = Math.Max(r.MaxRounds, result.MaxRounds);
                    r.MinMerit = Math.Min(r.MinMerit, result.MinMerit);
                    r.MinRounds = Math.Min(r.MinRounds, result.MinRounds);
                    r.SumMerit += result.SumMerit;
                    r.SumRounds += result.SumRounds;
                    r.NumberOfTimesReachedRoundFifty += result.NumberOfTimesReachedRoundFifty;
                    r.NumberOfTimesReachedRoundHundred += result.NumberOfTimesReachedRoundHundred;
                    r.RoundsWon += result.RoundsWon;

                    r.Merit.AddRange(result.Merit);

                    resultsMerged = true;
                    ret = r;
                    break;
                }
            }

            if (!resultsMerged)
            {
                Results.Add(result);
            }

            Save();

            return ret;
        }

        public static void Load()
        {
            string resultsFilePath = @"cache/results.json";
            if (!File.Exists(resultsFilePath)) return;
            using (StreamReader streamReader = new StreamReader(resultsFilePath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                Results = JsonConvert.DeserializeObject<List<BattleResult>>(response);
            }
        }

        public static void Save()
        {
            string json = JsonConvert.SerializeObject(Results, Formatting.Indented);

            File.WriteAllText(@"cache/results.json", json);
        }

        private void SaveReplay(Battle battle, string tag)
        {
            if (battle == null) return;
            core.history.Battle b = new core.history.Battle();
            b.AttackPlayer = new core.history.BattlePlayer();
            b.AttackPlayer.Cards = Attacker.UserDeck.Cards.ToList();
            b.AttackPlayer.Runes = Attacker.UserDeck.Runes.ToList();
            b.AttackPlayer.NickName = Attacker.NickName;
            b.AttackPlayer.Level = Attacker.Level.ToString();
            b.AttackPlayer.Avatar = Attacker.Avatar;
            b.AttackPlayer.Sex = Attacker.Sex;
            b.AttackPlayer.HP = Attacker.HP.ToString(); ;
            b.AttackPlayer.RemainHP = battle.AttackPlayer.CurrentHP.ToString();

            b.DefendPlayer = new core.history.BattlePlayer();
            b.DefendPlayer.Cards = Defender.UserDeck.Cards.ToList();
            b.DefendPlayer.Runes = Defender.UserDeck.Runes.ToList();
            b.DefendPlayer.NickName = Defender.NickName;
            b.DefendPlayer.Level = Defender.Level.ToString();
            b.DefendPlayer.Avatar = Defender.Avatar;
            b.DefendPlayer.Sex = Defender.Sex;
            b.DefendPlayer.HP = Defender.HP.ToString(); ;
            b.DefendPlayer.RemainHP = battle.DefendPlayer.CurrentHP.ToString();

            b.BattleId = ElementalKingdoms.Util.GetFilenameFriendlyCurrentTime() + ElementalKingdoms.server.ServerHashGenerator.GetMd5(DateTime.Now.ToFileTime().ToString());
            b.Rounds = battle.Rounds;

            foreach (var card in battle.summonedCards.Values)
            {
                if (card.UUID.StartsWith("atk_"))
                {
                    b.AttackPlayer.Cards.Add(card);
                }
                else
                {
                    b.DefendPlayer.Cards.Add(card);
                }
            }

            util.JsonLoader.SaveBattle(b, "cache/battle/" + tag + "_" + b.BattleId + ".json");
        }
    }
}
