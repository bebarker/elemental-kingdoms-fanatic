﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElementalKingdoms.ui
{
    public partial class BattleField : UserControl
    {

        BattleFieldCard[] cards = new BattleFieldCard[20];
        public BattleField()
        {
            InitializeComponent();
            for (int i = 0; i < cards.Length; i++)
            {
                cards[i] = new BattleFieldCard();
                cards[i].Location = new Point(3 + i * cards[i].Size.Width, 3);

                // only show the first 5 battlefield cards by default
                if (i > 4)
                {
                    cards[i].Visible = false;
                }
                this.Controls.Add(cards[i]);
            }
        }

        public void SetBattlefield(ElementalKingdoms.core.BattleField field)
        {
            this.SuspendLayout();
            for (int i = 0; i < cards.Length; i++)
            {
                cards[i].SetCard(field[i]);
                cards[i].Visible = true;
            }

            for (int i = cards.Length - 1; i > 4; i--)
            {
                if (field[i] == null)
                {
                    cards[i].Visible = false;
                }
            }
            this.ResumeLayout();
        }

        public void SetBattlefield(List<core.history.BattleCard> field)
        {
            this.SuspendLayout();
            List<int> emptySpots = Enumerable.Range(0, cards.Length).ToList();
            foreach (var card in field)
            {
                cards[card.LocationIndex].SetCard(card);
                cards[card.LocationIndex].Visible = true;
                emptySpots.Remove(card.LocationIndex);
            }

            foreach (int i in emptySpots)
            {
                cards[i].SetCard((core.history.BattleCard)null);
                if (i > 4)
                {
                    cards[i].Visible = false;
                }
            }
            this.ResumeLayout();
        }
    }
}
