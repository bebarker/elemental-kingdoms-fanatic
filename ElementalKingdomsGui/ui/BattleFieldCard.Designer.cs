﻿namespace ElementalKingdoms.ui
{
    partial class BattleFieldCard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblHp = new System.Windows.Forms.Label();
            this.pbCard = new System.Windows.Forms.PictureBox();
            this.lblAttack = new System.Windows.Forms.Label();
            this.lblConfused = new System.Windows.Forms.Label();
            this.lblFrozen = new System.Windows.Forms.Label();
            this.lblParalyzed = new System.Windows.Forms.Label();
            this.lblTrapped = new System.Windows.Forms.Label();
            this.lblBurned = new System.Windows.Forms.Label();
            this.lblPoisoned = new System.Windows.Forms.Label();
            this.lblLaceration = new System.Windows.Forms.Label();
            this.lblStunned = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lblSilenced = new System.Windows.Forms.Label();
            this.lblDeathMark = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbCard)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHp
            // 
            this.lblHp.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblHp.ForeColor = System.Drawing.SystemColors.Info;
            this.lblHp.Location = new System.Drawing.Point(3, 106);
            this.lblHp.Name = "lblHp";
            this.lblHp.Size = new System.Drawing.Size(60, 17);
            this.lblHp.TabIndex = 17;
            this.lblHp.Text = "0";
            this.lblHp.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblHp.Visible = false;
            // 
            // pbCard
            // 
            this.pbCard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbCard.Location = new System.Drawing.Point(3, 3);
            this.pbCard.Name = "pbCard";
            this.pbCard.Size = new System.Drawing.Size(60, 120);
            this.pbCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbCard.TabIndex = 16;
            this.pbCard.TabStop = false;
            // 
            // lblAttack
            // 
            this.lblAttack.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblAttack.ForeColor = System.Drawing.SystemColors.Info;
            this.lblAttack.Location = new System.Drawing.Point(3, 89);
            this.lblAttack.Name = "lblAttack";
            this.lblAttack.Size = new System.Drawing.Size(60, 17);
            this.lblAttack.TabIndex = 18;
            this.lblAttack.Text = "0";
            this.lblAttack.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblAttack.Visible = false;
            // 
            // lblConfused
            // 
            this.lblConfused.AutoSize = true;
            this.lblConfused.BackColor = System.Drawing.Color.MediumOrchid;
            this.lblConfused.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblConfused.Location = new System.Drawing.Point(20, 54);
            this.lblConfused.Name = "lblConfused";
            this.lblConfused.Size = new System.Drawing.Size(17, 17);
            this.lblConfused.TabIndex = 19;
            this.lblConfused.Text = "C";
            // 
            // lblFrozen
            // 
            this.lblFrozen.BackColor = System.Drawing.Color.Blue;
            this.lblFrozen.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblFrozen.Location = new System.Drawing.Point(3, 3);
            this.lblFrozen.Name = "lblFrozen";
            this.lblFrozen.Size = new System.Drawing.Size(17, 17);
            this.lblFrozen.TabIndex = 20;
            this.lblFrozen.Text = "F";
            // 
            // lblParalyzed
            // 
            this.lblParalyzed.AutoSize = true;
            this.lblParalyzed.BackColor = System.Drawing.Color.Yellow;
            this.lblParalyzed.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblParalyzed.Location = new System.Drawing.Point(3, 20);
            this.lblParalyzed.Name = "lblParalyzed";
            this.lblParalyzed.Size = new System.Drawing.Size(17, 17);
            this.lblParalyzed.TabIndex = 21;
            this.lblParalyzed.Text = "P";
            // 
            // lblTrapped
            // 
            this.lblTrapped.AutoSize = true;
            this.lblTrapped.BackColor = System.Drawing.Color.SlateGray;
            this.lblTrapped.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblTrapped.Location = new System.Drawing.Point(3, 37);
            this.lblTrapped.Name = "lblTrapped";
            this.lblTrapped.Size = new System.Drawing.Size(17, 17);
            this.lblTrapped.TabIndex = 22;
            this.lblTrapped.Text = "T";
            // 
            // lblBurned
            // 
            this.lblBurned.AutoSize = true;
            this.lblBurned.BackColor = System.Drawing.Color.Crimson;
            this.lblBurned.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblBurned.Location = new System.Drawing.Point(20, 3);
            this.lblBurned.Name = "lblBurned";
            this.lblBurned.Size = new System.Drawing.Size(32, 17);
            this.lblBurned.TabIndex = 23;
            this.lblBurned.Text = "000";
            // 
            // lblPoisoned
            // 
            this.lblPoisoned.AutoSize = true;
            this.lblPoisoned.BackColor = System.Drawing.Color.Green;
            this.lblPoisoned.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPoisoned.Location = new System.Drawing.Point(20, 20);
            this.lblPoisoned.Name = "lblPoisoned";
            this.lblPoisoned.Size = new System.Drawing.Size(32, 17);
            this.lblPoisoned.TabIndex = 24;
            this.lblPoisoned.Text = "000";
            // 
            // lblLaceration
            // 
            this.lblLaceration.BackColor = System.Drawing.Color.Orange;
            this.lblLaceration.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLaceration.Location = new System.Drawing.Point(3, 71);
            this.lblLaceration.Name = "lblLaceration";
            this.lblLaceration.Size = new System.Drawing.Size(17, 17);
            this.lblLaceration.TabIndex = 25;
            this.lblLaceration.Text = "L";
            // 
            // lblStunned
            // 
            this.lblStunned.BackColor = System.Drawing.Color.Yellow;
            this.lblStunned.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblStunned.Location = new System.Drawing.Point(3, 54);
            this.lblStunned.Name = "lblStunned";
            this.lblStunned.Size = new System.Drawing.Size(17, 17);
            this.lblStunned.TabIndex = 26;
            this.lblStunned.Text = "S";
            // 
            // lblSilenced
            // 
            this.lblSilenced.BackColor = System.Drawing.Color.Purple;
            this.lblSilenced.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblSilenced.Location = new System.Drawing.Point(20, 71);
            this.lblSilenced.Name = "lblSilenced";
            this.lblSilenced.Size = new System.Drawing.Size(17, 17);
            this.lblSilenced.TabIndex = 27;
            this.lblSilenced.Text = "S";
            // 
            // lblDeathMark
            // 
            this.lblDeathMark.AutoSize = true;
            this.lblDeathMark.BackColor = System.Drawing.Color.Black;
            this.lblDeathMark.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblDeathMark.Location = new System.Drawing.Point(20, 37);
            this.lblDeathMark.Name = "lblDeathMark";
            this.lblDeathMark.Size = new System.Drawing.Size(32, 17);
            this.lblDeathMark.TabIndex = 28;
            this.lblDeathMark.Text = "000";
            // 
            // BattleFieldCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblDeathMark);
            this.Controls.Add(this.lblSilenced);
            this.Controls.Add(this.lblStunned);
            this.Controls.Add(this.lblLaceration);
            this.Controls.Add(this.lblPoisoned);
            this.Controls.Add(this.lblBurned);
            this.Controls.Add(this.lblTrapped);
            this.Controls.Add(this.lblParalyzed);
            this.Controls.Add(this.lblFrozen);
            this.Controls.Add(this.lblConfused);
            this.Controls.Add(this.lblAttack);
            this.Controls.Add(this.lblHp);
            this.Controls.Add(this.pbCard);
            this.Name = "BattleFieldCard";
            this.Size = new System.Drawing.Size(66, 126);
            ((System.ComponentModel.ISupportInitialize)(this.pbCard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHp;
        private System.Windows.Forms.PictureBox pbCard;
        private System.Windows.Forms.Label lblAttack;
        private System.Windows.Forms.Label lblConfused;
        private System.Windows.Forms.Label lblFrozen;
        private System.Windows.Forms.Label lblParalyzed;
        private System.Windows.Forms.Label lblTrapped;
        private System.Windows.Forms.Label lblBurned;
        private System.Windows.Forms.Label lblPoisoned;
        private System.Windows.Forms.Label lblLaceration;
        private System.Windows.Forms.Label lblStunned;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lblSilenced;
        private System.Windows.Forms.Label lblDeathMark;
    }
}
