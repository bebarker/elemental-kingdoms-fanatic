﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.tools;

namespace ElementalKingdoms.ui
{
    public partial class ResultsForm : Form
    {
        BindingSource bs = new BindingSource();
        public ResultsForm()
        {
            InitializeComponent();
            bs.DataSource = MultiBattleExecutor.Results;
            dataGridView1.DataSource = bs;
            timer1.Start();
        }

        private void ResultsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            bs.ResetBindings(false);
        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MultiBattleExecutor.Save();
        }
    }
}
