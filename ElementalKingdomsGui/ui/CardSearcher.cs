﻿using ElementalKingdoms.core;
using ElementalKingdoms.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdomsGui.Properties;

namespace ElementalKingdoms.ui
{
    public partial class CardSearcher : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private UserCard preselectedCard;
        public UserCard SelectedCard { get; set; }
        private List<UserCard> UserCards;
        private List<UserCard> allCards;

        private List<UserCard> displayedCards;

        private const int DEFAULT_LEVEL = 10;
        
        public CardSearcher()
        {
            InitializeComponent();

            allCards = new List<UserCard>();
            foreach (var card in Card.Cards)
            {
                allCards.Add(new UserCard() { CardId = card.Id });
            }

            SetUserCards(new List<UserCard>());
            SetCard(null);

            lbCards.DisplayMember = "Name";

            cbSkillFilter.DataSource = GetSkillNames();
            cbSkillFilter.SelectedItem = null;

            cbEvolveSkill.DataSource = Skill.Skills.FindAll(x => x.EvoRank > 0 || x.SkillId == 1034 || x.SkillId == 649 || x.SkillId == 1209).OrderBy(x => x.Name).ToList();
            cbEvolveSkill.SelectedItem = null;

            this.ActiveControl = tbFilter;
        }

        public void SetCard(UserCard c)
        {
            SelectedCard = new UserCard();

            preselectedCard = c;
            if (preselectedCard != null)
            {
                SelectedCard.CardId = preselectedCard.CardId;
                SelectedCard.Level = preselectedCard.Level;
                SelectedCard.SkillNew = preselectedCard.SkillNew;
                SelectedCard.WashTime = preselectedCard.WashTime;

                //tbFilter.Text = preselectedCard.Name;
                cbEvolveSkill.SelectedItem = Skill.Get(preselectedCard.SkillNew);
                numLevel.Value = preselectedCard.Level;
                numTimesEvolved.Value = preselectedCard.WashTime.GetValueOrDefault(0);
            }
            else
            {
                SelectedCard.CardId = 1;
                SelectedCard.Level = DEFAULT_LEVEL;
            }
        }

        public void SetUserCards(List<UserCard> cards)
        {
            UserCards = cards;

            if (UserCards.Count == 0)
            {
                cbDisplayOnlyOwnedCards.Checked = false;
                cbDisplayOnlyOwnedCards.Enabled = false;
            }
            else
            {
                // Enable it, but don't automatically check it
                cbDisplayOnlyOwnedCards.Enabled = true;
            }
            
            
            if (cbDisplayOnlyOwnedCards.Checked)
            {
                EnableStatEditting(false);
            }
        }

        private List<string> GetSkillNames()
        {
            List<string> skills = new List<string>();

            foreach (var skill in Skill.Skills)
            {
                string name = skill.Name.Trim();
                bool skip = false;
                for (int i = 2; i <= 10; i++)
                {
                    if (name.EndsWith(String.Format(" {0}", i)))
                    {
                        skip = true;
                        break;
                    }
                }
                if (skip) continue;

                if (name.EndsWith(" 1"))
                {
                    name = name.Substring(0, name.Length - 2);
                }
                skills.Add(name);
            }
            return skills;
        }

        private void lbCards_SelectedIndexChanged(object sender, EventArgs e)
        {
            log.DebugFormat("Called. SelectedIndex: {0}", lbCards.SelectedIndex);
            var c = lbCards.SelectedItem as UserCard;
            if (c == null)
            {
                return;
            }
            if (c.BaseCard.Id != SelectedCard.CardId || c.UserCardId != SelectedCard.UserCardId)
            {
                log.DebugFormat("Changing selected card to {0}", c.Name);
                SelectedCard.CardId = c.BaseCard.Id;
                if (cbDisplayOnlyOwnedCards.Checked)
                {
                    SelectedCard.UserCardId = c.UserCardId;
                    //SelectedCard.Level = c.Level;
                    SelectedCard.Evolution = c.Evolution;
                    SelectedCard.Exp = c.Exp;
                    SelectedCard.SkillNew = c.SkillNew;
                    SelectedCard.WashTime = c.WashTime;
                    numLevel.Value = c.Level;
                }
                else if (preselectedCard != null)
                {
                    if (preselectedCard.Name == c.Name)
                    {
                        SelectedCard.Level = preselectedCard.Level;
                        SelectedCard.SkillNew = preselectedCard.SkillNew;
                    }
                    else
                    {
                        SelectedCard.Level = DEFAULT_LEVEL;
                        SelectedCard.SkillNew = 0;
                    }
                    numLevel.Value = SelectedCard.Level;
                }

                if (SelectedCard.SkillNew == 0)
                {
                    cbEvolveSkill.SelectedItem = null;
                }
                else
                {
                    cbEvolveSkill.SelectedItem = Skill.Get(SelectedCard.SkillNew);
                }
                UpdateLabels();
            }
        }

        private void tbFilter_TextChanged(object sender, EventArgs e)
        {
            log.DebugFormat("Filtering {0}", tbFilter.Text);
            UpdateFilter();
        }

        private void UpdateFilter()
        {
            var currentlySelectedCard = lbCards.SelectedItem;

            if (cbDisplayOnlyOwnedCards.Checked)
            {
                displayedCards = UserCards;
            }
            else
            {
                displayedCards = allCards;
            }

            if (!cbStars1.Checked)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Stars != 1);
            }
            if (!cbStars2.Checked)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Stars != 2);
            }
            if (!cbStars3.Checked)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Stars != 3);
            }
            if (!cbStars4.Checked)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Stars != 4);
            }
            if (!cbStars5.Checked)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Stars != 5);
            }

            if (!cbClassForest.Checked)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Race != Race.Forest);
            }
            if (!cbClassTundra.Checked)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Race != Race.Tundra);
            }
            if (!cbClassSwamp.Checked)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Race != Race.Swamp);
            }
            if (!cbClassMountain.Checked)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Race != Race.Mountain);
            }
            if (!cbClassOther.Checked)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Race != Race.Demon && x.BaseCard.Race != Race.Feast && x.BaseCard.Race != Race.Gold && x.BaseCard.Race != Race.Hydra && x.BaseCard.Race != Race.Special);
            }

            if (tbFilter.Text.Length > 0)
            {
                displayedCards = displayedCards.FindAll(x => x.BaseCard.Name.IndexOf(tbFilter.Text, StringComparison.CurrentCultureIgnoreCase) != -1);
            }

            if (cbSkillFilter.SelectedItem != null)
            {
                string skillName = cbSkillFilter.SelectedItem.ToString();
                log.DebugFormat("Filtering on skill {0}", skillName);

                displayedCards = displayedCards.FindAll(x => (x.BaseCard.Skill != null && Skill.Get(x.BaseCard.Skill.Value).Name.StartsWith(skillName, StringComparison.CurrentCultureIgnoreCase))
                    || (x.BaseCard.LockSkill1 != null && Skill.Get(x.BaseCard.LockSkill1.Value).Name.StartsWith(skillName, StringComparison.CurrentCultureIgnoreCase))
                    || (x.BaseCard.LockSkill1b != null && Skill.Get(x.BaseCard.LockSkill1b.Value).Name.StartsWith(skillName, StringComparison.CurrentCultureIgnoreCase))
                    || (x.BaseCard.LockSkill2 != null && Skill.Get(x.BaseCard.LockSkill2.Value).Name.StartsWith(skillName, StringComparison.CurrentCultureIgnoreCase))
                    || (x.BaseCard.LockSkillDemon != null && Skill.Get(x.BaseCard.LockSkillDemon.Value).Name.StartsWith(skillName, StringComparison.CurrentCultureIgnoreCase))
                    );
            }

            lbCards.DataSource = displayedCards;
            lbCards.SelectedItem = currentlySelectedCard;
        }

        private void numLevel_ValueChanged(object sender, EventArgs e)
        {
            SelectedCard.Level = Decimal.ToInt32(numLevel.Value);
            UpdateLabels();
        }

        private void numTimesEvolved_ValueChanged(object sender, EventArgs e)
        {
            SelectedCard.WashTime = Decimal.ToInt32(numTimesEvolved.Value);
            UpdateLabels();
        }

        private void UpdateLabels()
        {
            BattleCard bc = new BattleCard(SelectedCard);
            lblAttack.Text = bc.CurrentAttack.ToString();
            lblHP.Text = bc.CurrentHP.ToString();

            lblCost.Text = SelectedCard.Cost.ToString();

            lblSkill1.Text = "";
            lblSkill1b.Text = "";
            lblSkill2.Text = "";
            lblSkill3.Text = "";
            lblSkillDemon.Text = "";

            if (SelectedCard.BaseCard.Skill != null)
            {
                lblSkill1.Text = Skill.Get(SelectedCard.BaseCard.Skill.Value).Name;
            }

            if (SelectedCard.Level >= 5 && SelectedCard.BaseCard.LockSkill1 != null)
            {
                lblSkill2.Text = Skill.Get(SelectedCard.BaseCard.LockSkill1.Value).Name;
            }

            if (SelectedCard.Level >= 5 && SelectedCard.BaseCard.LockSkill1b != null)
            {
                lblSkill1b.Text = Skill.Get(SelectedCard.BaseCard.LockSkill1b.Value).Name;
            }

            if (SelectedCard.Level >= 10 && SelectedCard.BaseCard.LockSkill2 != null)
            {
                lblSkill3.Text = Skill.Get(SelectedCard.BaseCard.LockSkill2.Value).Name;
            }

            lblSkillDemon.Visible = false;
            cbEvolveSkill.Visible = true;
            cbAllEvoSkills.Visible = true;
            btnClearEvolveSkill.Visible = true;
            if (SelectedCard.BaseCard.LockSkillDemon != null)
            {
                if (SelectedCard.Level >= 10)
                {
                    lblSkillDemon.Text = Skill.Get(SelectedCard.BaseCard.LockSkillDemon.Value).Name;
                    lblSkillDemon.Visible = true;
                }
                cbEvolveSkill.Visible = false;
                btnClearEvolveSkill.Visible = false;
                cbAllEvoSkills.Visible = false;
            }
            
            UpdateCheckboxHighlightColor();
            UpdateEvoChain();

            while (imageLoader.IsBusy)
            {
                Application.DoEvents();
            }
            imageLoader.RunWorkerAsync(bc.Name);
        }

        private void UpdateEvoChain()
        {
            log.DebugFormat("Evo possible? {0}", SelectedCard.BaseCard.CanEvo);
            if (SelectedCard.BaseCard.CanEvo == 1)
            {
                Card evoFrom = Card.Cards.Find(x => x.Id == SelectedCard.BaseCard.AlternateEvoCardId);
                log.DebugFormat(" using {0}x {1}", SelectedCard.BaseCard.AlternateEvoCardAmount, evoFrom.Name);
                pbEvolveLeft.Image = ImageLoader.GetSmallImageByName(evoFrom.Name);
                pbEvolveLeft.Tag = SelectedCard.BaseCard.AlternateEvoCardId;
                ttEvo.SetToolTip(pbEvolveLeft, String.Format("You can evolve your {0} with {1}x {2}", SelectedCard.Name, SelectedCard.BaseCard.AlternateEvoCardAmount, evoFrom.Name));
            }
            else
            {
                pbEvolveLeft.Image = null;
                pbEvolveLeft.Tag = 0;
                ttEvo.SetToolTip(pbEvolveLeft, "");
            }

            Card evoTo = Card.Cards.Find(x => x.AlternateEvoCardId == SelectedCard.CardId);
            if (evoTo != null)
            {
                log.DebugFormat("{0}x of me needed to evo {1}", evoTo.AlternateEvoCardAmount, evoTo.Name);
                pbEvolveRight.Image = ImageLoader.GetSmallImageByName(evoTo.Name);
                pbEvolveRight.Tag = evoTo.Id;
                ttEvo.SetToolTip(pbEvolveRight, String.Format("You can use {0}x {1} evolve a {2}", evoTo.AlternateEvoCardAmount, SelectedCard.Name, evoTo.Name));
            }
            else
            {
                pbEvolveRight.Image = null;
                pbEvolveRight.Tag = 0;
                ttEvo.SetToolTip(pbEvolveRight, "");
            }
        }

        private void UpdateCheckboxHighlightColor()
        {
            Color cDefault = System.Drawing.SystemColors.Control;
            Color cHighlight = System.Drawing.SystemColors.GradientActiveCaption;

            cbStars1.BackColor = SelectedCard.BaseCard.Stars == 1 ? cHighlight : cDefault;
            cbStars2.BackColor = SelectedCard.BaseCard.Stars == 2 ? cHighlight : cDefault;
            cbStars3.BackColor = SelectedCard.BaseCard.Stars == 3 ? cHighlight : cDefault;
            cbStars4.BackColor = SelectedCard.BaseCard.Stars == 4 ? cHighlight : cDefault;
            cbStars5.BackColor = SelectedCard.BaseCard.Stars == 5 ? cHighlight : cDefault;

            cbClassForest.BackColor = SelectedCard.BaseCard.Race == Race.Forest ? cHighlight : cDefault;
            cbClassSwamp.BackColor = SelectedCard.BaseCard.Race == Race.Swamp ? cHighlight : cDefault;
            cbClassMountain.BackColor = SelectedCard.BaseCard.Race == Race.Mountain ? cHighlight : cDefault;
            cbClassTundra.BackColor = SelectedCard.BaseCard.Race == Race.Tundra ? cHighlight : cDefault;
            cbClassOther.BackColor = SelectedCard.BaseCard.Race == Race.Demon || SelectedCard.BaseCard.Race == Race.Feast || SelectedCard.BaseCard.Race == Race.Gold || SelectedCard.BaseCard.Race == Race.Hydra || SelectedCard.BaseCard.Race == Race.Special ? cHighlight : cDefault;

        }

        private void cbStars_CheckedChanged(object sender, EventArgs e)
        {
            UpdateFilter();
        }

        private void cbClass_CheckedChanged(object sender, EventArgs e)
        {
            UpdateFilter();
        }

        private void imageLoader_DoWork(object sender, DoWorkEventArgs e)
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
            pictureBox1.Image = Resources.LoadingSpinner;

            log.DebugFormat("Loading image {0}", (string)e.Argument);
            e.Result = ImageLoader.GetImageByName((string)e.Argument);
           // e.Result = ImageLoader.GetImage((string)e.Argument);
        }

        private void imageLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.Image = (Image)e.Result;
        }

        private void cbSkillFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateFilter();
        }

        private void cbEvolveSkill_SelectedIndexChanged(object sender, EventArgs e)
        {
            Skill s = cbEvolveSkill.SelectedItem as Skill;
            if (s == null)
            {
                log.DebugFormat("Removing evolve skill");
                SelectedCard.SkillNew = 0;
            }
            else
            {
                log.DebugFormat("Changed Evolution skill from {0} to {1}", SelectedCard.SkillNew, s.SkillId);
                SelectedCard.SkillNew = s.SkillId;
            }
        }

        private void cbDisplayOnlyOwnedCards_CheckedChanged(object sender, EventArgs e)
        {
            EnableStatEditting(!cbDisplayOnlyOwnedCards.Checked);
            UpdateFilter();
        }

        public void EnableStatEditting(bool isEditEnabled)
        {
            numLevel.Enabled = isEditEnabled;
            cbEvolveSkill.Enabled = isEditEnabled;
            numTimesEvolved.Enabled = isEditEnabled;
            btnClearEvolveSkill.Enabled = isEditEnabled;
            cbAllEvoSkills.Enabled = isEditEnabled;
        }

        private void btnClearEvolveSkill_Click(object sender, EventArgs e)
        {
            cbEvolveSkill.SelectedItem = null;
        }

        private void comboBox_DropDown(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.PreviewKeyDown += new PreviewKeyDownEventHandler(comboBox_PreviewKeyDown);
        }

        private void comboBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.PreviewKeyDown -= comboBox_PreviewKeyDown;
            if (cbo.DroppedDown) cbo.Focus();
        }

        private void pbEvolve_Click(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            if (pb != null)
            {
                int i = (int)pb.Tag;
                if (i != 0)
                {
                    var card = displayedCards.Find(x => x.CardId == i);
                    if (card != null)
                    {
                        lbCards.SelectedItem = card;
                    }
                }
            }
        }

        private void cbAllEvoSkills_CheckedChanged(object sender, EventArgs e)
        {
            var selectedItem = SelectedCard.SkillNew != 0 ? Skill.Get(SelectedCard.SkillNew) : null;
            if (cbAllEvoSkills.Checked)
            {
                cbEvolveSkill.DataSource = Skill.Skills.OrderBy(x => x.Name).ToList();
            }
            else
            {
                cbEvolveSkill.DataSource = Skill.Skills.FindAll(x => x.EvoRank > 0 || x.SkillId == 1034 || x.SkillId == 649 || x.SkillId == 1209).OrderBy(x => x.Name).ToList();
            }
            cbEvolveSkill.SelectedItem = selectedItem;
        }

        private void CardSearcher_Shown(object sender, EventArgs e)
        {
            Console.WriteLine("CardSearcher_Shown");

            cbSkillFilter.SelectedIndexChanged -= cbSkillFilter_SelectedIndexChanged;
            lbCards.SelectedIndexChanged -= lbCards_SelectedIndexChanged;
            cbDisplayOnlyOwnedCards.CheckedChanged -= cbDisplayOnlyOwnedCards_CheckedChanged;

            if (preselectedCard != null)
            {
                UpdateFilter();
                // TODO: Ensure that the filter settings are okay
                var card = displayedCards.FirstOrDefault(x => x.CardId == preselectedCard.CardId);
                log.DebugFormat("Setting selected card to {0}", card);
                lbCards.SelectedItem = card;
            }
            else if (lbCards.Items.Count > 0)
            {
                SetCard(lbCards.Items[0] as UserCard);
                lbCards.SelectedIndex = 0;
            }
            
            // Remove any previously entered text
            tbFilter.Text = "";
            UpdateFilter();
            UpdateLabels();
            ActiveControl = tbFilter;//.Focus();



            cbDisplayOnlyOwnedCards.CheckedChanged += cbDisplayOnlyOwnedCards_CheckedChanged;
            lbCards.SelectedIndexChanged += lbCards_SelectedIndexChanged;
            cbSkillFilter.SelectedIndexChanged += cbSkillFilter_SelectedIndexChanged;
        }
    }
}
