0.0.9.2
Improvements
- Display the name of the player in the deck editors when loading a FoH deck from the menu

Bugfixes
- Fixed issue where a card with double resurrection (due to Dirt rune) would be added to your hand twice
- Fixed issue where Confusion would pick 3 targets (like Field of Chaos)

0.0.9.1
New
- The following skills are now working: Diana's Protection, Water Shield

Improvements
- Improved usability of the Card Searcher - no longer remembers the last searched card, only the filter settings.

Bugfixes
- Fixed issue where the KW decks would appear in the EW menu

0.0.9.0
New
- You can now use your email address or UIN to log in on the game server. If you choose to do this, your own decks and cards will be downloaded. You can use the card searcher to display only the cards which you own.
- If a new update is released, you will now get a notication when you start the program.
- Death Marker implemented
- Dying Strike implemented

Improvements
- Card searcher will now remember your filter settings and previously added card. 
- Kingdom war is now capped at 500 fights per life


Bugfixes
- Fixed issue where decks with [Race] Guard cards would give too positive results in KW mode
- Fix some variables not being sent correctly to the game server
- Fixed issues loading invalid replay files
- Fixed issue where a KW replay would not display the correct health amounts
- Asura's Flame and the direct damage part of Apocalypse are now properly reflected
- Fixed issue where exiling summoned cards would return it to your deck, causing it to be drawn and entering play as if it were a normal card.
- Fixed issue where hero level would not always be properly applied
- Fixed issues loading invalid EW and KW deck lists

0.0.8.2
New
- The following skills are now working: Mana burn, Field of Chaos, Divine Shield, Terror Roar, Dread Roar, Spine, Block, Asura's Flame, Apocalypse, Cerberus, Plague Blast and Demon Skin

Bugfixes
- Fixed issue where Hydra / Demon's race would be changed by Corruption (and its variants)
- Fixed issue when replacing the normal cards.json with a Chinese version
- Fixed issue where a Giant Mollusc deck would give lower results then expected against Bahamut
- Fixed crash when trying to save your deck after starting for the first time

0.0.8.1
Improvements
- Results can now be copied

Bugfixes
- Fixed issue where loading new skill data from the server would cause the program to not start anymore

0.0.8.0
New
- Major UI changes. 
- Support for Kingdom War.
- Results are now also displayed in a graph besides the numbers.
- Your saved decks are now stored in a different folder, so if you download a new version of the sim which you run from a different folder, your custom decks are still there.
- Added import tool to load Peppa, Mitzi or Crystark deck files from file
- Added export tool to save decks in Peppa, Mitzi or Crystark deck format

Improvements
- Improved performance of skill lookups in certain cases
- Main window is now resizable
- Rune images are now shown in the deck editor
- Rune skill is now shown when holding your mouse over a rune in the deck editor
- Hide the deck cost indicator for level 0 players (like Demons)
- Hero level is now stored with the deck
- Added button to delete selected deck (can also use delete on your keyboard)

Bugfixes
- Fixed issue loading a map level where the opponent has no runes (like 1-1)
- Fixed issue where a card with Last Chance would refuse to die
- Fixed issue where decks containing cards with Last Chance would sim very slowly
- Fixed issue where your selected server is not remembered


0.0.7.6
Bugfixes
- Fixed issue where a card dying in its own turn due to retal or poison would cause another card to be skipped that turn.
- Fixed issue where a card dying from a multi-target skill would cause other targets to be skipped.

0.0.7.5
New
- Added EW bosses. Using roughly the Season 2 rules, where the minions have 10% attack/hp and the boss has a HP bonus (using x16 - should be enough to show merit per fight)
You can edit the EW bosses in cache/decks_EW.txt. This file uses Mitzi's deck format.

Bugfixes
- Fixed error message after loading data from server
- Fixed issue where the deck cost meter did not include evolution costs

0.0.7.4
Improvements
- Minor performance improvements

Bugfixes
- Purification now also works on Aries 
- Fixed player health display issue in replayer
- Card images are now downloaded again


0.0.7.3
New
- Added support for cards with 5 skills (the Legendary cards for Elemental Wars have this)

Improvements
- Replay viewer now shows a list of all replays in the default folder

Bugfixes
- Fixed crash when simulating a fight where both sides are performing lethal OnDeath skills in the same attack
- Fixed crash when loading the new card data file (due to the Legendary cards having 5 skills)

0.0.7.2
New
- Purify implemented
- Last Chance implemented
- Bloody Battle implemented

Bugfixes
- Divine Plea no longer activates when there is no card with a status ailment on your battlefield
- Blight Stone now properly activates
- Disabled automatic card downloader as it crashes the application in certain installations
- Fixed issue where Thunder Dragons summoned by Dragon Summoner would resurrect
- Fixed issue where summoned cards would be added to your deck
- Fixed issue where Destroy leaves a card on the battlefield with 0 hp
- Fixed issue where Silence would not remove certain characteristics, like Immunity and Infiltrator
- Fixed issue where Silence would not be cleared after surving some Ancient Garuda onslaught.
- Fixed issue where sacrificed cards would remain on the battlefield

0.0.7.1
Improvements
- Memory usage should not be going sky-high anymore
- MPM is no longer hidden behind some buttons. 
- Replayer and data downloader have gotten a menu item

Bugfixes
- Fixed crash upon startup

0.0.7.0
New
- Added a replay viewer. You can now save manual replays, and when simming multiple battles, the best and worst Merit hit will be saved, or a winning and losing battle.
  Please note that this is still somewhat experimental, and it will cause simulations to take about 2 times longer.
- Implemented card summoning (Gang Up! The Don's Bodyguard, etc)

Improvements
- Cached several expensive lookups to increase simulation speed

Bugfixes
- Display silenced status in manual battles
- Fixed issue displaying more then 10 cards on the battlefield (now that we have summons)
- Fixed issue where AfterDeath skills would fire even when the card is silenced (Demonic Imps resurrecting in fights against Bahamut)
- Fixed display issue where runes will not show as active if the battle is reset / ends while the rune is active.

0.0.6.2
New
- Holy Light, Naturalize, Corruption and Lava Trial implemented

Bugfixes
- Fixed issue where decks aren't loaded anymore
- Fixed issue where Silence would not be applied to cards with immunity
- Fixed issue where a silenced card that is reanimated would not get its skills back
- Fixed rare issue where DI merit is not correct

0.0.6.1
Bugfixes
- Fixed issue where the GUI would not start at all

0.0.6.0
New
- Added console application. Only FoH and DI are supported for now. 
- Display total deck cost
- Able to set the number of times a card is evolved (to ensure the correct cost is used for unleveled cards)
- Silence skill implemented - Bahamut should be working now

0.0.5.7
Bugfixes
- Fixed performance issues with FoH simulation

0.0.5.6
New
- Frost Shock skill implemented
- Shield of Earth skill implemented
- Inspire skill implemented (not available ingame yet)
- When you run out of life, the battle immediately ends instead of at the end of the round

Improvements
- When connecting to a server, download all data files. No need to click all the buttons one by one anymore.
- Tooltips with the full status ailment name on mouseover
- You are now able to select any skill as evolve skill in the card searcher

Bugfixes
- FoH progress bar will now properly end at 100%
- Evolution skills on map stages will now be properly loaded
- Several runes will no longer activate if you have no cards on the battlefield, costing precious charges 
    Runes affected: Arctic Freeze, Blood Stone, Dirt, Explosion, Fire Forge, Flying Stone, Frost Bite, Ghost Step,
                    Ice Wall, Lore, Mineral, Nimble Soul, Raised Flag, Red Valley, Revival, Spring Breeze, 
                    Stone Forest, Stonewall, Thunder Shield, Transparency, Tsunami  
- Results should now be cleared when upgrading to a new version

0.0.5.5
New
- The FoH simulator is now able to determine who will be attacker in a match. This means that there will be no more chance-range (unless you disable this mode)

Improvements
- Display version and number of iterations in the FoH simulator

Bugfixes
- Guard damage overkill is now properly applied to the player

0.0.5.4
Improvements
- Your latest fight will now be automatically loaded when you reopen the application

Bugfixes
- Fixed issue where Fireball, Firewall and Firestorm (and their Quick Strike / Desperation equivalents) would always to their lowest possible damage

0.0.5.3
Bugfixes
- Fixed issue where the runes of map levels would not be loaded
- Fixed crash when loading a FoH match where a player has an avatar of a card that has not retrieved from the server yet
- Greatly reduced the chance that demons and thieves will die due to not enough 'hero hp'
- Fixed a crash with Healing Mist
- Fixed issue where no logging would be visible in the log window
- Fixed FoH icon for players who have Dumpling Feast as avatar

0.0.5.2
Improvements
- Display status ailments

Bugfixes
- Fixed issue when loading a FoH round where someone has the default male/female avatar
- Fire God and Combustion will no longer be applied to cards with Immunity
- Fixed crash when trying to change a demon card
- Fixed issue where the rune status would not be properly displayed when resetting the battle


0.0.5.1
Improvements
- The Field of Honor simulator is looking a bit prettier now.
- Loading your settings (like number of rounds and selected server) after updating
- Added url to website in about dialog

Bugfixes
- Fixed Advanced Strike and Warcry not reducing but increasing the wait time of your own cards.
- Fixed a crash when loading a Field of Honor round where more then 2147483647 honor has been bet


0.0.5.0
New
- You are now able to select a level 100 (legendary) thief as opponent. Tweak your thief decks!
- Now able to load map stages from the server
- The card searcher now displays the evolution chain of the selected card. Clicking on the cards select them in the list, unless they are hidden due to filters

Improvements
- In several places, images are loaded in the background to make the application more responsive
- When you select a different card, its level is reset to 10 and its evo skill is removed.
- When editing a card, it will now be selected in the list instead of having its name as filter.
- Remember hero level when closing application
- Slightly moved the deck editor to remove the clipping
- Slightly adjusted the FoH simulator fight results output
- Highlight stars / race of the selected card in the Card Searcher
- Remember the last server you have connected to

Bugfixes
- Fixed crash with Mineral rune


0.0.4.2
Bugfixes
- Fixed crash involving a combination of Desperation: Reanimation and Quick Strike: Exile
- Fixed crash when a card dies due to retaliation


0.0.4.1
Improvements
- Performing multiple FoH simulations adds some extra whitespace

Bugfixes
- Fixed a crash when changing the number of simulations in the FoH window
- Desperation: Reanimation is now working


0.0.4.0
New
- Results of simulations are now stored in a file. You can look back at past results in Options -> Results. 

Improvements
- Search by rune skill as well (so you can just type in 'snipe' to get Leaf and Flying Stone)
- Now able to remove runes in the deck editor. Just try right-clicking on one.
- Display if a rune is active on the battlefield display
- Reduced size of deck editor images to make room for more usefull stuff.
- Now able to remove decks (just select one in the editor and click on the delete button).

Bugfixes
- Fixed soem typo's!
- Mineral rune is now working
- Fixed crash when loading a completed Field of Honor season from the server (between 23:00 and 0:00 server time)
- Fixed battlefield rune display, where runes would not always be properly removed.
- Corrected the order of AfterAttack skills (Bloodsucker, Weaken, Blight, Bloodthirsty, Laceration and Puncture) and AfterDefend skills (Counterattack, Retaliation, Craze, Devil's Armor, Combustion and Wicked Leech)
- Clean Sweep secondary targets no longer ignore AfterAttack and AfterDefend skills
- Fixed crash if there is no leagueinfo.json present


0.0.3.1
Bugfixes
- Fixed crash after closing the LogWindow
- Fixed issues obtaining Field of Honor data from the server
- Fixed issue introduced in 0.0.3.0 where some skills would not work anymore
- Corrected the round 51 and beyond damage.


0.0.3.0
New
- Field of Honor menu added to Opponent. This menu is filled with recent FoH decks.
- Easy simulator for Field of Honor rounds. Check it out in Opponent -> Field of Honor -> FoH Simulator. You can connect to a game server in this dialog and load the current Field of Honor decks. You can also load card, rune and skill data from the server here.

Improvements
- Display cooldown time and MPM (merit per minute) when simulating a demon fight

Bugfixes
- Spring Breeze rune is now working
- Advanced Strike no longer causes the application to crash
- Revival rune is now working


0.0.2.1
New
- Added button to clear simulation results

Improvements
- Made editing deck more responsive by no longer auto-simulating

Bugfixes
- Self-Destruct (and Explosion) are now working
- Advanced Strike is now working
- Warcry is now working
- Combustion is now working
- Dexterity is now working


0.0.2.0
New
- Reset battle
- You can now select the number of simulated battles. Less clicking!
- Show number of times round 100 is reached
- Auto-play a battle

Improvements
- Easier to remove evolve skill
- Reduced lag when editing cards in your deck
- Made the deck-edit buttons more prominent

Bugfixes
- Display correct level when editing a card
- Corrected hero hp for levels above 100
- Fixed issue where a card killed by a skill would still act as a defender
- Fixed issue where you could not properly select an evolve skill if you are searching by name
- Now able to properly see card info on mouseover in the deck editor
- Fixed missing evolve skills Evasion, Ice Shield 4 and Sacred Flame

0.0.1.0
Initial release