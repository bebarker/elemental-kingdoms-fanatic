﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.server;
using ElementalKingdoms.ui;
using ElementalKingdomsGui.Properties;

namespace ElementalKingdoms
{
    public partial class MainForm : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private User user;
        private ServerCommunicator com;

        public MainForm()
        {
            InitializeComponent();

            //*
            com = ServerCommunicator.Create(Settings.Default.DeviceId, true);

            //com.GetAllCard();
            //com.GetAllSkill();
            //com.GetMapStages();
            //com.GetAllRunes();

            com.GetCardGroups();
            com.GetUserLegion();
            com.GetThieves();
            com.GetBackground();
            com.GetActivityInfo();
            com.GetWelfare();
            com.GetUserRunes();
            com.GetUserCards();

            com.GetUserMapStages();
            com.GetFreeCards();
            com.GetSalary();
            com.GetRewards();

            //com.Tutorial();

            //com.GetLeagueInfo();
            com.GetUserInfo();
            //*/

            LoadUserInfo();

            deckEditor1.DeckChanged += deckEditor1_DeckChanged;
            cbDeck.SelectedValueChanged += cbDeck_SelectedValueChanged;
        }

        private void LoadUserInfo()
        {
            user = User.Load();
            userInfo1.SetUser(user);
            setCardGroups();

            deckEditor1.SetUser(user);
        }

        void deckEditor1_DeckChanged(object sender, EventArgs e)
        {
            log.DebugFormat("Deck changed! {0} {1}", deckEditor1.Deck.GroupId, deckEditor1.Deck);
            foreach (var card in deckEditor1.Deck.Cards)
            {
                log.DebugFormat("{0} {1}", card.UserCardId, card.Name);
            }

            com.SetCardGroup(deckEditor1.Deck.GroupId, deckEditor1.Deck.Cards.Select(c => c.UserCardId).ToList(), new List<int>());
        }

        private void setCardGroups()
        {
            cbDeck.DataSource = user.AllDecks;
        }

        void cbDeck_SelectedValueChanged(object sender, EventArgs e)
        {
            log.Error("Need to fix this, SetDeck is no longer available!!!");
            deckEditor1.SetUser(user);
            //throw new NotImplementedException("Need to change this when I get back to working on this form...");
            //deckEditor1.SetDeck(cbDeck.SelectedItem as Deck);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MapStagesForm mapStages = new MapStagesForm();
            mapStages.SetUserMapStages(user);
            mapStages.ShowDialog();
        }

        private void btnSimulate_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not working anymore with the new battleform");
            //new BattleForm().ShowDialog();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            com.GetUserInfo();
            LoadUserInfo();
        }

        private void btnFreeShizzle_Click(object sender, EventArgs e)
        {
            com.GetFreeCards();
            com.GetSalary();
            com.GetRewards();

        }
    }
}
