﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ElementalKingdoms.core
{
    public class BattleUser : INotifyPropertyChanged
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private User _user;

        public string NickName
        {
            get { return _user.NickName; }
        }
        public int MaxHP { get; set; }

        private int currentHP;
        public int CurrentHP { 
            get { return currentHP; } 
            set { 
                if (value > currentHP) 
                {
                    int newHP = Math.Min(MaxHP, value); // Ensure you do not get healed above your max health
                    if (currentHP != newHP)
                    {
                        currentHP = newHP;
                    }
                }
                else
                {
                    currentHP = value;
                }
            } 
        }

        public void UnavoidableDamage(int dmg)
        {
            currentHP -= dmg;
        }

        public List<BattleCard> Deck { get; private set; }
        public BattleField Battlefield { get; private set; }
        public List<BattleCard> Hand { get; private set; }
        public List<BattleCard> Cemetary { get; private set; }

        public List<BattleRune> Runes { get; private set; }

        public string UUID { get; private set; }

        public List<BattleCard> Guards { get; private set; }

        public BattleUser(User u, bool isAttacker)
        {
            _user = u;

            Deck = new List<BattleCard>();
            Battlefield = new BattleField();
            Hand = new List<BattleCard>();
            Cemetary = new List<BattleCard>();

            Guards = new List<BattleCard>();

            MaxHP = u.HP;
            CurrentHP = u.HP;

            UUID = isAttacker ? "atk" : "def";
            int i = 1;

            foreach (UserCard c in u.UserDeck.Cards)
            {
                Deck.Add(new BattleCard(c));
                c.UUID = UUID + "_" + (i++);
            }

            if (u.HasLegendary())
            {
                // Legendary decks are assumed to have *20 hps and *3 atk
                foreach (var card in Deck)
                {
                    if  (card.Race == Race.Hydra)
                    {
                        card.SetBaseStats(card.BaseAttack, card.BaseHP * 16);
                    }
                    else
                    {
                        card.SetBaseStats(card.BaseAttack / 10, card.BaseHP / 10);
                    }
                }
            }
            else if (u.HasHydra())
            {
                /*
                H1 => no bonus
                H2 => HP * 2
                H3 => HP * 3, atk * 1.2
                H4 => HP * 4, atk * 1.5
                H5 => HP * 5, atk * 2
                */
                int hpBonusFactor = 1;
                float atkBonusFactor = 1.0f;

                foreach (var card in Deck)
                {
                    if (card.Race == Race.Hydra && card.Name.StartsWith("Hydra"))
                    {
                        if (card.Name.Contains(" II "))
                        {
                            hpBonusFactor = 2;
                        }
                        else if (card.Name.Contains(" III "))
                        {
                            hpBonusFactor = 3;
                            atkBonusFactor = 1.2f;
                        }
                        else if (card.Name.Contains(" IV "))
                        {
                            hpBonusFactor = 4;
                            atkBonusFactor = 1.5f;
                        }
                        else if (card.Name.Contains(" V "))
                        {
                            hpBonusFactor = 5;
                            atkBonusFactor = 2.0f;
                        }
                    }
                }

                foreach (var card in Deck)
                {
                    card.SetBaseStats((int)(card.BaseAttack * atkBonusFactor), card.BaseHP * hpBonusFactor);
                }
            }

            Deck.Shuffle();

            Runes = new List<BattleRune>();
            foreach (UserRune r in u.UserDeck.Runes)
            {
                Runes.Add(new BattleRune(r));
                r.UUID = UUID + "rune_" + (i++);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
