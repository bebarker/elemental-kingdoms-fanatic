﻿namespace ElementalKingdoms.ui
{
    partial class ServerLoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerLoginForm));
            this.btnLogin = new System.Windows.Forms.Button();
            this.rbGuest = new System.Windows.Forms.RadioButton();
            this.rbArc = new System.Windows.Forms.RadioButton();
            this.rbFacebook = new System.Windows.Forms.RadioButton();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.lblErrors = new System.Windows.Forms.Label();
            this.rbUin = new System.Windows.Forms.RadioButton();
            this.tbUin = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(271, 193);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 28);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Sign in";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // rbGuest
            // 
            this.rbGuest.AutoSize = true;
            this.rbGuest.Checked = true;
            this.rbGuest.Location = new System.Drawing.Point(12, 12);
            this.rbGuest.Name = "rbGuest";
            this.rbGuest.Size = new System.Drawing.Size(67, 21);
            this.rbGuest.TabIndex = 2;
            this.rbGuest.TabStop = true;
            this.rbGuest.Text = "Guest";
            this.rbGuest.UseVisualStyleBackColor = true;
            // 
            // rbArc
            // 
            this.rbArc.AutoSize = true;
            this.rbArc.Location = new System.Drawing.Point(12, 39);
            this.rbArc.Name = "rbArc";
            this.rbArc.Size = new System.Drawing.Size(84, 21);
            this.rbArc.TabIndex = 3;
            this.rbArc.Text = "Arc login";
            this.rbArc.UseVisualStyleBackColor = true;
            this.rbArc.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // rbFacebook
            // 
            this.rbFacebook.AutoSize = true;
            this.rbFacebook.Enabled = false;
            this.rbFacebook.Location = new System.Drawing.Point(12, 66);
            this.rbFacebook.Name = "rbFacebook";
            this.rbFacebook.Size = new System.Drawing.Size(91, 21);
            this.rbFacebook.TabIndex = 4;
            this.rbFacebook.Text = "Facebook";
            this.rbFacebook.UseVisualStyleBackColor = true;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(138, 12);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(202, 22);
            this.tbEmail.TabIndex = 5;
            this.tbEmail.Visible = false;
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(138, 40);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(202, 22);
            this.tbPassword.TabIndex = 6;
            this.tbPassword.Visible = false;
            // 
            // lblErrors
            // 
            this.lblErrors.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrors.Location = new System.Drawing.Point(12, 132);
            this.lblErrors.Name = "lblErrors";
            this.lblErrors.Size = new System.Drawing.Size(334, 58);
            this.lblErrors.TabIndex = 7;
            this.lblErrors.Text = "Placeholder text for login failed. Is extra long so I can test multi-line texts, " +
    "to see if it will fit in nicely";
            // 
            // rbUin
            // 
            this.rbUin.AutoSize = true;
            this.rbUin.Location = new System.Drawing.Point(12, 93);
            this.rbUin.Name = "rbUin";
            this.rbUin.Size = new System.Drawing.Size(50, 21);
            this.rbUin.TabIndex = 8;
            this.rbUin.Text = "Uin";
            this.rbUin.UseVisualStyleBackColor = true;
            this.rbUin.CheckedChanged += new System.EventHandler(this.rbUin_CheckedChanged);
            // 
            // tbUin
            // 
            this.tbUin.Location = new System.Drawing.Point(138, 92);
            this.tbUin.Name = "tbUin";
            this.tbUin.Size = new System.Drawing.Size(202, 22);
            this.tbUin.TabIndex = 9;
            this.tbUin.Visible = false;
            // 
            // ServerLoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 233);
            this.Controls.Add(this.tbUin);
            this.Controls.Add(this.rbUin);
            this.Controls.Add(this.lblErrors);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.tbEmail);
            this.Controls.Add(this.rbFacebook);
            this.Controls.Add(this.rbArc);
            this.Controls.Add(this.rbGuest);
            this.Controls.Add(this.btnLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ServerLoginForm";
            this.Text = "Login";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.RadioButton rbGuest;
        private System.Windows.Forms.RadioButton rbArc;
        private System.Windows.Forms.RadioButton rbFacebook;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label lblErrors;
        private System.Windows.Forms.RadioButton rbUin;
        private System.Windows.Forms.TextBox tbUin;
    }
}