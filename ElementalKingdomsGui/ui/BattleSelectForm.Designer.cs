﻿namespace ElementalKingdomsGui.ui
{
    partial class BattleSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BattleSelectForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.foHSimulatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataLoaderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.replayViewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opponentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thiefToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.level100ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.level100LegendaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.demonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leagueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.elementalWarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.offToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.warningsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLoad = new System.Windows.Forms.Button();
            this.lbCustomDecks = new System.Windows.Forms.ListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClearResults = new System.Windows.Forms.Button();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.btnMultiBattle = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.btnLoadBottom = new System.Windows.Forms.Button();
            this.btnSaveBottom = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnClearBottom = new System.Windows.Forms.Button();
            this.btnSingleFight = new System.Windows.Forms.Button();
            this.rbDefault = new System.Windows.Forms.RadioButton();
            this.rbKingdomWar = new System.Windows.Forms.RadioButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnImportDeck = new System.Windows.Forms.Button();
            this.btnDeleteDeck = new System.Windows.Forms.Button();
            this.btnExportDeck = new System.Windows.Forms.Button();
            this.tbResults = new System.Windows.Forms.TextBox();
            this.hydraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deckEditor2 = new ElementalKingdoms.ui.DeckEditor();
            this.deckEditor1 = new ElementalKingdoms.ui.DeckEditor();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.opponentToolStripMenuItem,
            this.toolStripMenuItem1,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1050, 28);
            this.menuStrip1.TabIndex = 47;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.foHSimulatorToolStripMenuItem,
            this.dataLoaderToolStripMenuItem,
            this.replayViewerToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(57, 24);
            this.toolStripMenuItem3.Text = "Tools";
            // 
            // foHSimulatorToolStripMenuItem
            // 
            this.foHSimulatorToolStripMenuItem.Name = "foHSimulatorToolStripMenuItem";
            this.foHSimulatorToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.foHSimulatorToolStripMenuItem.Text = "FoH simulator";
            this.foHSimulatorToolStripMenuItem.Click += new System.EventHandler(this.leagueToolStripMenuItem_Click);
            // 
            // dataLoaderToolStripMenuItem
            // 
            this.dataLoaderToolStripMenuItem.Name = "dataLoaderToolStripMenuItem";
            this.dataLoaderToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.dataLoaderToolStripMenuItem.Text = "Data loader";
            this.dataLoaderToolStripMenuItem.Click += new System.EventHandler(this.dataLoaderToolStripMenuItem_Click);
            // 
            // replayViewerToolStripMenuItem
            // 
            this.replayViewerToolStripMenuItem.Name = "replayViewerToolStripMenuItem";
            this.replayViewerToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.replayViewerToolStripMenuItem.Text = "Replay viewer";
            this.replayViewerToolStripMenuItem.Click += new System.EventHandler(this.replayViewerToolStripMenuItem_Click);
            // 
            // opponentToolStripMenuItem
            // 
            this.opponentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mapToolStripMenuItem,
            this.thiefToolStripMenuItem,
            this.demonToolStripMenuItem,
            this.hydraToolStripMenuItem,
            this.playerToolStripMenuItem,
            this.leagueToolStripMenuItem,
            this.elementalWarToolStripMenuItem,
            this.customToolStripMenuItem});
            this.opponentToolStripMenuItem.Name = "opponentToolStripMenuItem";
            this.opponentToolStripMenuItem.Size = new System.Drawing.Size(88, 24);
            this.opponentToolStripMenuItem.Text = "Opponent";
            // 
            // mapToolStripMenuItem
            // 
            this.mapToolStripMenuItem.Name = "mapToolStripMenuItem";
            this.mapToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.mapToolStripMenuItem.Text = "Map";
            // 
            // thiefToolStripMenuItem
            // 
            this.thiefToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.level100ToolStripMenuItem,
            this.level100LegendaryToolStripMenuItem});
            this.thiefToolStripMenuItem.Name = "thiefToolStripMenuItem";
            this.thiefToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.thiefToolStripMenuItem.Text = "Thief";
            // 
            // level100ToolStripMenuItem
            // 
            this.level100ToolStripMenuItem.Name = "level100ToolStripMenuItem";
            this.level100ToolStripMenuItem.Size = new System.Drawing.Size(219, 26);
            this.level100ToolStripMenuItem.Text = "Level 100";
            this.level100ToolStripMenuItem.Click += new System.EventHandler(this.level100ToolStripMenuItem_Click);
            // 
            // level100LegendaryToolStripMenuItem
            // 
            this.level100LegendaryToolStripMenuItem.Name = "level100LegendaryToolStripMenuItem";
            this.level100LegendaryToolStripMenuItem.Size = new System.Drawing.Size(219, 26);
            this.level100LegendaryToolStripMenuItem.Text = "Level 100 Legendary";
            this.level100LegendaryToolStripMenuItem.Click += new System.EventHandler(this.level100LegendaryToolStripMenuItem_Click);
            // 
            // demonToolStripMenuItem
            // 
            this.demonToolStripMenuItem.Name = "demonToolStripMenuItem";
            this.demonToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.demonToolStripMenuItem.Text = "Demon";
            // 
            // playerToolStripMenuItem
            // 
            this.playerToolStripMenuItem.Name = "playerToolStripMenuItem";
            this.playerToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.playerToolStripMenuItem.Text = "Player";
            this.playerToolStripMenuItem.Visible = false;
            // 
            // leagueToolStripMenuItem
            // 
            this.leagueToolStripMenuItem.Name = "leagueToolStripMenuItem";
            this.leagueToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.leagueToolStripMenuItem.Text = "Field of Honor";
            // 
            // elementalWarToolStripMenuItem
            // 
            this.elementalWarToolStripMenuItem.Name = "elementalWarToolStripMenuItem";
            this.elementalWarToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.elementalWarToolStripMenuItem.Text = "Elemental War";
            // 
            // customToolStripMenuItem
            // 
            this.customToolStripMenuItem.Name = "customToolStripMenuItem";
            this.customToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.customToolStripMenuItem.Text = "Custom...";
            this.customToolStripMenuItem.Visible = false;
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loggingToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.resultsToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(73, 24);
            this.toolStripMenuItem1.Text = "Options";
            // 
            // loggingToolStripMenuItem
            // 
            this.loggingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.offToolStripMenuItem,
            this.errorsToolStripMenuItem,
            this.warningsToolStripMenuItem,
            this.infoToolStripMenuItem,
            this.debugToolStripMenuItem,
            this.toolStripSeparator1,
            this.toolStripMenuItem2});
            this.loggingToolStripMenuItem.Name = "loggingToolStripMenuItem";
            this.loggingToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.loggingToolStripMenuItem.Text = "Logging";
            // 
            // offToolStripMenuItem
            // 
            this.offToolStripMenuItem.Name = "offToolStripMenuItem";
            this.offToolStripMenuItem.Size = new System.Drawing.Size(146, 26);
            this.offToolStripMenuItem.Text = "Off";
            this.offToolStripMenuItem.Click += new System.EventHandler(this.setDebugLevelToolStripMenuItem_Click);
            // 
            // errorsToolStripMenuItem
            // 
            this.errorsToolStripMenuItem.Name = "errorsToolStripMenuItem";
            this.errorsToolStripMenuItem.Size = new System.Drawing.Size(146, 26);
            this.errorsToolStripMenuItem.Text = "Errors";
            this.errorsToolStripMenuItem.Click += new System.EventHandler(this.setDebugLevelToolStripMenuItem_Click);
            // 
            // warningsToolStripMenuItem
            // 
            this.warningsToolStripMenuItem.Name = "warningsToolStripMenuItem";
            this.warningsToolStripMenuItem.Size = new System.Drawing.Size(146, 26);
            this.warningsToolStripMenuItem.Text = "Warnings";
            this.warningsToolStripMenuItem.Click += new System.EventHandler(this.setDebugLevelToolStripMenuItem_Click);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Checked = true;
            this.infoToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(146, 26);
            this.infoToolStripMenuItem.Text = "Info";
            this.infoToolStripMenuItem.Click += new System.EventHandler(this.setDebugLevelToolStripMenuItem_Click);
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(146, 26);
            this.debugToolStripMenuItem.Text = "Debug";
            this.debugToolStripMenuItem.Click += new System.EventHandler(this.setDebugLevelToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(146, 26);
            this.toolStripMenuItem2.Text = "Show log";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.showLogToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // resultsToolStripMenuItem
            // 
            this.resultsToolStripMenuItem.Name = "resultsToolStripMenuItem";
            this.resultsToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.resultsToolStripMenuItem.Text = "Results";
            this.resultsToolStripMenuItem.Click += new System.EventHandler(this.resultsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(125, 26);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(86, 164);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(68, 23);
            this.btnLoad.TabIndex = 51;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // lbCustomDecks
            // 
            this.lbCustomDecks.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbCustomDecks.FormattingEnabled = true;
            this.lbCustomDecks.ItemHeight = 16;
            this.lbCustomDecks.Location = new System.Drawing.Point(12, 194);
            this.lbCustomDecks.Name = "lbCustomDecks";
            this.lbCustomDecks.Size = new System.Drawing.Size(120, 164);
            this.lbCustomDecks.TabIndex = 50;
            this.lbCustomDecks.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lbDecks_KeyUp);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 164);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(68, 23);
            this.btnSave.TabIndex = 48;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClearResults
            // 
            this.btnClearResults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearResults.Location = new System.Drawing.Point(967, 40);
            this.btnClearResults.Name = "btnClearResults";
            this.btnClearResults.Size = new System.Drawing.Size(71, 63);
            this.btnClearResults.TabIndex = 56;
            this.btnClearResults.Text = "Clear results";
            this.btnClearResults.UseVisualStyleBackColor = true;
            this.btnClearResults.Click += new System.EventHandler(this.btnClearResults_Click);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ElementalKingdomsGui.Properties.Settings.Default, "numberOfSimulations", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.maskedTextBox1.Location = new System.Drawing.Point(492, 194);
            this.maskedTextBox1.Mask = "0000000";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.PromptChar = ' ';
            this.maskedTextBox1.Size = new System.Drawing.Size(69, 22);
            this.maskedTextBox1.TabIndex = 55;
            this.maskedTextBox1.Text = "1000";
            // 
            // btnMultiBattle
            // 
            this.btnMultiBattle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnMultiBattle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMultiBattle.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnMultiBattle.Location = new System.Drawing.Point(492, 295);
            this.btnMultiBattle.Name = "btnMultiBattle";
            this.btnMultiBattle.Size = new System.Drawing.Size(71, 63);
            this.btnMultiBattle.TabIndex = 53;
            this.btnMultiBattle.Text = "Fight!";
            this.btnMultiBattle.UseVisualStyleBackColor = false;
            this.btnMultiBattle.Click += new System.EventHandler(this.btnMultiBattle_Click);
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.AxisY.Title = "# fights";
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(628, 164);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(410, 355);
            this.chart1.TabIndex = 58;
            this.chart1.Text = "chart1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(249, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 23);
            this.button1.TabIndex = 59;
            this.button1.Text = "Map";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(249, 266);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(68, 23);
            this.button2.TabIndex = 60;
            this.button2.Text = "Thief";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(323, 237);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(68, 23);
            this.button3.TabIndex = 61;
            this.button3.Text = "Demon";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(323, 208);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(68, 23);
            this.button4.TabIndex = 62;
            this.button4.Text = "KW";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(249, 237);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(68, 23);
            this.button5.TabIndex = 63;
            this.button5.Text = "EW";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(323, 266);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(68, 23);
            this.button6.TabIndex = 64;
            this.button6.Text = "FoH";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(249, 295);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(68, 23);
            this.button7.TabIndex = 65;
            this.button7.Text = "Custom";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Visible = false;
            // 
            // btnLoadBottom
            // 
            this.btnLoadBottom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadBottom.Location = new System.Drawing.Point(86, 370);
            this.btnLoadBottom.Name = "btnLoadBottom";
            this.btnLoadBottom.Size = new System.Drawing.Size(68, 23);
            this.btnLoadBottom.TabIndex = 67;
            this.btnLoadBottom.Text = "Load";
            this.btnLoadBottom.UseVisualStyleBackColor = true;
            this.btnLoadBottom.Click += new System.EventHandler(this.btnLoadBottom_Click);
            // 
            // btnSaveBottom
            // 
            this.btnSaveBottom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveBottom.Location = new System.Drawing.Point(12, 370);
            this.btnSaveBottom.Name = "btnSaveBottom";
            this.btnSaveBottom.Size = new System.Drawing.Size(68, 23);
            this.btnSaveBottom.TabIndex = 66;
            this.btnSaveBottom.Text = "Save";
            this.btnSaveBottom.UseVisualStyleBackColor = true;
            this.btnSaveBottom.Click += new System.EventHandler(this.btnSaveBottom_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(160, 164);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(68, 23);
            this.btnClear.TabIndex = 68;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnClearBottom
            // 
            this.btnClearBottom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearBottom.Location = new System.Drawing.Point(160, 370);
            this.btnClearBottom.Name = "btnClearBottom";
            this.btnClearBottom.Size = new System.Drawing.Size(68, 23);
            this.btnClearBottom.TabIndex = 69;
            this.btnClearBottom.Text = "Clear";
            this.btnClearBottom.UseVisualStyleBackColor = true;
            this.btnClearBottom.Click += new System.EventHandler(this.btnClearBottom_Click);
            // 
            // btnSingleFight
            // 
            this.btnSingleFight.Location = new System.Drawing.Point(415, 295);
            this.btnSingleFight.Name = "btnSingleFight";
            this.btnSingleFight.Size = new System.Drawing.Size(71, 63);
            this.btnSingleFight.TabIndex = 70;
            this.btnSingleFight.Text = "Single fight";
            this.btnSingleFight.UseVisualStyleBackColor = true;
            this.btnSingleFight.Click += new System.EventHandler(this.btnSingleFight_Click);
            // 
            // rbDefault
            // 
            this.rbDefault.AutoSize = true;
            this.rbDefault.Checked = true;
            this.rbDefault.Location = new System.Drawing.Point(492, 222);
            this.rbDefault.Name = "rbDefault";
            this.rbDefault.Size = new System.Drawing.Size(74, 21);
            this.rbDefault.TabIndex = 71;
            this.rbDefault.TabStop = true;
            this.rbDefault.Text = "Default";
            this.rbDefault.UseVisualStyleBackColor = true;
            this.rbDefault.CheckedChanged += new System.EventHandler(this.rbDefault_CheckedChanged);
            // 
            // rbKingdomWar
            // 
            this.rbKingdomWar.AutoSize = true;
            this.rbKingdomWar.Location = new System.Drawing.Point(492, 249);
            this.rbKingdomWar.Name = "rbKingdomWar";
            this.rbKingdomWar.Size = new System.Drawing.Size(110, 21);
            this.rbKingdomWar.TabIndex = 72;
            this.rbKingdomWar.Text = "Kingdom war";
            this.rbKingdomWar.UseVisualStyleBackColor = true;
            this.rbKingdomWar.CheckedChanged += new System.EventHandler(this.rbKingdomWar_CheckedChanged);
            // 
            // btnImportDeck
            // 
            this.btnImportDeck.Location = new System.Drawing.Point(138, 194);
            this.btnImportDeck.Name = "btnImportDeck";
            this.btnImportDeck.Size = new System.Drawing.Size(68, 23);
            this.btnImportDeck.TabIndex = 73;
            this.btnImportDeck.Text = "Import";
            this.btnImportDeck.UseVisualStyleBackColor = true;
            this.btnImportDeck.Click += new System.EventHandler(this.btnImportDeck_Click);
            // 
            // btnDeleteDeck
            // 
            this.btnDeleteDeck.Location = new System.Drawing.Point(138, 223);
            this.btnDeleteDeck.Name = "btnDeleteDeck";
            this.btnDeleteDeck.Size = new System.Drawing.Size(68, 23);
            this.btnDeleteDeck.TabIndex = 74;
            this.btnDeleteDeck.Text = "Delete";
            this.btnDeleteDeck.UseVisualStyleBackColor = true;
            this.btnDeleteDeck.Click += new System.EventHandler(this.btnDeleteDeck_Click);
            // 
            // btnExportDeck
            // 
            this.btnExportDeck.Location = new System.Drawing.Point(138, 252);
            this.btnExportDeck.Name = "btnExportDeck";
            this.btnExportDeck.Size = new System.Drawing.Size(68, 23);
            this.btnExportDeck.TabIndex = 75;
            this.btnExportDeck.Text = "Export";
            this.btnExportDeck.UseVisualStyleBackColor = true;
            this.btnExportDeck.Click += new System.EventHandler(this.btnExportDeck_Click);
            // 
            // tbResults
            // 
            this.tbResults.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbResults.Location = new System.Drawing.Point(628, 40);
            this.tbResults.Multiline = true;
            this.tbResults.Name = "tbResults";
            this.tbResults.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbResults.Size = new System.Drawing.Size(333, 118);
            this.tbResults.TabIndex = 76;
            // 
            // hydraToolStripMenuItem
            // 
            this.hydraToolStripMenuItem.Name = "hydraToolStripMenuItem";
            this.hydraToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.hydraToolStripMenuItem.Text = "Hydra";
            // 
            // deckEditor2
            // 
            this.deckEditor2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deckEditor2.Location = new System.Drawing.Point(12, 399);
            this.deckEditor2.Name = "deckEditor2";
            this.deckEditor2.Size = new System.Drawing.Size(610, 120);
            this.deckEditor2.TabIndex = 52;
            this.deckEditor2.DeckChanged += new System.EventHandler(this.deckEditor2_DeckChanged);
            this.deckEditor2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.deckEditor2_MouseMove);
            // 
            // deckEditor1
            // 
            this.deckEditor1.Location = new System.Drawing.Point(12, 40);
            this.deckEditor1.Name = "deckEditor1";
            this.deckEditor1.Size = new System.Drawing.Size(610, 118);
            this.deckEditor1.TabIndex = 41;
            this.deckEditor1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.deckEditor1_MouseMove);
            // 
            // BattleSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 531);
            this.Controls.Add(this.tbResults);
            this.Controls.Add(this.btnExportDeck);
            this.Controls.Add(this.btnDeleteDeck);
            this.Controls.Add(this.btnImportDeck);
            this.Controls.Add(this.rbKingdomWar);
            this.Controls.Add(this.rbDefault);
            this.Controls.Add(this.btnSingleFight);
            this.Controls.Add(this.btnClearBottom);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnLoadBottom);
            this.Controls.Add(this.btnSaveBottom);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.btnClearResults);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.btnMultiBattle);
            this.Controls.Add(this.deckEditor2);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.lbCustomDecks);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.deckEditor1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BattleSelectForm";
            this.Text = "Elemental Kingdoms Fanatic";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BattleSelectForm_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private ElementalKingdoms.ui.DeckEditor deckEditor1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem foHSimulatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataLoaderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem replayViewerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opponentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thiefToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem level100ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem level100LegendaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem demonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leagueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem elementalWarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem offToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem errorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem warningsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.ListBox lbCustomDecks;
        private System.Windows.Forms.Button btnSave;
        private ElementalKingdoms.ui.DeckEditor deckEditor2;
        private System.Windows.Forms.Button btnClearResults;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Button btnMultiBattle;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btnLoadBottom;
        private System.Windows.Forms.Button btnSaveBottom;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnClearBottom;
        private System.Windows.Forms.Button btnSingleFight;
        private System.Windows.Forms.RadioButton rbDefault;
        private System.Windows.Forms.RadioButton rbKingdomWar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnImportDeck;
        private System.Windows.Forms.Button btnDeleteDeck;
        private System.Windows.Forms.Button btnExportDeck;
        private System.Windows.Forms.TextBox tbResults;
        private System.Windows.Forms.ToolStripMenuItem hydraToolStripMenuItem;
    }
}