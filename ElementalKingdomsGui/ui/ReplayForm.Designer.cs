﻿namespace ElementalKingdoms.ui
{
    partial class ReplayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReplayForm));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.battleFieldDefender = new ElementalKingdoms.ui.BattleField();
            this.battleFieldAttacker = new ElementalKingdoms.ui.BattleField();
            this.handViewerDefender = new ElementalKingdoms.ui.HandViewer();
            this.handViewerAttacker = new ElementalKingdoms.ui.HandViewer();
            this.lblAttackerName = new System.Windows.Forms.Label();
            this.lblDefenderName = new System.Windows.Forms.Label();
            this.rvDefenderRunes = new ElementalKingdoms.ui.BattleRuneViewer();
            this.rvAttackerRunes = new ElementalKingdoms.ui.BattleRuneViewer();
            this.lblRound = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblQuickStats = new System.Windows.Forms.Label();
            this.lbAttackerDeck = new System.Windows.Forms.ListBox();
            this.lbDefenderDeck = new System.Windows.Forms.ListBox();
            this.lbAttackerCemetary = new System.Windows.Forms.ListBox();
            this.lbDefenderCemetary = new System.Windows.Forms.ListBox();
            this.lblAttackerHP = new System.Windows.Forms.Label();
            this.lblDefenderHP = new System.Windows.Forms.Label();
            this.tbRounds = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMaxRounds = new System.Windows.Forms.Label();
            this.tbOps = new System.Windows.Forms.TextBox();
            this.lblDropFileHint = new System.Windows.Forms.Label();
            this.replayFolderViewer1 = new ElementalKingdomsGui.ui.replay.ReplayFolderViewer();
            ((System.ComponentModel.ISupportInitialize)(this.tbRounds)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(0, 565);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(1346, 184);
            this.textBox1.TabIndex = 0;
            // 
            // battleFieldDefender
            // 
            this.battleFieldDefender.AutoScroll = true;
            this.battleFieldDefender.Location = new System.Drawing.Point(175, 115);
            this.battleFieldDefender.Name = "battleFieldDefender";
            this.battleFieldDefender.Size = new System.Drawing.Size(374, 150);
            this.battleFieldDefender.TabIndex = 54;
            // 
            // battleFieldAttacker
            // 
            this.battleFieldAttacker.AutoScroll = true;
            this.battleFieldAttacker.Location = new System.Drawing.Point(175, 271);
            this.battleFieldAttacker.Name = "battleFieldAttacker";
            this.battleFieldAttacker.Size = new System.Drawing.Size(374, 150);
            this.battleFieldAttacker.TabIndex = 53;
            // 
            // handViewerDefender
            // 
            this.handViewerDefender.Location = new System.Drawing.Point(175, 37);
            this.handViewerDefender.Name = "handViewerDefender";
            this.handViewerDefender.Size = new System.Drawing.Size(340, 80);
            this.handViewerDefender.TabIndex = 52;
            // 
            // handViewerAttacker
            // 
            this.handViewerAttacker.Location = new System.Drawing.Point(175, 413);
            this.handViewerAttacker.Name = "handViewerAttacker";
            this.handViewerAttacker.Size = new System.Drawing.Size(340, 80);
            this.handViewerAttacker.TabIndex = 51;
            // 
            // lblAttackerName
            // 
            this.lblAttackerName.AutoSize = true;
            this.lblAttackerName.Location = new System.Drawing.Point(12, 280);
            this.lblAttackerName.Name = "lblAttackerName";
            this.lblAttackerName.Size = new System.Drawing.Size(91, 17);
            this.lblAttackerName.TabIndex = 50;
            this.lblAttackerName.Text = "Player NAME";
            // 
            // lblDefenderName
            // 
            this.lblDefenderName.AutoSize = true;
            this.lblDefenderName.Location = new System.Drawing.Point(12, 219);
            this.lblDefenderName.Name = "lblDefenderName";
            this.lblDefenderName.Size = new System.Drawing.Size(114, 17);
            this.lblDefenderName.TabIndex = 49;
            this.lblDefenderName.Text = "Opponent NAME";
            // 
            // rvDefenderRunes
            // 
            this.rvDefenderRunes.Location = new System.Drawing.Point(29, 86);
            this.rvDefenderRunes.Name = "rvDefenderRunes";
            this.rvDefenderRunes.Size = new System.Drawing.Size(130, 130);
            this.rvDefenderRunes.TabIndex = 48;
            // 
            // rvAttackerRunes
            // 
            this.rvAttackerRunes.Location = new System.Drawing.Point(29, 300);
            this.rvAttackerRunes.Name = "rvAttackerRunes";
            this.rvAttackerRunes.Size = new System.Drawing.Size(130, 130);
            this.rvAttackerRunes.TabIndex = 47;
            // 
            // lblRound
            // 
            this.lblRound.AutoSize = true;
            this.lblRound.Location = new System.Drawing.Point(524, 507);
            this.lblRound.Name = "lblRound";
            this.lblRound.Size = new System.Drawing.Size(16, 17);
            this.lblRound.TabIndex = 46;
            this.lblRound.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(477, 507);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 45;
            this.label3.Text = "Round";
            // 
            // lblQuickStats
            // 
            this.lblQuickStats.AutoSize = true;
            this.lblQuickStats.Location = new System.Drawing.Point(550, 300);
            this.lblQuickStats.Name = "lblQuickStats";
            this.lblQuickStats.Size = new System.Drawing.Size(0, 17);
            this.lblQuickStats.TabIndex = 44;
            // 
            // lbAttackerDeck
            // 
            this.lbAttackerDeck.FormattingEnabled = true;
            this.lbAttackerDeck.ItemHeight = 16;
            this.lbAttackerDeck.Location = new System.Drawing.Point(12, 436);
            this.lbAttackerDeck.Name = "lbAttackerDeck";
            this.lbAttackerDeck.Size = new System.Drawing.Size(141, 68);
            this.lbAttackerDeck.TabIndex = 42;
            // 
            // lbDefenderDeck
            // 
            this.lbDefenderDeck.FormattingEnabled = true;
            this.lbDefenderDeck.ItemHeight = 16;
            this.lbDefenderDeck.Location = new System.Drawing.Point(12, 12);
            this.lbDefenderDeck.Name = "lbDefenderDeck";
            this.lbDefenderDeck.Size = new System.Drawing.Size(141, 68);
            this.lbDefenderDeck.TabIndex = 41;
            // 
            // lbAttackerCemetary
            // 
            this.lbAttackerCemetary.FormattingEnabled = true;
            this.lbAttackerCemetary.ItemHeight = 16;
            this.lbAttackerCemetary.Location = new System.Drawing.Point(553, 396);
            this.lbAttackerCemetary.Name = "lbAttackerCemetary";
            this.lbAttackerCemetary.Size = new System.Drawing.Size(175, 84);
            this.lbAttackerCemetary.TabIndex = 40;
            // 
            // lbDefenderCemetary
            // 
            this.lbDefenderCemetary.FormattingEnabled = true;
            this.lbDefenderCemetary.ItemHeight = 16;
            this.lbDefenderCemetary.Location = new System.Drawing.Point(555, 44);
            this.lbDefenderCemetary.Name = "lbDefenderCemetary";
            this.lbDefenderCemetary.Size = new System.Drawing.Size(173, 84);
            this.lbDefenderCemetary.TabIndex = 39;
            // 
            // lblAttackerHP
            // 
            this.lblAttackerHP.AutoSize = true;
            this.lblAttackerHP.Location = new System.Drawing.Point(71, 263);
            this.lblAttackerHP.Name = "lblAttackerHP";
            this.lblAttackerHP.Size = new System.Drawing.Size(80, 17);
            this.lblAttackerHP.TabIndex = 38;
            this.lblAttackerHP.Text = "Player LIFE";
            // 
            // lblDefenderHP
            // 
            this.lblDefenderHP.AutoSize = true;
            this.lblDefenderHP.Location = new System.Drawing.Point(70, 237);
            this.lblDefenderHP.Name = "lblDefenderHP";
            this.lblDefenderHP.Size = new System.Drawing.Size(103, 17);
            this.lblDefenderHP.TabIndex = 37;
            this.lblDefenderHP.Text = "Opponent LIFE";
            // 
            // tbRounds
            // 
            this.tbRounds.AutoSize = false;
            this.tbRounds.Location = new System.Drawing.Point(0, 527);
            this.tbRounds.Maximum = 5;
            this.tbRounds.Minimum = 1;
            this.tbRounds.Name = "tbRounds";
            this.tbRounds.Size = new System.Drawing.Size(1065, 32);
            this.tbRounds.TabIndex = 58;
            this.tbRounds.Value = 1;
            this.tbRounds.Scroll += new System.EventHandler(this.tbRounds_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 507);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 17);
            this.label1.TabIndex = 59;
            this.label1.Text = "1";
            // 
            // lblMaxRounds
            // 
            this.lblMaxRounds.AutoSize = true;
            this.lblMaxRounds.Location = new System.Drawing.Point(1049, 507);
            this.lblMaxRounds.Name = "lblMaxRounds";
            this.lblMaxRounds.Size = new System.Drawing.Size(16, 17);
            this.lblMaxRounds.TabIndex = 60;
            this.lblMaxRounds.Text = "1";
            // 
            // tbOps
            // 
            this.tbOps.Location = new System.Drawing.Point(553, 167);
            this.tbOps.Multiline = true;
            this.tbOps.Name = "tbOps";
            this.tbOps.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbOps.Size = new System.Drawing.Size(512, 184);
            this.tbOps.TabIndex = 61;
            // 
            // lblDropFileHint
            // 
            this.lblDropFileHint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDropFileHint.BackColor = System.Drawing.Color.Transparent;
            this.lblDropFileHint.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDropFileHint.ForeColor = System.Drawing.Color.Lime;
            this.lblDropFileHint.Location = new System.Drawing.Point(109, 191);
            this.lblDropFileHint.Name = "lblDropFileHint";
            this.lblDropFileHint.Size = new System.Drawing.Size(1126, 242);
            this.lblDropFileHint.TabIndex = 62;
            this.lblDropFileHint.Text = "Drop a replay json file in this window to watch";
            this.lblDropFileHint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // replayFolderViewer1
            // 
            this.replayFolderViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.replayFolderViewer1.Folder = "D:\\projects\\Games\\ElementalKingdomsGui\\bin\\Debug\\cache\\battle";
            this.replayFolderViewer1.Location = new System.Drawing.Point(1071, 12);
            this.replayFolderViewer1.Name = "replayFolderViewer1";
            this.replayFolderViewer1.Size = new System.Drawing.Size(263, 547);
            this.replayFolderViewer1.TabIndex = 63;
            // 
            // ReplayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1346, 749);
            this.Controls.Add(this.lblDropFileHint);
            this.Controls.Add(this.replayFolderViewer1);
            this.Controls.Add(this.tbOps);
            this.Controls.Add(this.lblMaxRounds);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbRounds);
            this.Controls.Add(this.battleFieldDefender);
            this.Controls.Add(this.battleFieldAttacker);
            this.Controls.Add(this.handViewerDefender);
            this.Controls.Add(this.handViewerAttacker);
            this.Controls.Add(this.lblAttackerName);
            this.Controls.Add(this.lblDefenderName);
            this.Controls.Add(this.rvDefenderRunes);
            this.Controls.Add(this.rvAttackerRunes);
            this.Controls.Add(this.lblRound);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblQuickStats);
            this.Controls.Add(this.lbAttackerDeck);
            this.Controls.Add(this.lbDefenderDeck);
            this.Controls.Add(this.lbAttackerCemetary);
            this.Controls.Add(this.lbDefenderCemetary);
            this.Controls.Add(this.lblAttackerHP);
            this.Controls.Add(this.lblDefenderHP);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "ReplayForm";
            this.Text = "Replayer";
            ((System.ComponentModel.ISupportInitialize)(this.tbRounds)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private BattleField battleFieldDefender;
        private BattleField battleFieldAttacker;
        private HandViewer handViewerDefender;
        private HandViewer handViewerAttacker;
        private System.Windows.Forms.Label lblAttackerName;
        private System.Windows.Forms.Label lblDefenderName;
        private BattleRuneViewer rvDefenderRunes;
        private BattleRuneViewer rvAttackerRunes;
        private System.Windows.Forms.Label lblRound;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblQuickStats;
        private System.Windows.Forms.ListBox lbAttackerDeck;
        private System.Windows.Forms.ListBox lbDefenderDeck;
        private System.Windows.Forms.ListBox lbAttackerCemetary;
        private System.Windows.Forms.ListBox lbDefenderCemetary;
        private System.Windows.Forms.Label lblAttackerHP;
        private System.Windows.Forms.Label lblDefenderHP;
        private System.Windows.Forms.TrackBar tbRounds;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMaxRounds;
        private System.Windows.Forms.TextBox tbOps;
        private System.Windows.Forms.Label lblDropFileHint;
        private ElementalKingdomsGui.ui.replay.ReplayFolderViewer replayFolderViewer1;
    }
}