﻿using ElementalKingdoms.core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.tools
{
    public class CounterdeckGenerator
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        User Defender;
        User Attacker;

        List<UserCard> CardPool;

        public CounterdeckGenerator(User attacker, User defender)
        {
            Defender = defender;
            Attacker = attacker;

            GenerateCardPool();
            GenerateAttackerCards();
        }

        private void GenerateCardPool()
        {
            CardPool = new List<UserCard>();
            foreach (Card c in Card.Cards)
            {
                if (c.Race != Race.Forest && c.Race != Race.Mountain && c.Race != Race.Swamp && c.Race != Race.Tundra)
                {
                    // Useless or unavailable card
                    continue;
                }

                if (c.Stars == 1)
                {
                    continue;
                }
                if (c.Stars == 2)
                {
                    // unless it has a useful skill
                    continue;
                }
                if (c.Stars == 3)
                {
                    // unless it has a useful skill
                    continue;
                }

                CardPool.Add(new UserCard() { CardId = c.Id, Level = 10 });
            }
        }

        private void GenerateAttackerCards()
        {
            // Generate a set of cards that fits in the settings

            // Check all skills that our opponent uses and find counters against those.
            Dictionary<SkillType, int> skillTypePriorities = GetSkillTypePriority();

            foreach (var st in from entry in skillTypePriorities orderby entry.Value descending select entry)
            {
                //Console.WriteLine("{0}: {1}", st.Key, st.Value);

                foreach (UserCard c in CardPool)
                {
                    if (c.ActiveSkills().Find(x => x.Type == st.Key) != null)
                    {
                        //Console.WriteLine("{0}", Card.Cards.Find(x => x.Id == c.CardId).Name);
                        Card baseCard = Card.Cards.Find(x => x.Id == c.CardId);
                        if (baseCard.HpArray[c.Level] > 1500)
                        {
                            Console.WriteLine("{0}", Card.Cards.Find(x => x.Id == c.CardId).Name);
                        }
                    }
                }
            }

            for (int i = 0; i < 10; i++)
            {
                //Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Omniscient Dragon"), Level = 10 });
               // Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Dragon Knight"), Level = 10 });
                Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Santa's helper"), Level = 10 });
            }

            //Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Cerato"), Level = 15, SkillNew = 562 });
            //Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Leprechaun"), Level = 10 });
            //Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Taiga Cleric"), Level = 10 });
            //Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Arctic Defender"), Level = 10 });
            //Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Arctic Defender"), Level = 10 });
            //Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Statue of Light"), Level = 10 });
            //Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Statue of Light"), Level = 10 });
            //Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Statue of Light"), Level = 10 });
            //Attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Statue of Light"), Level = 10 });


        }

        private void AddOrIncrease<T>(Dictionary<T, int> dic, T key, int value)
        {
            if (dic.ContainsKey(key))
            {
                dic[key] += value;
            }
            else
            {
                dic.Add(key, value);
            }
        }

        private Dictionary<SkillType, int> GetSkillTypePriority()
        {
            Dictionary<SkillType, int> priorities = new Dictionary<SkillType,int>();
            List<Skill> opponentSkills = new List<Skill>();
            AddOrIncrease(priorities, SkillType.Resurrection, 1);
            foreach (UserCard c in Defender.UserDeck.Cards)
            {
                foreach (Skill s in c.ActiveSkills())
                {
                    //Console.WriteLine("Type {0}", s.Type);
                    switch (s.Type)
                    {
                        case SkillType.Exile:
                            AddOrIncrease(priorities, SkillType.Immunity, 1);
                            AddOrIncrease(priorities, SkillType.Resistance, 5);
                            break;
                        case SkillType.Destroy:
                            AddOrIncrease(priorities, SkillType.Immunity, 1);
                            AddOrIncrease(priorities, SkillType.Resistance, 5);
                            break;
                        case SkillType.Teleportation:
                            AddOrIncrease(priorities, SkillType.Immunity, 1);
                            AddOrIncrease(priorities, SkillType.Resistance, 5);
                            break;
                        case SkillType.Trap:
                            AddOrIncrease(priorities, SkillType.Immunity, 2); // damn you EK for also giving Evasion SkillType immunity >.<
                            break;
                        case SkillType.MultiHit:
                            AddOrIncrease(priorities, SkillType.CounterAttack, 1);
                            break;
                        case SkillType.PercentageDamageHeal:
                            break;
                        case SkillType.CounterAttack:
                            AddOrIncrease(priorities, SkillType.MultiHit, -2);
                            break;
                        case SkillType.Stun:
                            break;
                        case SkillType.Freeze:
                            break;
                        case SkillType.Damage:
                            break;
                        case SkillType.DamageAndHeal:
                            break;
                        case SkillType.Poison:
                            break;
                        case SkillType.DamageReduction:
                            break;
                        case SkillType.Heal:
                            break;
                        case SkillType.Snipe:
                            break;
                        case SkillType.AttackIncrease:
                            break;
                        case SkillType.HPIncrease:
                            break;
                        case SkillType.Resurrection:
                            break;
                        case SkillType.AttackDecrease:
                            break;
                        case SkillType.Selfdestruct:
                            AddOrIncrease(priorities, SkillType.Exile, 1);
                            AddOrIncrease(priorities, SkillType.Teleportation, 2);
                            break;
                        case SkillType.HeroHeal:
                            break;
                        case SkillType.HeroDamage:
                            AddOrIncrease(priorities, SkillType.Guard, 3);
                            break;
                        case SkillType.AttackIncreaseAfterDealingDamage:
                            break;
                        case SkillType.AttackIncreaseAfterReceivingDamage:
                            break;
                        case SkillType.AttackAndHealthDecrease:
                            break;
                        case SkillType.Reflection:
                            break;
                        case SkillType.Burn:
                            break;
                        case SkillType.IceShield:
                            break;
                        case SkillType.Laceration:
                            break;
                        case SkillType.MagicShield:
                            break;
                        case SkillType.Resistance:
                            break;
                        case SkillType.Immunity:
                            break;
                        case SkillType.Guard:
                            break;
                        case SkillType.Dodge:
                            AddOrIncrease(priorities, SkillType.Puncture, 1); // Damn you once more EK for giving Puncture and Infiltrator the same type >.<
                            break;
                        case SkillType.Sacrifice:
                            break;
                        case SkillType.Puncture:
                            break;
                        case SkillType.Confusion:
                            break;
                        case SkillType.Leech:
                            break;
                        case SkillType.ManaCorruption:
                            break;
                        case SkillType.SacredFlame:
                            break;
                        case SkillType.WaitTimeReduction:
                            break;
                        case SkillType.WaitTimeIncrease:
                            break;
                        case SkillType.NonCombat:
                            break;
                        default:
                            break;
                    }
                }
            }

            return priorities;
        }



    }
}
