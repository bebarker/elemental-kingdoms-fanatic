﻿using System;
using ElementalKingdoms.core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ElementalKingdomsTests
{
    [TestClass]
    public class DataTests
    {
        [TestMethod]
        public void TestDeckCompare()
        {
            Deck d1 = new Deck();
            Deck d2 = new Deck();

            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(true, d1.Equals(d2));
            Assert.AreEqual(true, d2.Equals(d1));

            d1.Cards.Add(new UserCard() { CardId = 1 });

            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(false, d1.Equals(d2));
            Assert.AreEqual(false, d2.Equals(d1));

            d2.Cards.Add(new UserCard() { CardId = 1 });

            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(true, d1.Equals(d2));
            Assert.AreEqual(true, d2.Equals(d1));

            d1.Cards.Add(new UserCard() { CardId = 1 });
            d2.Cards.Add(new UserCard() { CardId = 2 });

            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(false, d1.Equals(d2));
            Assert.AreEqual(false, d2.Equals(d1));

            d1.Cards.Add(new UserCard() { CardId = 2 });
            d2.Cards.Add(new UserCard() { CardId = 1 });

            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(true, d1.Equals(d2));
            Assert.AreEqual(true, d2.Equals(d1));

            d1.Cards.Add(new UserCard() { CardId = 1, Level = 10 });
            d2.Cards.Add(new UserCard() { CardId = 1 });

            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(false, d1.Equals(d2));
            Assert.AreEqual(false, d2.Equals(d1));

            d2.Cards[0].Level = 10;

            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(true, d1.Equals(d2));
            Assert.AreEqual(true, d2.Equals(d1));

            d1.Runes.Add(new UserRune() { RuneId = 1 });
            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(false, d1.Equals(d2));
            Assert.AreEqual(false, d2.Equals(d1));

            d2.Runes.Add(new UserRune() { RuneId = 2 });
            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(false, d1.Equals(d2));
            Assert.AreEqual(false, d2.Equals(d1));

            d1.Runes.Add(new UserRune() { RuneId = 2 });
            d2.Runes.Add(new UserRune() { RuneId = 1 });
            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(true, d1.Equals(d2));
            Assert.AreEqual(true, d2.Equals(d1));

            d1.Cards[0].SkillNew = 1;
            Assert.AreEqual(true, d1.Equals(d1));
            Assert.AreEqual(true, d2.Equals(d2));
            Assert.AreEqual(false, d1.Equals(d2));
            Assert.AreEqual(false, d2.Equals(d1));
        }
    }
}
