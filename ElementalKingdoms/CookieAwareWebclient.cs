﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms
{
    /// <summary>
    /// A Cookie-aware WebClient that will store authentication cookie information and persist it through subsequent requests.
    /// </summary>
    public class CookieAwareWebClient : WebClient
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Properties to handle implementing a timeout
        private int? _timeout = null;
        public int? Timeout
        {
            get
            {
                return _timeout;
            }
            set
            {
                _timeout = value;
            }
        }
        //A CookieContainer class to house the Cookie once it is contained within one of the Requests
        public CookieContainer CookieContainer { get; private set; }
        //Constructor
        public CookieAwareWebClient()
        {
            CookieContainer = new CookieContainer();
#if DEBUG
            //Proxy = new WebProxy("127.0.0.1", 8080) { BypassProxyOnLocal = false };
#endif
        }
        //Method to handle setting the optional timeout (in milliseconds)
        public void SetTimeout(int timeout)
        {
            _timeout = timeout;
        }
        //This handles using and storing the Cookie information as well as managing the Request timeout
        protected override WebRequest GetWebRequest(Uri address)
        {
            //Handles the CookieContainer
            var request = (HttpWebRequest)base.GetWebRequest(address);
            request.CookieContainer = CookieContainer;
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            //Sets the Timeout if it exists
            if (_timeout.HasValue)
            {
                request.Timeout = _timeout.Value;
            }
            return request;
        }


        private void AddQueryParams(NameValueCollection parameters)
        {
            if (parameters == null) return;
            foreach (string key in parameters.AllKeys)
            {
                QueryString.Add(key, parameters[key]);
            }
        }

        public string GetRequest(string url, NameValueCollection queryParams, bool resetQueryString = true)
        {
            if (resetQueryString)
            {
                QueryString.Clear();
            }
            AddQueryParams(queryParams);

            var response = DownloadString(url);

            //log.DebugFormat("GET  Response received was :\n{0}", response);

            return response;
        }


        public string PostRequest(string url, NameValueCollection queryParams)
        {
            byte[] responseArray = UploadValues(url, queryParams);
            
            var response = Encoding.UTF8.GetString(responseArray);
            // Decode and display the response.
            //log.DebugFormat("POST Response received was :\n{0}", response);

            return response;
        }

        public CookieCollection GetAllCookies()
        {
            CookieCollection cookieCollection = new CookieCollection();

            Hashtable table = (Hashtable)CookieContainer.GetType().InvokeMember("m_domainTable",
                                                                            BindingFlags.NonPublic |
                                                                            BindingFlags.GetField |
                                                                            BindingFlags.Instance,
                                                                            null,
                                                                            CookieContainer,
                                                                            new object[] { });

            foreach (var tableKey in table.Keys)
            {
                String str_tableKey = (string)tableKey;

                if (str_tableKey[0] == '.')
                {
                    str_tableKey = str_tableKey.Substring(1);
                }

                SortedList list = (SortedList)table[tableKey].GetType().InvokeMember("m_list",
                                                                            BindingFlags.NonPublic |
                                                                            BindingFlags.GetField |
                                                                            BindingFlags.Instance,
                                                                            null,
                                                                            table[tableKey],
                                                                            new object[] { });

                foreach (var listKey in list.Keys)
                {
                    String url = "https://" + str_tableKey + (string)listKey;
                    cookieCollection.Add(CookieContainer.GetCookies(new Uri(url)));
                }
            }

            return cookieCollection;
        }
    }
}
