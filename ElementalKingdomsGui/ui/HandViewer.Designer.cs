﻿namespace ElementalKingdoms.ui
{
    partial class HandViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblWait1 = new System.Windows.Forms.Label();
            this.lblWait2 = new System.Windows.Forms.Label();
            this.lblWait3 = new System.Windows.Forms.Label();
            this.lblWait4 = new System.Windows.Forms.Label();
            this.lblWait5 = new System.Windows.Forms.Label();
            this.pbHand5 = new System.Windows.Forms.PictureBox();
            this.pbHand4 = new System.Windows.Forms.PictureBox();
            this.pbHand3 = new System.Windows.Forms.PictureBox();
            this.pbHand2 = new System.Windows.Forms.PictureBox();
            this.pbHand1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbHand5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHand4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHand3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHand2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHand1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblWait1
            // 
            this.lblWait1.AutoSize = true;
            this.lblWait1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblWait1.ForeColor = System.Drawing.SystemColors.Info;
            this.lblWait1.Location = new System.Drawing.Point(7, 53);
            this.lblWait1.Name = "lblWait1";
            this.lblWait1.Size = new System.Drawing.Size(16, 17);
            this.lblWait1.TabIndex = 5;
            this.lblWait1.Text = "0";
            // 
            // lblWait2
            // 
            this.lblWait2.AutoSize = true;
            this.lblWait2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblWait2.ForeColor = System.Drawing.SystemColors.Info;
            this.lblWait2.Location = new System.Drawing.Point(72, 53);
            this.lblWait2.Name = "lblWait2";
            this.lblWait2.Size = new System.Drawing.Size(16, 17);
            this.lblWait2.TabIndex = 6;
            this.lblWait2.Text = "0";
            // 
            // lblWait3
            // 
            this.lblWait3.AutoSize = true;
            this.lblWait3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblWait3.ForeColor = System.Drawing.SystemColors.Info;
            this.lblWait3.Location = new System.Drawing.Point(137, 53);
            this.lblWait3.Name = "lblWait3";
            this.lblWait3.Size = new System.Drawing.Size(16, 17);
            this.lblWait3.TabIndex = 7;
            this.lblWait3.Text = "0";
            // 
            // lblWait4
            // 
            this.lblWait4.AutoSize = true;
            this.lblWait4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblWait4.ForeColor = System.Drawing.SystemColors.Info;
            this.lblWait4.Location = new System.Drawing.Point(202, 53);
            this.lblWait4.Name = "lblWait4";
            this.lblWait4.Size = new System.Drawing.Size(16, 17);
            this.lblWait4.TabIndex = 8;
            this.lblWait4.Text = "0";
            // 
            // lblWait5
            // 
            this.lblWait5.AutoSize = true;
            this.lblWait5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblWait5.ForeColor = System.Drawing.SystemColors.Info;
            this.lblWait5.Location = new System.Drawing.Point(267, 53);
            this.lblWait5.Name = "lblWait5";
            this.lblWait5.Size = new System.Drawing.Size(16, 17);
            this.lblWait5.TabIndex = 9;
            this.lblWait5.Text = "0";
            // 
            // pbHand5
            // 
            this.pbHand5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHand5.Location = new System.Drawing.Point(270, 10);
            this.pbHand5.Name = "pbHand5";
            this.pbHand5.Size = new System.Drawing.Size(60, 60);
            this.pbHand5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbHand5.TabIndex = 4;
            this.pbHand5.TabStop = false;
            // 
            // pbHand4
            // 
            this.pbHand4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHand4.Location = new System.Drawing.Point(205, 10);
            this.pbHand4.Name = "pbHand4";
            this.pbHand4.Size = new System.Drawing.Size(60, 60);
            this.pbHand4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbHand4.TabIndex = 3;
            this.pbHand4.TabStop = false;
            // 
            // pbHand3
            // 
            this.pbHand3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHand3.Location = new System.Drawing.Point(140, 10);
            this.pbHand3.Name = "pbHand3";
            this.pbHand3.Size = new System.Drawing.Size(60, 60);
            this.pbHand3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbHand3.TabIndex = 2;
            this.pbHand3.TabStop = false;
            // 
            // pbHand2
            // 
            this.pbHand2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHand2.Location = new System.Drawing.Point(75, 10);
            this.pbHand2.Name = "pbHand2";
            this.pbHand2.Size = new System.Drawing.Size(60, 60);
            this.pbHand2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbHand2.TabIndex = 1;
            this.pbHand2.TabStop = false;
            // 
            // pbHand1
            // 
            this.pbHand1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHand1.Location = new System.Drawing.Point(10, 10);
            this.pbHand1.Name = "pbHand1";
            this.pbHand1.Size = new System.Drawing.Size(60, 60);
            this.pbHand1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbHand1.TabIndex = 0;
            this.pbHand1.TabStop = false;
            // 
            // HandViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblWait5);
            this.Controls.Add(this.lblWait4);
            this.Controls.Add(this.lblWait3);
            this.Controls.Add(this.lblWait2);
            this.Controls.Add(this.lblWait1);
            this.Controls.Add(this.pbHand5);
            this.Controls.Add(this.pbHand4);
            this.Controls.Add(this.pbHand3);
            this.Controls.Add(this.pbHand2);
            this.Controls.Add(this.pbHand1);
            this.Name = "HandViewer";
            this.Size = new System.Drawing.Size(340, 80);
            ((System.ComponentModel.ISupportInitialize)(this.pbHand5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHand4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHand3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHand2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHand1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbHand1;
        private System.Windows.Forms.PictureBox pbHand2;
        private System.Windows.Forms.PictureBox pbHand4;
        private System.Windows.Forms.PictureBox pbHand3;
        private System.Windows.Forms.PictureBox pbHand5;
        private System.Windows.Forms.Label lblWait1;
        private System.Windows.Forms.Label lblWait2;
        private System.Windows.Forms.Label lblWait3;
        private System.Windows.Forms.Label lblWait4;
        private System.Windows.Forms.Label lblWait5;
    }
}
