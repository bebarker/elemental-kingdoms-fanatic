﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElementalKingdoms.core;

namespace ElementalKingdomsGui.util
{
    public class RecentData
    {
        public User Attacker { get; set; }
        public User Defender { get; set; }

        public User LoggedInUser { get; set; }
    }
}
