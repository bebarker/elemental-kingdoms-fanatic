﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.util;

namespace ElementalKingdoms.ui
{
    public partial class BattleRuneViewer : UserControl
    {
        private PictureBox[] RunePicBoxes;

        public BattleRuneViewer()
        {
            InitializeComponent();

            RunePicBoxes = new PictureBox[] { pbRune1, pbRune2, pbRune3, pbRune4 };
        }

        public void SetRunes(List<BattleRune> runes)
        {
            for (int i = 0; i < 4; i++)
            {
                if (i < runes.Count)
                {
                    RunePicBoxes[i].Tag = runes[i];
                    RunePicBoxes[i].Image = ImageLoader.GetRuneImageById(runes[i].RuneId);
                }
                else
                {
                    RunePicBoxes[i].Tag = null;
                    RunePicBoxes[i].Image = null;
                }
            }

            UpdateLabels();
        }

        public void UpdateLabels()
        {
            foreach (PictureBox pb in RunePicBoxes)
            {
                BattleRune br = pb.Tag as BattleRune;
                if (br == null) continue;
                if (br.Active)
                {
                    pb.BackColor = Color.Green;
                    pb.Padding = new Padding(5);
                }
                else if (br.NumActivationsLeft == 0)
                {
                    pb.BackColor = Color.Red;
                    pb.Padding = new Padding(5);
                }
                else
                {
                    pb.Padding = new Padding(0);
                }
                pb.Refresh();
            }
        }
    }
}
