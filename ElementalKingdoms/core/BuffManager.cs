﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class BuffManager
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Battle battle;
        private Dictionary<BattleUser, Dictionary<BattleCard, List<Skill>>> attackerActiveAttackBuffs;

        public BuffManager(Battle b)
        {
            attackerActiveAttackBuffs = new Dictionary<BattleUser, Dictionary<BattleCard, List<Skill>>>();
            attackerActiveAttackBuffs.Add(b.AttackPlayer, new Dictionary<BattleCard, List<Skill>>());
            attackerActiveAttackBuffs.Add(b.DefendPlayer, new Dictionary<BattleCard, List<Skill>>());
            this.battle = b;
        }

        public void AddBuffs(BattleCard b, BattleUser player)
        {
            var playerActiveBuffs = attackerActiveAttackBuffs[player];
            // First, apply my 'on battlefield' bonusses to all other cards
            foreach (Skill s in b.ActiveSkills().FindAll(x => x.LanchType == LaunchType.WhileOnBattlefield))
            {
                if (s.Type == SkillType.AttackIncrease)
                {
                    if (!playerActiveBuffs.ContainsKey(b))
                    {
                        playerActiveBuffs.Add(b, new List<Skill>());
                    }

                    playerActiveBuffs[b].Add(s);
                }

                foreach (BattleCard target in player.Battlefield)
                {
                    // this should include myself for Divine Protection
                    ApplyBattleFieldBuffsToTarget(b, s, target, player);
                }

                // Add the buff to our cache so it can be applied to all other cards more easily later on
            }

            foreach (var x in playerActiveBuffs)
            {
                if (x.Key == b) continue;
                foreach (Skill s in x.Value)
                {
                    ApplyBattleFieldBuffsToTarget(x.Key, s, b, player);
                }
            }

            // Next, apply the 'on battlefield' bonusses from all other cards to me.
            foreach (BattleCard otherCard in player.Battlefield)
            {
                // this should exlude myself since I already applied my own buffs to myself in the previous part
                if (otherCard == b) continue;

                foreach (Skill s in otherCard.ActiveSkills().FindAll(x => x.LanchType == LaunchType.WhileOnBattlefield))
                {
                    if (s.Type == SkillType.AttackIncrease) continue;
                    ApplyBattleFieldBuffsToTarget(otherCard, s, b, player);
                }
            }
        }
        
        private void ApplyBattleFieldBuffsToTarget(BattleCard b, Skill s, BattleCard target, BattleUser player)
        {
            // 0 -> buff self and left + right neighbour (Divine Protection only at the moment)
            // 1-4 -> buff all cards of certain realm
            // 5 -> buff all cards
            if (s.AffectValue2 == 0)
            {
                // Divine protection, must be a neighbour or self
                if (target != b)
                {
                    int myBattlefieldIndex = player.Battlefield.IndexOf(b);
                    if (myBattlefieldIndex == -1)
                    {
                        // Impossible?
                        throw new ArgumentException("Trying to apply buffs by a card that is not currently on the battlefield");
                    }

                    int targetBattlefieldIndex = player.Battlefield.IndexOf(target);
                    if (myBattlefieldIndex == -1)
                    {
                        // Impossible?
                        throw new ArgumentException("Trying to apply buffs to a card that is not currently on the battlefield");
                    }

                    int diff = myBattlefieldIndex - targetBattlefieldIndex;

                    if (Math.Abs(diff) > 1)
                    {
                        // Not a neighbour
                        return;
                    }
                }
            }
            else if (s.AffectValue2 > 5)
            {
                log.ErrorFormat("Detected a skill with an unhandled AffectValue2: {0}", s);
                throw new ArgumentException("Detected a skill with an unhandled AffectValue2");
            }
            else
            {
                // All other buffs exclude self
                if (b == target) return;
            }

            if (s.AffectValue2 > 0 && s.AffectValue2 < 5)
            {
                // Race must match
                if ((int)target.Race != s.AffectValue2) return;
            }

            if (s.Type == SkillType.HPIncrease)
            {
                target.BonusSkillHP += s.AffectValue;
            }
            else if (s.Type == SkillType.AttackIncrease)
            {
                if (battle.StoreReplay) battle.AddOp(OpType.CardModifyAttack, b.Card.UUID, new List<string>() { target.Card.UUID }, s.AffectValue);
                target.BonusSkillAttack += s.AffectValue;
            }
        }

        public void RemoveBuffs(BattleCard card, BattleUser player)
        {
            BattleField battleField = player.Battlefield;
            
            List<BattleCard> targets;

            bool crazeOnlyOnce = true;

            foreach (Skill s in card.ActiveSkills().FindAll(x => x.LanchType == LaunchType.WhileOnBattlefield))
            {
                // 1-4 -> buff all cards of certain realm
                // 5 -> buff all cards
                // 0 -> buff self and left + right neighbour (Divine Protection only at the moment)
                if (s.AffectValue2 == 0)
                {
                    int myIndex = battleField.IndexOf(card);
                    targets = new List<BattleCard>();
                    if (myIndex > 0 && battleField[myIndex - 1] != null)
                    {
                        targets.Add(battleField[myIndex - 1]);
                    }
                    if (myIndex < battleField.Count() - 1 && battleField[myIndex + 1] != null)
                    {
                        targets.Add(battleField[myIndex + 1]);
                    }
                }
                else if (s.AffectValue2 > 0 && s.AffectValue2 < 5)
                {
                    targets = battleField.ToList().FindAll(x => x != card && (int)x.Race == s.AffectValue2).ToList();
                }
                else
                {
                    targets = battleField.ToList().FindAll(x => x != card).ToList();
                }

                if (s.Type == SkillType.HPIncrease)
                {
                    targets.ForEach(target => target.BonusSkillHP -= s.AffectValue);
                }
                else if (s.Type == SkillType.AttackIncrease)
                {
                    
                    attackerActiveAttackBuffs[player].Remove(card);
                    foreach (var target in targets)
                    {
                        // Craze bug: If target has craze, it does not lose the full amount, but the craze value is subtracted
                        int totalCraze = 0;
                        if (crazeOnlyOnce && target.CrazeSources.Count > 0) // but we only substract craze from the first Force (so a RN dieing with FF7 + FF6 causes a RW to lose 175 + 150 - 60, not 175 - 60 + 150 - 60)
                        {
                            totalCraze = target.ActiveSkills().FindAll(craze => craze.Type == SkillType.AttackIncreaseAfterReceivingDamage && target.CrazeSources.Contains(craze.GetHashCode())).Sum(x => x.AffectValue);
                            //if (totalCraze > 0)
                            //{
                            //    log.DebugFormat("Craze bug: excluding {0} ", totalCraze);
                            //}
                        }
                        target.BonusSkillAttack -= s.AffectValue - totalCraze;
                    }
                    crazeOnlyOnce = false;
                }
            }
        }
    }
}
