﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class SkillExecutor
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Battle b;

        // s is the working skill, which might change in case of Quick Strike / Desperation
        private Skill s;
        // originalSkill always contains the original skill, which is needed for Fire God to see the difference between QS: FG10 and Desp: FG10 (since both will be internally converted to FG10)
        private readonly Skill originalSkill;
        private BattleUser you;
        private BattleUser opponent;
        private BattleCard source;

        private string sourceUUID;

        public SkillExecutor(Battle b, Skill s, BattleUser you, BattleUser opponent, BattleCard source)
        {
            this.b = b;
            this.s = s;
            this.originalSkill = s;
            this.you = you;
            this.opponent = opponent;
            this.source = source;

            if (source != null)
            {
                sourceUUID = source.Card.UUID;
            }

            ExecuteSkill();
        }

        private void ExecuteSkill()
        {

            switch (s.LanchType)
            {
                case LaunchType.BeforeAttack:
                    break;
                case LaunchType.AfterAttack:
                    break;
                case LaunchType.AfterDefend:
                    break;
                case LaunchType.OnSkillExecutePhase:
                    break;
                case LaunchType.BeforeDefend:
                    break;
                case LaunchType.WhileOnBattlefield:
                    break;
                case LaunchType.Regen:
                    break;
                case LaunchType.AfterDeath:
                    break;
                case LaunchType.OnDeath:
                    if (s.AffectType == 53)
                    {
                        // Desperation: XXX, we are actually casting another skill (id is in AffectValue)
                        s = Skill.Get(s.AffectValue);
                    }
                    break;
                case LaunchType.OnEnterBattlefield:
                    if (s.AffectType == 59)
                    {
                        // Quick Strike: XXX, we are actually casting another skill (id is in AffectValue)
                        s = Skill.Get(s.AffectValue);
                    }
                    break;
                case LaunchType.OnElementalHit:
                    break;
                case LaunchType.OnInstagibHit:
                    break;
                case LaunchType.OnSkillHit:
                    break;
                case LaunchType.OnHeroDamaged:
                    break;
                case LaunchType.Runes:
                    UseRuneSkill();
                    return;
                default:
                    break;
            }


            log.DebugFormat("Executing skill {0} {1} {2} {3} {4} {5} {6} {7}", s.Name, s.AffectType, s.LanchCondition, s.SkillCategory, s.Type, s.AffectValue, s.AffectValue2, s.AffectValueExtra);

            if (s.Type == SkillType.AttackIncrease)
            {
                if (s.AffectType == 72)
                {
                    // Mania
                    DamageCard(source, s.AffectValue);
                    source.CurrentAttack += s.AffectValue;
                }
                else if (s.AffectType == 111)
                {
                    // Dying Strike
                    int damage = source.BaseAttack * s.AffectValue / 100;
                    int myIndex = you.Battlefield.IndexOf(source);
                    var target = opponent.Battlefield[myIndex];
                    if (target != null)
                    {
                        log.DebugFormat("Dying strike: hitting {0} for {1} (current attack={2}, bonus={3}%)", target.Name, damage, source.BaseAttack, s.AffectValue);
                        DamageCard(target, damage);
                    }
                }
                else
                {
                    log.Warn("Unhandled attack increase skill");
                }
            }
            else if (s.Type == SkillType.HeroDamage)
            {
                log.DebugFormat("Hero damage");
                if (s.AffectType == 34)
                {
                    // Obstinacy
                    b.DamageHero(sourceUUID, you, s.AffectValue, true);
                    log.DebugFormat("Obstinacy - Dealing {0} damage (HP down to {1}", s.AffectValue, you.CurrentHP);
                    // TODO: Check if that didn't kill us.

                }
                else
                {
                    // Curse and Damnation
                    int damage = s.AffectValue;
                    if (s.AffectType == 67)
                    {
                        // Damnation
                        damage *= opponent.Battlefield.Count;
                    }
                    log.DebugFormat("Dealing {0} damage to the hero", damage);
                    b.DamageHero(sourceUUID, opponent, damage, true);
                }
            }
            else if (s.Type == SkillType.Heal)
            {
                Heal();
            }
            else if (s.Type == SkillType.PercentageHeal)
            {
                PercentageHeal();
            }
            else if (s.Type == SkillType.HeroHeal)
            {
                b.HealHero(sourceUUID, you, s.AffectValue);
            }
            else if (s.Type == SkillType.Resurrection)
            {
                if (you.Cemetary.Count > 0)
                {
                    log.DebugFormat("Resurrection skill {0}", s.Name);

                    if (s.AffectType == 44)
                    {
                        // Making a copy with ToList so we can safely remove items from the actual Cemetary.
                        var reincarnateTargets = you.Cemetary.TakeRandom(s.AffectValue).ToList();
                        //Reincarnation atk_4 [atk_10] --> 0
                        foreach (var c in reincarnateTargets)
                        {
                            if (b.StoreReplay) b.AddOp(OpType.Reincarnation, sourceUUID, new List<string>() { c.Card.UUID }, 0);
                            you.Cemetary.Remove(c);
                            you.Deck.Add(c);
                            //CemetaryToDeck atk_10 [atk_10] --> 0
                            if (b.StoreReplay) b.AddOp(OpType.CemetaryToDeck, c.Card.UUID, new List<string>() { c.Card.UUID }, 0);
                        }
                    }
                    else if (s.AffectType == 27)
                    {
                        if (!you.Cemetary.Contains(source))
                        {
                            // Already resurrected - this can happen if the card itself has resurrection and the Dirt rune is active.
                            log.Debug("Already resurrected");
                        }
                        else
                        if (Util.PercentChance(s.AffectValue))
                        {
                            log.DebugFormat("Resurrected!");
                            you.Cemetary.Remove(source);
                            if (you.Hand.Count >= Battle.MAX_HAND_SIZE)
                            {
                                if (b.StoreReplay) b.AddOp(OpType.BattlefieldToLibrary, sourceUUID, new List<string>() { sourceUUID }, 0);
                                you.Deck.Add(source);
                            }
                            else
                            {
                                if (b.StoreReplay) b.AddOp(OpType.CemetaryToHand, sourceUUID, new List<string>() { sourceUUID }, 0);
                                you.Hand.Add(source);
                            }
                        }
                    }
                    else
                    {
                        log.DebugFormat("Unhandled resurrection skill {0} {1} {2} {3}", s.Name, s.LanchCondition, s.SkillCategory, s.Type);
                    }
                    //var validReanimationTargets = you.Cemetary.FindAll(x => !x.HasImmunity && !x.HasReanimation);
                    //if (validReanimationTargets.Count > 0)
                    //{
                    //    BattleCard bc = validReanimationTargets.TakeRandom(1).ElementAt(0);
                    //    log.DebugFormat("Reanimating {0}", bc);
                    //    bc.Reset();
                    //    bc.OnDeath += b_OnDeath;
                    //    // TODO: Fire Quick Strike skills
                    //    you.Cemetary.Remove(bc);
                    //    you.Battlefield.Add(bc);

                    //    List<BattleCard> tmp = new List<BattleCard>() { bc };
                    //    you.Battlefield.FindAll(x => x != bc).ForEach(y => ApplyBattlefieldBuffs(y, tmp));
                    //}
                }
            }
            else  if (s.Type == SkillType.Snipe)
            {
                int numTargets = Math.Max(1, s.AffectValue2); // dual snipe has 2 filled in, but the single snipes have 0...

                foreach (var target in opponent.Battlefield.OrderBy(x => x.CurrentHP).Take(numTargets))
                {
                    if (target == null) continue; // There were enough targets at the start of the turn, but due to casualities there are not enough anymore (defenders array has null values) 
                    log.DebugFormat("Sniped {0} for {1}", target, s.AffectValue);
                    DamageCard(target, s.AffectValue);
                }
            }
            else if (s.Type == SkillType.Poison || s.Type == SkillType.Stun || s.Type == SkillType.Freeze || s.Type == SkillType.Damage || s.Type == SkillType.DamageAndHeal
                || s.Type == SkillType.Destroy || s.Type == SkillType.ManaCorruption || s.Type == SkillType.AttackDecrease || s.Type == SkillType.Confusion || s.Type == SkillType.AttackAndHealthDecrease
                || s.Type == SkillType.FrostShock || s.Type == SkillType.Asura)
            {
                FindDefenderTarget();
            }
            else if (s.Type == SkillType.Trap)
            {
                Trap();
            }
            else if (s.Type == SkillType.Exile)
            {
                // this is not handled here at the moment, but in Battle. Added the case to prevent the warnings below from appearing.
            }
            else if (s.Type == SkillType.SacredFlame)
            {
                if (opponent.Cemetary.Count > 0)
                {
                    var target = opponent.Cemetary.TakeRandom(1).First();
                    log.DebugFormat("Sacred Flame: Removing {0} from the cemetary", target);
                    //SacredFlame def_5 [atk_1] --> 0
                    //CemetaryToDeck atk_1 [atk_1] --> 0
                    if (b.StoreReplay) b.AddOp(OpType.SacredFlame, sourceUUID, new List<string>() { target.Card.UUID }, 0);
                    if (b.StoreReplay) b.AddOp(OpType.CemetaryToDeck, target.Card.UUID, new List<string>() { target.Card.UUID }, 0);
                    opponent.Cemetary.Remove(target);
                }
            }
            else if (s.Type == SkillType.Teleportation)
            {
                Teleportation();
            }
            else if (s.Type == SkillType.WaitTimeIncrease)
            {
                IncreaseWaitTime();
            }
            else if (s.Type == SkillType.WaitTimeReduction)
            {
                ReduceWaitTime();
            }
            else if (s.Type == SkillType.Sacrifice)
            {
                if (you.Battlefield.Count > 1)
                {
                    var sac = you.Battlefield.All.FindAll(x => x != source).TakeRandom(1).First();
                    log.DebugFormat("Saccing: {0}", sac);
                    if (sac.HasImmunity)
                    {
                        log.Debug("NOT");
                    }
                    else
                    {
                        b.Sacrifice(sourceUUID, sac);
                        source.BonusSkillHP += source.CurrentHP * s.AffectValue / 100;
                        source.BonusSkillAttack += source.CurrentAttack * s.AffectValue / 100;
                    }
                }
            }
            else if (s.Type== SkillType.Burn)
            {
                Burn();
            }
            else if (s.Type == SkillType.Selfdestruct)
            {
                SelfDestruct();
            }
            else if (s.Type == SkillType.Silence)
            {
                Silence();
            }
            else if (s.Type == SkillType.HolyLight || s.Type == SkillType.Naturalize || s.Type == SkillType.Corruption || s.Type == SkillType.LavaTrial)
            {
                ChangeRace();
            }
            else if (s.Type == SkillType.SummonCards)
            {
                SummonCards();
            }
            else if (s.Type == SkillType.Purification)
            {
                Purify();
            }
            else if (s.Type == SkillType.Roar)
            {
                Roar();
            }
            else if (s.Type == SkillType.LastChance)
            {

            }
            else if (s.Type == SkillType.DeathMarker)
            {
                DeathMarker();
            }
            else 
            {
                log.WarnFormat("Unhandled skill {0} {1} {2} {3}", s.Name, s.LanchCondition, s.SkillCategory, s.Type);
            }
        }

        private void DeathMarker()
        {
            int myIndex = you.Battlefield.IndexOf(source);
            if (myIndex == -1)
            {
                log.ErrorFormat("Could not find {0} in battlefield", source);
                return;
            }

            SkillHit(opponent.Battlefield[myIndex]);
        }

        private void Roar()
        {
            int numTargets = s.AffectValue;
            /* Dread Roar has affect value 10, but has 'all cards' in its description. In case someone mentions that 
             it really affects all targets, uncomment these lines
            if (numTargets == 10)
            {
                numTargets = opponent.Battlefield.Count;
            }
            */

            foreach (var target in opponent.Battlefield.All.TakeRandom(numTargets))
            {
                SkillHit(target);
            }
        }

        private void Purify()
        {
            foreach (var card in you.Battlefield)
            {
                if (card.Trapped)
                {
                    card.Trapped = false;
                    if (b.StoreReplay) b.AddOp(OpType.Trapped, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
                if (card.Paralyzed)
                {
                    card.Paralyzed = false;
                    if (b.StoreReplay) b.AddOp(OpType.Paralyzed, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
                if (card.Frozen)
                {
                    card.Frozen = false;
                    if (b.StoreReplay) b.AddOp(OpType.Frozen, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
                if (card.Confused)
                {
                    card.Confused = false;
                    if (b.StoreReplay) b.AddOp(OpType.Confused, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
                if (card.Stunned)
                {
                    card.Stunned = false;
                    if (b.StoreReplay) b.AddOp(OpType.Stunned, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }

                if (card.Poisoned)
                {
                    card.Poisons.Clear();
                    if (b.StoreReplay) b.AddOp(OpType.Poisoned, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }

                if (card.Laceration)
                {
                    card.Laceration = false;
                    if (b.StoreReplay) b.AddOp(OpType.Lacerated, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }

                if (card.Burned)
                {
                    card.Burns.Clear();
                    if (b.StoreReplay) b.AddOp(OpType.Burned, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
                if (card.Roared)
                {
                    card.Roared = false;
                    if (b.StoreReplay) b.AddOp(OpType.Roared, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }

                if (card.DeathMark > 0)
                {
                    card.DeathMark = 0;
                    if (b.StoreReplay) b.AddOp(OpType.DeathMarked, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
            }
        }

        private void SummonCards()
        {
            SummonCard(s.AffectValue, 0);
            SummonCard(s.AffectValue2, 1);
        }

        private void SummonCard(int cardId, int summonId)
        {
            if (cardId == 0 || Card.Get(cardId) == null)
            {
                return;
            }
            string summonName = String.Format("{0}_{1}_{2}_{3}", source.Card.UUID, cardId, summonId, s.GetHashCode());

            BattleCard existing = you.Battlefield.All.Find(x => x.Card.UUID.Equals(summonName));

            if (existing == null)
            {
                UserCard c = new UserCard();
                c.CardId = cardId;
                c.Level = 10;
                c.UUID = summonName;

                BattleCard b = new BattleCard(c);
                b.IsSummoned = true;

                this.b.SummonCard(b, you);
                
            }
            else
            {
                log.DebugFormat("Summon #{0} {1} is already on the field", summonId, existing.Name);
            }
        }

        private void ChangeRace()
        {
            foreach (var card in opponent.Battlefield)
            {
                if (card.Race != Race.Demon && card.Race != Race.Hydra)
                {
                    log.DebugFormat("Setting {0}'s race to {1}", card.Name, (Race)s.AffectValue);
                    card.Race = (Race)s.AffectValue;
                }
                else
                {
                    log.DebugFormat("{0}'s race cannot be changed", card.Name);
                }
            }
        }

        private void Silence()
        {
            int myIndex = you.Battlefield.IndexOf(source);
            if (myIndex == -1)
            {
                log.ErrorFormat("Could not find {0} in battlefield", source);
                return;
            }
            SkillHit(opponent.Battlefield[myIndex]);
        }

        private void SelfDestruct()
        {
            int myIndex = you.Battlefield.IndexOf(source);
            if (myIndex == -1)
            {
                log.ErrorFormat("Could not find {0} in battlefield", source);
                return;
            }

            SkillHit(opponent.Battlefield[myIndex]);
            SkillHit(opponent.Battlefield[myIndex - 1]); // left
            SkillHit(opponent.Battlefield[myIndex + 1]); // right
        }

        private void Burn()
        {
            if (s.AffectType == 45)
            {
                // hmm, I have no idea who actually attacked me. I can guess, but it could be via Clean Sweep...
                //BurnTarget()
            }
            else
            {
                foreach (var target in opponent.Battlefield)
                {
                    BurnTarget(target);
                }
            }
        }

        private void BurnTarget(BattleCard target)
        {
            if (target.HasImmunity)
            {
                log.DebugFormat("{0} is immune!", target.Name);
                return;
            }
            string src = "";
            if (source == null)
            {
                src += "RUNE";
            }
            else
            {
                src += source.Name + source.GetHashCode();
            }
            src += "_" + originalSkill.Name;
            target.AddBurn(src, s.AffectValue);
            if (b.StoreReplay) b.AddOp(OpType.Burned, sourceUUID, new List<string>() { target.Card.UUID }, 1);
        }

        private void IncreaseWaitTime()
        {
            if (opponent.Hand.Count == 0) return;
            if (s.AffectType == 81)
            {
                // Impede - 
                var target = opponent.Hand.OrderBy(x => x.CurrentWaitTime).First();
                log.DebugFormat("Increasing wait time for {0} by {1}", target, s.AffectValue);
                target.CurrentWaitTime += s.AffectValue;
            }
            else if (s.AffectType == 93)
            {
                // Mass Attrition
                foreach (var target in opponent.Hand)
                {
                    log.DebugFormat("Increasing wait time for {0} by {1}", target, s.AffectValue);
                    target.CurrentWaitTime += s.AffectValue;
                }
            }
            else
            {
                log.WarnFormat("Unknown AffectType {0} for skill {1} (id: {2})", s.AffectType, s.Name, s.SkillId);
            }
        }

        private void ReduceWaitTime()
        {
            if (you.Hand.Count == 0) return;
            if (s.AffectType == 80)
            {
                // Advanced Strike
                var target = you.Hand.OrderByDescending(x => x.CurrentWaitTime).First();
                log.DebugFormat("Reducing wait time for {0} by {1}", target, s.AffectValue);
                target.CurrentWaitTime += s.AffectValue;
            }
            else if (s.AffectType == 92)
            {
                // Warcry
                foreach (var target in you.Hand)
                {
                    log.DebugFormat("Reducing wait time for {0} by {1}", target, s.AffectValue);
                    target.CurrentWaitTime += s.AffectValue;
                }
            }
            else
            {
                log.WarnFormat("Unknown AffectType {0} for skill {1} (id: {2})", s.AffectType, s.Name, s.SkillId);
            }
        }


        //private void Exile(int i, BattleCard defendingCard)
        //{
        //    if (defendingCard != null)
        //    {
        //        log.DebugFormat("Exiling {0}", defendingCard);
        //        if (defendingCard.HasImmunity || defendingCard.HasResistance)
        //        {
        //            log.DebugFormat("NOT");
        //        }
        //        else
        //        {
        //            defendingCard.OnDeath -= b_OnDeath;
        //            CleanupBuffs(defendingCard, false);
        //            TargetPlayer.Battlefield.Remove(defendingCard);
        //            TargetPlayer.Deck.Add(defendingCard);
        //        }
        //    }
        //}

        private void Teleportation()
        {
            //Casts the card with the longest waiting time in the opponent's hand directly into their cemetery.
            if (opponent.Hand.Count > 0)
            {
                var target = opponent.Hand.OrderByDescending(x => x.CurrentWaitTime).First();
                if (b.StoreReplay) b.AddOp(OpType.Teleportation, sourceUUID, new List<string>() { target.Card.UUID }, 0);
                log.DebugFormat("Teleportating {0} from hand to the cemetary", target);
                if (target.HasImmunity)
                {
                    // Immune atk_4 [atk_4] --> 0
                    log.DebugFormat("Immune");
                    if (b.StoreReplay) b.AddOp(OpType.Immune, target.Card.UUID, new List<string>() { target.Card.UUID }, 0);
                }
                else if (target.HasResistance)
                {
                    // Resistance def_5 [def_5] --> 0
                    log.DebugFormat("Resistance");
                    if (b.StoreReplay) b.AddOp(OpType.Resistance, target.Card.UUID, new List<string>() { target.Card.UUID }, 0);
                }
                else
                {
                    // CardDies atk_6 [] --> 0
                    if (b.StoreReplay) b.AddOp(OpType.CardDies, target.Card.UUID, new List<string>() { }, 0);
                    opponent.Hand.Remove(target);
                    opponent.Cemetary.Add(target);
                }
            }
        }

        private void UseRuneSkill()
        {
            if (s.AffectType == 60)
            {
                // Add the skill in s.AffectValue to every card you have.
                Skill applySkill = Skill.Get(s.AffectValue);

                foreach (var card in you.Battlefield)
                {
                    card.AddRuneSkill(applySkill);
                }
            }
            else if (s.AffectType == 61)
            {
                // Shield: Allows one card on your side to ward off X damage in the following round.
                //log.WarnFormat("Shield not implemented");
                if (you.Battlefield.Count > 0)
                {
                    you.Battlefield.All.TakeRandom(1).First().AddRuneSkill(s);
                }
            }
            else if (s.AffectType == 62)
            {
                // Barricade: Allows all cards on your side to ward off X damage in the following round.
                //log.WarnFormat("Barricade not implemented");
                foreach (var card in you.Battlefield)
                {
                    card.AddRuneSkill(s);
                }
            }
            else if (s.AffectType == 63)
            {
                // Inspiration: Increases the Attack of one card on your battlefield by X.
                // Unused I believe...
                log.WarnFormat("Inspiration not implemented");
            }
            else if (s.AffectType == 64)
            {
                // Group Morale: Increases the Attack of ALL cards on your battlefield by X.
                //log.WarnFormat("Group Morale not implemented");
                foreach (var card in you.Battlefield)
                {
                    card.AddRuneSkill(s);
                }
            }
        }


        private void Heal()
        {
            log.DebugFormat("Handling heal {0}", s.Name);
            if (source != null)
            {
                log.DebugFormat("Source: {0}", source.Name);
            }
            else
            {
                log.DebugFormat("No source");
            }

            IEnumerable<BattleCard> targets = null;
            if (s.AffectType == 20)
            {
                // Healing
                targets = you.Battlefield.OrderBy(x => x.CurrentHP).Take(1);
            }
            else if (s.AffectType == 21)
            {
                // Regeneration
                targets = you.Battlefield;
            }
            else if (s.AffectType == 83)
            {
                // healing mists
                List<BattleCard> tmp = new List<BattleCard>(3);
                tmp.Add(source);

                int i = you.Battlefield.IndexOf(source);
                if (i > 1)
                {
                    tmp.Add(you.Battlefield[i - 1]);
                }

                if (i < you.Battlefield.Count - 1)
                {
                    tmp.Add(you.Battlefield[i + 1]);
                }
                targets = tmp;
            }

            if (targets != null)
            {
                foreach (var target in targets)
                {
                    if (target != null && !target.Laceration && !target.HasImmunity)
                    {
                        HealCard(target, s.AffectValue);
                    }
                }
            }
        }
        
        private void PercentageHeal()
        {
            log.DebugFormat("Handling heal {0}", s.Name);
            if (source != null)
            {
                log.DebugFormat("Source: {0}", source.Name);
            }
            else
            {
                log.DebugFormat("No source");
            }

            foreach (var target in you.Battlefield)
            {
                if (target != null && !target.Laceration && !target.HasImmunity)
                {
                    int healingAmount = (target.BaseHP + target.BonusSkillHP) * s.AffectValue / 100;
                    HealCard(target, healingAmount);
                }
            }
        }
        
        private void Trap()
        {
            if (s.AffectType == 48 && source.FirstAttack == false)
            {
                // Seal only 1 time
                return;
            }

            int numTargets = s.AffectValue;
            if (numTargets == 0)
            {
                numTargets = opponent.Battlefield.Count;
            }

            foreach (var target in opponent.Battlefield.All.TakeRandom(numTargets))
            {
                if (target == null)
                {
                    // Don't think this can happen anymore now that I am looking at Battlefield instead of defenders, keeping logging anyway.
                    log.ErrorFormat("I am skipping a trap target, but not looking for a new one.");
                    continue;
                }
                SkillHit(target);
            }
        }

        private void FindDefenderTarget()
        {
            if (opponent.Battlefield.Count == 1)
            {
                SkillHit(opponent.Battlefield.All.First());
                return;
            }

            int numTargets = 1;
            if (s.AffectType == 7 // Chain Lightning
                || s.AffectType == 10 // Nova Frost
                || s.AffectType == 13 // Fire Wall
                || s.AffectType == 17 // Smog
                || s.AffectType == 114 // Mana Burn
                )
            {
                numTargets = 3;
            }
            else if (s.AffectType == 8 // Electric Shock
                || s.AffectType == 11 // Blizzard
                || s.AffectType == 14 // Firestorm
                || s.AffectType == 18 // Toxic Clouds
                || s.AffectType == 29 // Group Weaken
                || s.AffectType == 38 // Plague
                || s.AffectType == 85 // Feast of Blood
                || s.AffectType == 96 // Frost Shock
                || s.AffectType == 121 // Asura's Flame
                )
            {
                numTargets = opponent.Battlefield.Count;
            }
            else if (s.AffectType == 69) // Confusion / Field of Chaos )
            {
                if (s.AffectValue2 == 0) // Confusion
                {
                    numTargets = 1;
                }
                else // Field of Chaos
                {
                    numTargets = s.AffectValue2;
                }
            }

            List<BattleCard> targets;

            if (numTargets < opponent.Battlefield.Count)
            {
                targets = opponent.Battlefield.TakeRandom(numTargets).ToList();
            }
            else
            {
                targets = opponent.Battlefield.All.ToList();
                numTargets = targets.Count;
            }

            for (int i = 0; i < numTargets; i++)
            {
                if (targets[i] == null) continue;
                log.DebugFormat("Selected target {0}", targets[i].Card.UUID);
                SkillHit(targets[i]);
            }
        }


        private void SkillHit(BattleCard target)
        {
            if (target == null)
            {
                // Invalid target
                return;
            }
            log.DebugFormat("{0} is getting a {1} thrown by {2}", target.Name, s.Name, source);

            if (s.Type == SkillType.Stun || s.Type == SkillType.Freeze || s.Type == SkillType.DamageAndHeal || s.Type == SkillType.Damage || s.Type == SkillType.FrostShock || s.Type == SkillType.Asura)
            {
                if (target.HasReflection)
                {
                    log.DebugFormat("{0} has reflection. OUCH {1} fails!", target, s);
                    if (source != null)
                    {
                        log.DebugFormat("{0} damage dealt", target.ReflectDamage);
                        DamageCard(source, target.ReflectDamage);
                    }
                    return;
                }
            }

            if (target.HasImmunity)
            {
                if (s.Type == SkillType.Roar)
                {
                    // Roar skips immunity, but doesn't work against demons
                    if (target.Race == Race.Demon || target.Race == Race.Hydra)
                    {
                        log.DebugFormat("Roar - Demon / Hydra immunity!");
                        return;
                    }
                }
                else
                if (s.Type != SkillType.ManaCorruption && s.Type != SkillType.Silence)
                {
                    // pretty much everything is ignored if I have immunity.
                    log.DebugFormat("Immune!");
                    return;
                }
            }

            if (target.LastChanceActive)
            {
                if (s.Type != SkillType.Destroy)
                {
                    log.Debug("Ignored - last chance is active");
                    return;
                }
            }

            if (s.Type == SkillType.Destroy)
            {
                if (!(target.HasImmunity || target.HasResistance))
                {
                    b.Destroy(sourceUUID, target);
                }
                else
                {
                    log.DebugFormat("Resist / immune");
                }
            }
            else if (s.Type == SkillType.Poison)
            {
                DamageCard(target, s.AffectValue);
                if (b.StoreReplay) b.AddOp(OpType.Poisoned, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                target.Poisons.Add(s.AffectValue);
            }
            else if (s.Type == SkillType.Stun)
            {
                int damage = s.AffectValue;
                if (target.HasMagicShield)
                {
                    log.DebugFormat("Magically shielding against Lightning");
                    damage = Math.Min(target.MagicShield, s.AffectValue);
                }

                DamageCard(target, damage);

                if (!target.HasEvasion)
                {
                    if (Util.PercentChance(s.AffectValue2))
                    {
                        log.DebugFormat("Stunned!");
                        target.Paralyzed = true;
                        if (b.StoreReplay) b.AddOp(OpType.Paralyzed, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                    }
                }
            }
            else if (s.Type == SkillType.Freeze)
            {
                int damage = s.AffectValue;
                if (target.HasMagicShield)
                {
                    log.DebugFormat("Magically shielding against Ice");
                    damage = Math.Min(target.MagicShield, s.AffectValue);
                }

                DamageCard(target, damage);

                if (!target.HasEvasion)
                {
                    if (Util.PercentChance(s.AffectValue2))
                    {
                        log.DebugFormat("Frozen!");
                        target.Frozen = true;
                        if (b.StoreReplay) b.AddOp(OpType.Frozen, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                    }
                }
            }
            else if (s.Type == SkillType.FrostShock)
            {
                int damage = s.AffectValue + opponent.Battlefield.Count * s.AffectValue2;
                if (target.HasMagicShield)
                {
                    log.DebugFormat("Magically shielding against Ice");
                    damage = Math.Min(target.MagicShield, damage);
                }
                DamageCard(target, damage);

                if (!target.HasEvasion)
                {
                    if (Util.PercentChance(50))
                    {
                        log.DebugFormat("Frozen!");
                        target.Frozen = true;
                        if (b.StoreReplay) b.AddOp(OpType.Frozen, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                    }
                }
            }
            else if (s.Type == SkillType.Damage)
            {
                int damage = ThreadSafeRandom.ThisThreadsRandom.Next(s.AffectValue, 1 + s.AffectValue * 2);
                log.DebugFormat("Fire damage {0}-{1} => {2}", s.AffectValue, s.AffectValue * 2, damage);

                if (target.HasMagicShield)
                {
                    log.DebugFormat("Magically shielding against Fire");
                    damage = Math.Min(target.MagicShield, damage);
                }
                DamageCard(target, damage);
            }
            else if (s.Type == SkillType.DamageAndHeal)
            {
                int damage = Math.Min(target.CurrentHP, s.AffectValue);
                if (target.HasMagicShield)
                {
                    log.DebugFormat("Magically shielding against Blood");
                    damage = Math.Min(target.MagicShield, damage);
                }
                log.DebugFormat("Biting for {0}", damage);
                DamageCard(target, damage);
                HealCard(source, damage);
            }
            else if (s.Type == SkillType.Trap)
            {
                log.DebugFormat("Trapping / sealing");
                if (!target.HasEvasion)
                {
                    if (s.AffectType == 48 || Util.PercentChance(65))
                    {
                        target.Trapped = true;
                        if (b.StoreReplay) b.AddOp(OpType.Trapped, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                        log.Debug("Success");
                    }
                    else
                    {
                        log.Debug("Failed");
                    }
                }
            }
            else if (s.Type == SkillType.ManaCorruption)
            {
                log.DebugFormat("Mana Corruption");
                int damage = s.AffectValue;
                if (target.HasImmunity || target.HasReflection)
                {
                    log.DebugFormat("Target has immunity or reflection. 300% damage.");
                    damage *= 3;
                }
                DamageCard(target, damage);
            }
            else if (s.Type == SkillType.AttackDecrease)
            {
                if (s.AffectValue / 10 > 11)
                {
                    // Something went wrong
                    throw new ArgumentException("Trying to execute Quick Strike or Desperation: Group Weaken as a normal skill");
                }
                target.CurrentAttack -= s.AffectValue;
            }
            else if (s.Type == SkillType.AttackAndHealthDecrease)
            {
                DamageCard(target, s.AffectValue);
                target.CurrentAttack -= s.AffectValue;
            }
            else if (s.Type == SkillType.Confusion)
            {
                if (!target.HasEvasion)
                {
                    log.DebugFormat("Confusing...{0}% chance", s.AffectValue);
                    if (Util.PercentChance(s.AffectValue))
                    {
                        target.Confused = true;
                        if (b.StoreReplay) b.AddOp(OpType.Confused, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                        log.Debug("Success");
                    }
                    else
                    {
                        log.Debug("Failed");
                    }
                }
            }
            else if (s.Type == SkillType.Selfdestruct)
            {
                DamageCard(target, s.AffectValue);
            }
            else if (s.Type == SkillType.Silence)
            {
                target.Silence();
            }
            else if (s.Type == SkillType.Roar)
            {
                log.Debug("Roared");
                target.Roared = true;
                if (b.StoreReplay) b.AddOp(OpType.Roared, sourceUUID, new List<string>() { target.Card.UUID }, 1);
            }
            else if (s.Type == SkillType.Asura)
            {
                int damage = s.AffectValue + opponent.Battlefield.Count * s.AffectValue2;

                log.DebugFormat("Hitting for {0} damage", damage);

                DamageCard(target, damage);
            }
            else if (s.Type == SkillType.DeathMarker)
            {
                log.Debug("Marked for death");
                if (s.AffectValue > 1)
                {
                    log.WarnFormat("Death Marker with more then 1 turn is not supported yet.");
                }
                target.DeathMark += s.AffectValue2;
                if (b.StoreReplay) b.AddOp(OpType.DeathMarked, sourceUUID, new List<string>() { target.Card.UUID }, 1);
            }
            else
            {
                log.WarnFormat("Unhandled skill hit: {0}", s);
            }
        }

        private void DamageCard(BattleCard target, int damage)
        {
            b.DamageCard(sourceUUID, target, damage);
        }

        private void HealCard(BattleCard target, int amount)
        {
            b.HealCard(sourceUUID, target, amount);
        }
    }
}
