﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.util;
using ElementalKingdomsGui.Properties;

namespace ElementalKingdoms.ui
{
    public partial class UserInfo : UserControl
    {
        private User user;
        public UserInfo()
        {
            InitializeComponent();
        }

        public void SetUser(User u)
        {
            user = u;

            lblName.Text = u.NickName;
            lblLevel.Text = u.Level.ToString();
            lblGems.Text = u.Gems.ToString();
            lblGold.Text = u.Gold.ToString();
            progressEnergy.Maximum = Math.Max(u.Energy, u.EnergyMax);
            progressEnergy.Value = u.Energy;
            lblEnergy.Text = u.Energy.ToString();

            if (u.Avatar == 0)
            {
                if (u.Sex == Gender.Female)
                {
                    pbAvatar.Image = Resources.female;
                }
                else
                {
                    pbAvatar.Image = Resources.male;
                }
            }
            else
            {
                pbAvatar.Image = ImageLoader.GetSmallImageByName(Card.Cards.Find(x => x.Id == u.Avatar).Name);
            }
        }
    }
}
