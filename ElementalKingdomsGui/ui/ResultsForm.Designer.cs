﻿namespace ElementalKingdoms.ui
{
    partial class ResultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultsForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Cooldown = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MPF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MPM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PercentHittingRound50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PercentHittingRound100 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnReload = new System.Windows.Forms.Button();
            this.attackerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.defenderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iterationsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minMeritDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxMeritDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumMeritDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minRoundsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxRoundsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumRoundsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roundsWonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.battleResultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.battleResultBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnReload);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(873, 100);
            this.panel1.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.attackerDataGridViewTextBoxColumn,
            this.defenderDataGridViewTextBoxColumn,
            this.iterationsDataGridViewTextBoxColumn,
            this.Cooldown,
            this.MPF,
            this.MPM,
            this.minMeritDataGridViewTextBoxColumn,
            this.maxMeritDataGridViewTextBoxColumn,
            this.sumMeritDataGridViewTextBoxColumn,
            this.minRoundsDataGridViewTextBoxColumn,
            this.maxRoundsDataGridViewTextBoxColumn,
            this.sumRoundsDataGridViewTextBoxColumn,
            this.PercentHittingRound50,
            this.PercentHittingRound100,
            this.roundsWonDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.battleResultBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 100);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(873, 343);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView1_RowsRemoved);
            // 
            // Cooldown
            // 
            this.Cooldown.DataPropertyName = "Cooldown";
            this.Cooldown.HeaderText = "Cooldown";
            this.Cooldown.Name = "Cooldown";
            this.Cooldown.ReadOnly = true;
            // 
            // MPF
            // 
            this.MPF.DataPropertyName = "MPF";
            this.MPF.HeaderText = "MPF";
            this.MPF.Name = "MPF";
            this.MPF.ReadOnly = true;
            // 
            // MPM
            // 
            this.MPM.DataPropertyName = "MPM";
            this.MPM.HeaderText = "MPM";
            this.MPM.Name = "MPM";
            this.MPM.ReadOnly = true;
            // 
            // PercentHittingRound50
            // 
            this.PercentHittingRound50.DataPropertyName = "PercentHittingRound50";
            this.PercentHittingRound50.HeaderText = "PercentHittingRound50";
            this.PercentHittingRound50.Name = "PercentHittingRound50";
            this.PercentHittingRound50.ReadOnly = true;
            // 
            // PercentHittingRound100
            // 
            this.PercentHittingRound100.DataPropertyName = "PercentHittingRound100";
            this.PercentHittingRound100.HeaderText = "PercentHittingRound100";
            this.PercentHittingRound100.Name = "PercentHittingRound100";
            this.PercentHittingRound100.ReadOnly = true;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(12, 12);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(75, 23);
            this.btnReload.TabIndex = 0;
            this.btnReload.Text = "Refresh";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // attackerDataGridViewTextBoxColumn
            // 
            this.attackerDataGridViewTextBoxColumn.DataPropertyName = "Attacker";
            this.attackerDataGridViewTextBoxColumn.FillWeight = 50F;
            this.attackerDataGridViewTextBoxColumn.HeaderText = "Attacker";
            this.attackerDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.attackerDataGridViewTextBoxColumn.Name = "attackerDataGridViewTextBoxColumn";
            this.attackerDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // defenderDataGridViewTextBoxColumn
            // 
            this.defenderDataGridViewTextBoxColumn.DataPropertyName = "Defender";
            this.defenderDataGridViewTextBoxColumn.DividerWidth = 1;
            this.defenderDataGridViewTextBoxColumn.FillWeight = 50F;
            this.defenderDataGridViewTextBoxColumn.HeaderText = "Defender";
            this.defenderDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.defenderDataGridViewTextBoxColumn.Name = "defenderDataGridViewTextBoxColumn";
            this.defenderDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iterationsDataGridViewTextBoxColumn
            // 
            this.iterationsDataGridViewTextBoxColumn.DataPropertyName = "Iterations";
            this.iterationsDataGridViewTextBoxColumn.HeaderText = "Iterations";
            this.iterationsDataGridViewTextBoxColumn.Name = "iterationsDataGridViewTextBoxColumn";
            this.iterationsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // minMeritDataGridViewTextBoxColumn
            // 
            this.minMeritDataGridViewTextBoxColumn.DataPropertyName = "MinMerit";
            this.minMeritDataGridViewTextBoxColumn.HeaderText = "MinMerit";
            this.minMeritDataGridViewTextBoxColumn.Name = "minMeritDataGridViewTextBoxColumn";
            this.minMeritDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maxMeritDataGridViewTextBoxColumn
            // 
            this.maxMeritDataGridViewTextBoxColumn.DataPropertyName = "MaxMerit";
            this.maxMeritDataGridViewTextBoxColumn.HeaderText = "MaxMerit";
            this.maxMeritDataGridViewTextBoxColumn.Name = "maxMeritDataGridViewTextBoxColumn";
            this.maxMeritDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sumMeritDataGridViewTextBoxColumn
            // 
            this.sumMeritDataGridViewTextBoxColumn.DataPropertyName = "SumMerit";
            this.sumMeritDataGridViewTextBoxColumn.HeaderText = "SumMerit";
            this.sumMeritDataGridViewTextBoxColumn.Name = "sumMeritDataGridViewTextBoxColumn";
            this.sumMeritDataGridViewTextBoxColumn.ReadOnly = true;
            this.sumMeritDataGridViewTextBoxColumn.Visible = false;
            // 
            // minRoundsDataGridViewTextBoxColumn
            // 
            this.minRoundsDataGridViewTextBoxColumn.DataPropertyName = "MinRounds";
            this.minRoundsDataGridViewTextBoxColumn.HeaderText = "MinRounds";
            this.minRoundsDataGridViewTextBoxColumn.Name = "minRoundsDataGridViewTextBoxColumn";
            this.minRoundsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maxRoundsDataGridViewTextBoxColumn
            // 
            this.maxRoundsDataGridViewTextBoxColumn.DataPropertyName = "MaxRounds";
            this.maxRoundsDataGridViewTextBoxColumn.HeaderText = "MaxRounds";
            this.maxRoundsDataGridViewTextBoxColumn.Name = "maxRoundsDataGridViewTextBoxColumn";
            this.maxRoundsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sumRoundsDataGridViewTextBoxColumn
            // 
            this.sumRoundsDataGridViewTextBoxColumn.DataPropertyName = "SumRounds";
            this.sumRoundsDataGridViewTextBoxColumn.HeaderText = "SumRounds";
            this.sumRoundsDataGridViewTextBoxColumn.Name = "sumRoundsDataGridViewTextBoxColumn";
            this.sumRoundsDataGridViewTextBoxColumn.ReadOnly = true;
            this.sumRoundsDataGridViewTextBoxColumn.Visible = false;
            // 
            // roundsWonDataGridViewTextBoxColumn
            // 
            this.roundsWonDataGridViewTextBoxColumn.DataPropertyName = "RoundsWon";
            this.roundsWonDataGridViewTextBoxColumn.HeaderText = "RoundsWon";
            this.roundsWonDataGridViewTextBoxColumn.Name = "roundsWonDataGridViewTextBoxColumn";
            this.roundsWonDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // battleResultBindingSource
            // 
            this.battleResultBindingSource.DataSource = typeof(ElementalKingdoms.tools.BattleResult);
            // 
            // ResultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 443);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ResultsForm";
            this.Text = "Battle results";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ResultsForm_FormClosing);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.battleResultBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource battleResultBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn attackerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn defenderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iterationsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cooldown;
        private System.Windows.Forms.DataGridViewTextBoxColumn MPF;
        private System.Windows.Forms.DataGridViewTextBoxColumn MPM;
        private System.Windows.Forms.DataGridViewTextBoxColumn minMeritDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxMeritDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumMeritDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minRoundsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxRoundsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumRoundsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PercentHittingRound50;
        private System.Windows.Forms.DataGridViewTextBoxColumn PercentHittingRound100;
        private System.Windows.Forms.DataGridViewTextBoxColumn roundsWonDataGridViewTextBoxColumn;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnReload;
    }
}