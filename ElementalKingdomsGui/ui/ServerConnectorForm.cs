﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.server;

namespace ElementalKingdoms.ui
{
    public partial class ServerConnectorForm : Form
    {
        private ServerLoginForm loginForm;

        private List<Action> serverActions;
        private ServerCommunicator com;

        public User RegisteredUser;

        public ServerConnectorForm()
        {
            InitializeComponent();

            loginForm = new ServerLoginForm();

            com = ServerCommunicator.Create(ElementalKingdomsGui.Properties.Settings.Default.DeviceId, false);

            comboBox1.DisplayMember = "Name";
            serverActions = new List<Action>();
            serverActions.Add(() => this.btnCards_Click(null, null));
            serverActions.Add(() => this.btnSkills_Click(null, null));
            serverActions.Add(() => this.btnRunes_Click(null, null));
            serverActions.Add(() => this.btnMaps_Click(null, null));
            serverActions.Add(() => this.btnLeague_Click(null, null));
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            btnConnect.Enabled = false;
            if (loginForm.ShowDialog() == DialogResult.OK)
            {
                bcConnectToServer.RunWorkerAsync();
            }
            btnConnect.Enabled = true;
        }

        private void bcConnectToServer_DoWork(object sender, DoWorkEventArgs e)
        {
            bcConnectToServer.ReportProgress(10, "Connecting to server");

            com.GetCardGroups();
            com.GetUserRunes();
            com.GetUserCards();
            com.GetUserMapStages();

            User u = User.Load();

            if (!loginForm.IsGuestUser)
            {
                RegisteredUser = u;
            }

            bcConnectToServer.ReportProgress(100, String.Format("Connected | {0}@{1}", u.NickName, com.Server.Name));
            foreach (var action in serverActions)
            {
                action();
            }
        }

        private void bcConnectToServer_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblStatus.Text = e.UserState.ToString();
        }

        private void bcConnectToServer_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            comboBox1.DataSource = com.Servers;
            comboBox1.SelectedItem = com.Server;
            comboBox1.SelectedIndexChanged += new System.EventHandler(comboBox1_SelectedIndexChanged);
            comboBox1.Enabled = true;
            btnCards.Enabled = true;
            btnLeague.Enabled = true;
            btnRunes.Enabled = true;
            btnMaps.Enabled = true;
            btnSkills.Enabled = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == null) return;

            if (com.Server != comboBox1.SelectedItem)
            {
                ServerInfo si = comboBox1.SelectedItem as ServerInfo;
                com.ConnectToServer(si);
                User u = User.Load();
                lblStatus.Text = "Connected | " + u.NickName + "@" + com.Server.Name;
                ElementalKingdomsGui.Properties.Settings.Default.selectedServerId = si.Id;
                ElementalKingdomsGui.Properties.Settings.Default.Save();
            }
        }

        private void btnLeague_Click(object sender, EventArgs e)
        {
            pbLeagueCheck.SafeInvoke(() => pbLeagueCheck.Visible = false, false);
            com.GetLeagueInfo();
            lblMessage.Text = "Retrieved league data!";
            pbLeagueCheck.SafeInvoke(() => pbLeagueCheck.Visible = true, false);
        }

        private void btnCards_Click(object sender, EventArgs e)
        {
            pbCardsCheck.SafeInvoke(() => pbCardsCheck.Visible = false, false);
            com.GetAllCard();
            lblMessage.Text = "Retrieved card data!";
            pbCardsCheck.SafeInvoke(() => pbCardsCheck.Visible = true, false);
        }

        private void btnSkills_Click(object sender, EventArgs e)
        {
            pbSkillsCheck.SafeInvoke(() => pbSkillsCheck.Visible = false, false);
            com.GetAllSkill();
            lblMessage.Text = "Retrieved skill data!";
            pbSkillsCheck.SafeInvoke(() => pbSkillsCheck.Visible = true, false);
        }

        private void btnRunes_Click(object sender, EventArgs e)
        {
            pbRunesCheck.SafeInvoke(() => pbRunesCheck.Visible = false, false);
            com.GetAllRunes();
            lblMessage.Text = "Retrieved rune data!";
            pbRunesCheck.SafeInvoke(() => pbRunesCheck.Visible = true, false);
        }

        private void btnMaps_Click(object sender, EventArgs e)
        {
            pbMapsCheck.SafeInvoke(() => pbMapsCheck.Visible = false, false);
            com.GetMapStages();
            lblMessage.Text = "Retrieved map data!";
            pbMapsCheck.SafeInvoke(() => pbMapsCheck.Visible = true, false);
        }
    }
}
