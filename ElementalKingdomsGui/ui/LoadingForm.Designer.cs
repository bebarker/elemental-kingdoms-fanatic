﻿namespace ElementalKingdomsGui.ui
{
    partial class LoadingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadingForm));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tbChangeLog = new System.Windows.Forms.TextBox();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnSkipContinue = new System.Windows.Forms.Button();
            this.borderLabel4 = new cSouza.WinForms.Controls.BorderLabel();
            this.borderLabel3 = new cSouza.WinForms.Controls.BorderLabel();
            this.borderLabel2 = new cSouza.WinForms.Controls.BorderLabel();
            this.borderLabel1 = new cSouza.WinForms.Controls.BorderLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 615);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(936, 13);
            this.progressBar1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::ElementalKingdomsGui.Properties.Resources.background_1436100370;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(960, 640);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tbChangeLog
            // 
            this.tbChangeLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbChangeLog.Location = new System.Drawing.Point(232, 173);
            this.tbChangeLog.Multiline = true;
            this.tbChangeLog.Name = "tbChangeLog";
            this.tbChangeLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbChangeLog.Size = new System.Drawing.Size(514, 327);
            this.tbChangeLog.TabIndex = 6;
            // 
            // btnDownload
            // 
            this.btnDownload.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownload.Location = new System.Drawing.Point(317, 506);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(161, 66);
            this.btnDownload.TabIndex = 8;
            this.btnDownload.Text = "Download!";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnSkipContinue
            // 
            this.btnSkipContinue.Location = new System.Drawing.Point(487, 506);
            this.btnSkipContinue.Name = "btnSkipContinue";
            this.btnSkipContinue.Size = new System.Drawing.Size(161, 66);
            this.btnSkipContinue.TabIndex = 9;
            this.btnSkipContinue.Text = "Continue";
            this.btnSkipContinue.UseVisualStyleBackColor = true;
            this.btnSkipContinue.Click += new System.EventHandler(this.btnSkipContinue_Click);
            // 
            // borderLabel4
            // 
            this.borderLabel4.BackColor = System.Drawing.Color.Transparent;
            this.borderLabel4.BorderColor = System.Drawing.Color.Navy;
            this.borderLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.borderLabel4.ForeColor = System.Drawing.Color.Magenta;
            this.borderLabel4.Location = new System.Drawing.Point(12, 123);
            this.borderLabel4.Name = "borderLabel4";
            this.borderLabel4.Size = new System.Drawing.Size(936, 47);
            this.borderLabel4.TabIndex = 7;
            this.borderLabel4.Text = "Update available";
            this.borderLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // borderLabel3
            // 
            this.borderLabel3.BackColor = System.Drawing.Color.Transparent;
            this.borderLabel3.BorderColor = System.Drawing.Color.Navy;
            this.borderLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.borderLabel3.ForeColor = System.Drawing.Color.Magenta;
            this.borderLabel3.Location = new System.Drawing.Point(797, 528);
            this.borderLabel3.Name = "borderLabel3";
            this.borderLabel3.Size = new System.Drawing.Size(151, 47);
            this.borderLabel3.TabIndex = 5;
            this.borderLabel3.Text = "0.0.9.0";
            this.borderLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // borderLabel2
            // 
            this.borderLabel2.BackColor = System.Drawing.Color.Transparent;
            this.borderLabel2.BorderColor = System.Drawing.Color.Navy;
            this.borderLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.borderLabel2.ForeColor = System.Drawing.Color.Magenta;
            this.borderLabel2.Location = new System.Drawing.Point(7, 9);
            this.borderLabel2.Name = "borderLabel2";
            this.borderLabel2.Size = new System.Drawing.Size(936, 47);
            this.borderLabel2.TabIndex = 4;
            this.borderLabel2.Text = "Elemental Kingdoms Fanatic";
            this.borderLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // borderLabel1
            // 
            this.borderLabel1.BackColor = System.Drawing.Color.Transparent;
            this.borderLabel1.BorderColor = System.Drawing.Color.Black;
            this.borderLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.borderLabel1.ForeColor = System.Drawing.Color.White;
            this.borderLabel1.Location = new System.Drawing.Point(12, 575);
            this.borderLabel1.Name = "borderLabel1";
            this.borderLabel1.Size = new System.Drawing.Size(936, 34);
            this.borderLabel1.TabIndex = 3;
            this.borderLabel1.Text = "Progress";
            this.borderLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LoadingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 640);
            this.Controls.Add(this.btnSkipContinue);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.borderLabel4);
            this.Controls.Add(this.tbChangeLog);
            this.Controls.Add(this.borderLabel3);
            this.Controls.Add(this.borderLabel2);
            this.Controls.Add(this.borderLabel1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoadingForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoadingForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoadingForm_FormClosing);
            this.Shown += new System.EventHandler(this.LoadingForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private cSouza.WinForms.Controls.BorderLabel borderLabel1;
        private cSouza.WinForms.Controls.BorderLabel borderLabel2;
        private cSouza.WinForms.Controls.BorderLabel borderLabel3;
        private System.Windows.Forms.TextBox tbChangeLog;
        private cSouza.WinForms.Controls.BorderLabel borderLabel4;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button btnSkipContinue;
    }
}