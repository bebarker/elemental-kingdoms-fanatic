﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class BattleRune
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private UserRune r;

        [Newtonsoft.Json.JsonIgnore]
        public bool Active { get; private set; }
        [Newtonsoft.Json.JsonIgnore]
        public int NumActivationsLeft { get; private set; }

        [Newtonsoft.Json.JsonIgnore]
        public int RuneId { get { return r.RuneId; } }

        public BattleRune(UserRune r)
        {
            // TODO: Complete member initialization
            this.r = r;
            Reset();
        }

        public void Reset()
        {
            Active = false;
            NumActivationsLeft = r.BaseRune.SkillTimes;
        }

        /// <summary>
        /// Checks if the rune may be activated, and returns the skill that should be executed.
        /// </summary>
        /// <param name="you"></param>
        /// <param name="opponent"></param>
        /// <param name="round"></param>
        /// <returns></returns>
        public Skill Activate(BattleUser you, BattleUser opponent, int round)
        {
            if (NumActivationsLeft == 0)
            {
                Active = false;
                return null;
            }

            BattleUser checkPlayer = r.BaseRune.SkillConditionSlide == 1 ? you : opponent;

            bool activate = false;
            switch (r.BaseRune.SkillConditionType)
            {
                case 1:
                    // nr of cards (of type X) in your/opponent hand
                    activate = CheckNumberOfCardsInZone(checkPlayer.Hand, r.BaseRune.SkillConditionValue, r.BaseRune.SkillConditionCompare, r.BaseRune.SkillConditionRace);
                    break;
                case 2:
                    // nr of cards of type X in your/opponent cemetary
                    activate = CheckNumberOfCardsInZone(checkPlayer.Cemetary, r.BaseRune.SkillConditionValue, r.BaseRune.SkillConditionCompare, r.BaseRune.SkillConditionRace);
                    break;
                case 3:
                    // nr of cards in your deck
                    activate = CheckNumberOfCardsInZone(checkPlayer.Deck, r.BaseRune.SkillConditionValue, r.BaseRune.SkillConditionCompare, r.BaseRune.SkillConditionRace);
                    break;
                case 4:
                    // nr of cards of type X on your/opponent battlefield
                    activate = CheckNumberOfCardsInZone(checkPlayer.Battlefield.All, r.BaseRune.SkillConditionValue, r.BaseRune.SkillConditionCompare, r.BaseRune.SkillConditionRace);
                    break;
                case 5:
                    // exceeded X rounds
                    if (round > r.BaseRune.SkillConditionValue)
                    {
                        log.DebugFormat("Activating rune {0}", r.BaseRune.Name);
                        activate = true;
                    }
                    break;
                case 6:
                    // your hp %
                    if (r.BaseRune.SkillConditionValue > (checkPlayer.CurrentHP * 100 / checkPlayer.MaxHP))
                    {
                        log.DebugFormat("HP less then trigger HP, activating {0}", r.BaseRune.Name);
                        activate = true;
                    }
                    break;
                case 8:
                    if (opponent.Battlefield.Count - r.BaseRune.SkillConditionValue > you.Battlefield.Count)
                    {
                        activate = true;
                    }

                    break;
                default:
                    break;
            }
            if (activate)
            {
                return UseSkill(you, opponent);
            }
            return null;
        }

        private bool CheckNumberOfCardsInZone(List<BattleCard> zone, int amount, int compareType, int race)
        {
            int number = zone.Count;
            if (race != 0)
            {
                number = zone.Count(x => x.Race == (Race)race);
            }

            if (compareType == 1)
            {
                return number > amount;
            }
            else if (compareType == 3)
            {
                return number < amount;
            }
            throw new NotImplementedException("Only SkillConditionCompare values 1 and 3 are implemented");
        }

        private Skill UseSkill(BattleUser you, BattleUser opponent)
        {
            Skill s = r.GetSkill();
            //log.DebugFormat("{0} {1} {2} {3} {4} {5}", s.Name, s.AffectType, s.LanchCondition, s.LanchConditionValue, s.SkillCategory, s.Type, s.LanchType);
            if (s.LanchType == LaunchType.Runes && you.Battlefield.Count == 0)
            {
                log.DebugFormat("No cards on your battlefield, not activating {0}", r.Name);
                return null;
            }
            if (s.Type == SkillType.Purification)
            {
                bool hasCardWithAfflictions = false;
                // Divine Plea will be triggered only when at least one of your cards has been stunned/paralyzed/frozen/poisoned/burned.
                foreach (var c in you.Battlefield)
                {
                    if (c.Stunned || c.Paralyzed || c.Frozen || c.Poisoned || c.Burned 
                        || c.Trapped || c.Confused || c.Silenced || c.Laceration) // also adding the other status ailments *educated guess*
                    {
                        hasCardWithAfflictions = true;
                        break;
                    }
                }

                if (!hasCardWithAfflictions)
                {
                    log.DebugFormat("No cards who are stunned/paralyzed/frozen/poisoned/burned on your battlefield, not activating {0}", r.Name);
                    return null;
                }
            }
            Active = true;
            NumActivationsLeft--;
            log.InfoFormat("Activating rune {0} skill {1} (activation {2}/{3})", r.Name, s.Name, r.BaseRune.SkillTimes - NumActivationsLeft, r.BaseRune.SkillTimes);
            return s;
        }
    }
}
