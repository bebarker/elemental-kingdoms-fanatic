﻿namespace ElementalKingdoms.ui
{
    partial class RuneSearcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RuneSearcher));
            this.cbDisplayOnlyOwnedRunes = new System.Windows.Forms.CheckBox();
            this.cbElementFire = new System.Windows.Forms.CheckBox();
            this.cbElementAir = new System.Windows.Forms.CheckBox();
            this.cbElementWater = new System.Windows.Forms.CheckBox();
            this.cbElementEarth = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbStars4 = new System.Windows.Forms.CheckBox();
            this.cbStars3 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbStars2 = new System.Windows.Forms.CheckBox();
            this.cbStars1 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numLevel = new System.Windows.Forms.NumericUpDown();
            this.btnOK = new System.Windows.Forms.Button();
            this.tbFilter = new System.Windows.Forms.TextBox();
            this.lbRunes = new System.Windows.Forms.ListBox();
            this.cbStars5 = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.numLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // cbDisplayOnlyOwnedRunes
            // 
            this.cbDisplayOnlyOwnedRunes.AutoSize = true;
            this.cbDisplayOnlyOwnedRunes.Location = new System.Drawing.Point(308, 18);
            this.cbDisplayOnlyOwnedRunes.Name = "cbDisplayOnlyOwnedRunes";
            this.cbDisplayOnlyOwnedRunes.Size = new System.Drawing.Size(161, 21);
            this.cbDisplayOnlyOwnedRunes.TabIndex = 47;
            this.cbDisplayOnlyOwnedRunes.Text = "Only runes you have";
            this.cbDisplayOnlyOwnedRunes.UseVisualStyleBackColor = true;
            this.cbDisplayOnlyOwnedRunes.Visible = false;
            this.cbDisplayOnlyOwnedRunes.CheckedChanged += new System.EventHandler(this.cbAny_CheckedChanged);
            // 
            // cbElementFire
            // 
            this.cbElementFire.AutoSize = true;
            this.cbElementFire.Checked = true;
            this.cbElementFire.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbElementFire.Location = new System.Drawing.Point(267, 70);
            this.cbElementFire.Name = "cbElementFire";
            this.cbElementFire.Size = new System.Drawing.Size(54, 21);
            this.cbElementFire.TabIndex = 46;
            this.cbElementFire.Text = "Fire";
            this.cbElementFire.UseVisualStyleBackColor = true;
            this.cbElementFire.CheckedChanged += new System.EventHandler(this.cbAny_CheckedChanged);
            // 
            // cbElementAir
            // 
            this.cbElementAir.AutoSize = true;
            this.cbElementAir.Checked = true;
            this.cbElementAir.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbElementAir.Location = new System.Drawing.Point(214, 70);
            this.cbElementAir.Name = "cbElementAir";
            this.cbElementAir.Size = new System.Drawing.Size(47, 21);
            this.cbElementAir.TabIndex = 45;
            this.cbElementAir.Text = "Air";
            this.cbElementAir.UseVisualStyleBackColor = true;
            this.cbElementAir.CheckedChanged += new System.EventHandler(this.cbAny_CheckedChanged);
            // 
            // cbElementWater
            // 
            this.cbElementWater.AutoSize = true;
            this.cbElementWater.Checked = true;
            this.cbElementWater.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbElementWater.Location = new System.Drawing.Point(140, 70);
            this.cbElementWater.Name = "cbElementWater";
            this.cbElementWater.Size = new System.Drawing.Size(68, 21);
            this.cbElementWater.TabIndex = 44;
            this.cbElementWater.Text = "Water";
            this.cbElementWater.UseVisualStyleBackColor = true;
            this.cbElementWater.CheckedChanged += new System.EventHandler(this.cbAny_CheckedChanged);
            // 
            // cbElementEarth
            // 
            this.cbElementEarth.AutoSize = true;
            this.cbElementEarth.Checked = true;
            this.cbElementEarth.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbElementEarth.Location = new System.Drawing.Point(70, 70);
            this.cbElementEarth.Name = "cbElementEarth";
            this.cbElementEarth.Size = new System.Drawing.Size(64, 21);
            this.cbElementEarth.TabIndex = 43;
            this.cbElementEarth.Text = "Earth";
            this.cbElementEarth.UseVisualStyleBackColor = true;
            this.cbElementEarth.CheckedChanged += new System.EventHandler(this.cbAny_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 17);
            this.label7.TabIndex = 42;
            this.label7.Text = "Element";
            // 
            // cbStars4
            // 
            this.cbStars4.AutoSize = true;
            this.cbStars4.Checked = true;
            this.cbStars4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStars4.Location = new System.Drawing.Point(202, 43);
            this.cbStars4.Name = "cbStars4";
            this.cbStars4.Size = new System.Drawing.Size(38, 21);
            this.cbStars4.TabIndex = 41;
            this.cbStars4.Text = "4";
            this.cbStars4.UseVisualStyleBackColor = true;
            this.cbStars4.CheckedChanged += new System.EventHandler(this.cbAny_CheckedChanged);
            // 
            // cbStars3
            // 
            this.cbStars3.AutoSize = true;
            this.cbStars3.Checked = true;
            this.cbStars3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStars3.Location = new System.Drawing.Point(158, 43);
            this.cbStars3.Name = "cbStars3";
            this.cbStars3.Size = new System.Drawing.Size(38, 21);
            this.cbStars3.TabIndex = 40;
            this.cbStars3.Text = "3";
            this.cbStars3.UseVisualStyleBackColor = true;
            this.cbStars3.CheckedChanged += new System.EventHandler(this.cbAny_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 17);
            this.label6.TabIndex = 39;
            this.label6.Text = "Stars";
            // 
            // cbStars2
            // 
            this.cbStars2.AutoSize = true;
            this.cbStars2.Checked = true;
            this.cbStars2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStars2.Location = new System.Drawing.Point(114, 43);
            this.cbStars2.Name = "cbStars2";
            this.cbStars2.Size = new System.Drawing.Size(38, 21);
            this.cbStars2.TabIndex = 38;
            this.cbStars2.Text = "2";
            this.cbStars2.UseVisualStyleBackColor = true;
            this.cbStars2.CheckedChanged += new System.EventHandler(this.cbAny_CheckedChanged);
            // 
            // cbStars1
            // 
            this.cbStars1.AutoSize = true;
            this.cbStars1.Checked = true;
            this.cbStars1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStars1.Location = new System.Drawing.Point(70, 43);
            this.cbStars1.Name = "cbStars1";
            this.cbStars1.Size = new System.Drawing.Size(38, 21);
            this.cbStars1.TabIndex = 37;
            this.cbStars1.Text = "1";
            this.cbStars1.UseVisualStyleBackColor = true;
            this.cbStars1.CheckedChanged += new System.EventHandler(this.cbAny_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 17);
            this.label5.TabIndex = 36;
            this.label5.Text = "Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 244);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 35;
            this.label1.Text = "Level";
            // 
            // numLevel
            // 
            this.numLevel.Location = new System.Drawing.Point(80, 242);
            this.numLevel.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numLevel.Name = "numLevel";
            this.numLevel.Size = new System.Drawing.Size(48, 22);
            this.numLevel.TabIndex = 34;
            this.numLevel.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numLevel.ValueChanged += new System.EventHandler(this.numLevel_ValueChanged);
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(121, 281);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 33;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // tbFilter
            // 
            this.tbFilter.Location = new System.Drawing.Point(70, 15);
            this.tbFilter.Name = "tbFilter";
            this.tbFilter.Size = new System.Drawing.Size(232, 22);
            this.tbFilter.TabIndex = 32;
            this.tbFilter.TextChanged += new System.EventHandler(this.tbFilter_TextChanged);
            // 
            // lbRunes
            // 
            this.lbRunes.FormattingEnabled = true;
            this.lbRunes.ItemHeight = 16;
            this.lbRunes.Location = new System.Drawing.Point(14, 97);
            this.lbRunes.Name = "lbRunes";
            this.lbRunes.Size = new System.Drawing.Size(274, 132);
            this.lbRunes.TabIndex = 30;
            this.lbRunes.SelectedIndexChanged += new System.EventHandler(this.lbRunes_SelectedIndexChanged);
            // 
            // cbStars5
            // 
            this.cbStars5.AutoSize = true;
            this.cbStars5.Checked = true;
            this.cbStars5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStars5.Location = new System.Drawing.Point(246, 43);
            this.cbStars5.Name = "cbStars5";
            this.cbStars5.Size = new System.Drawing.Size(38, 21);
            this.cbStars5.TabIndex = 48;
            this.cbStars5.Text = "5";
            this.cbStars5.UseVisualStyleBackColor = true;
            this.cbStars5.CheckedChanged += new System.EventHandler(this.cbAny_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(294, 97);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(308, 254);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // RuneSearcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 320);
            this.Controls.Add(this.cbStars5);
            this.Controls.Add(this.cbDisplayOnlyOwnedRunes);
            this.Controls.Add(this.cbElementFire);
            this.Controls.Add(this.cbElementAir);
            this.Controls.Add(this.cbElementWater);
            this.Controls.Add(this.cbElementEarth);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbStars4);
            this.Controls.Add(this.cbStars3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbStars2);
            this.Controls.Add(this.cbStars1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numLevel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tbFilter);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbRunes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RuneSearcher";
            this.Text = "RuneSearcher";
            ((System.ComponentModel.ISupportInitialize)(this.numLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbDisplayOnlyOwnedRunes;
        private System.Windows.Forms.CheckBox cbElementFire;
        private System.Windows.Forms.CheckBox cbElementAir;
        private System.Windows.Forms.CheckBox cbElementWater;
        private System.Windows.Forms.CheckBox cbElementEarth;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cbStars4;
        private System.Windows.Forms.CheckBox cbStars3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cbStars2;
        private System.Windows.Forms.CheckBox cbStars1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numLevel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox tbFilter;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListBox lbRunes;
        private System.Windows.Forms.CheckBox cbStars5;
    }
}