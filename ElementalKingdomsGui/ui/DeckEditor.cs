﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.util;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ElementalKingdoms.ui
{
    public partial class DeckEditor : UserControl
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Button[] CardButtons;
        private Button[] RuneButtons;

        public Deck Deck;
        public User User;

        private List<UserCard> UserCards { get; set; }

        private static CardSearcher searcher = null;
        
        public DeckEditor()
        {
            InitializeComponent();

            if (searcher == null)
            {
                searcher = new CardSearcher();
            }

            Deck = new Deck();
            UserCards = new List<UserCard>();

            CardButtons = new Button[] { btnCard1, btnCard2, btnCard3, btnCard4, btnCard5, btnCard6, btnCard7, btnCard8, btnCard9, btnCard10 };
            RuneButtons = new Button[] { btnRune1, btnRune2, btnRune3, btnRune4 };
        }

        public void SetUser(User user)
        {
            User = new User();
            User.NickName = user.NickName;
            User.Level = user.Level;
            User.Avatar = user.Avatar;

            User.UserCards = user.UserCards.ToList();

            numEditHeroLevel.ValueChanged -= numEditHeroLevel_ValueChanged;
            numEditHeroLevel.Value = user.Level;
            numEditHeroLevel.ValueChanged += numEditHeroLevel_ValueChanged;

            SetDeck(user.UserDeck);
            //tbName.Text = user.NickName;
        }

        public void SetDeck(Deck d)
        {
            Deck = new Deck();
            Deck.Name = d.Name;
            Deck.GroupId = d.GroupId;
            Deck.Uid = d.Uid;
            Deck.Cards = d.Cards.ToList();
            Deck.Runes = d.Runes.ToList();
            Deck.GroupId = d.GroupId;

            tbName.Text = d.Name;

            User.UserDeck = Deck;

            UpdateTooltips();
            UpdateDeckCost();
        }

        public void SetUserCards(List<UserCard> userCards)
        {
            UserCards = userCards;
            searcher.SetUserCards(userCards);
        }

        private void UpdateTooltips()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i >= Deck.Cards.Count)
                {
                    CardButtons[i].Text = "";
                    CardButtons[i].BackgroundImage = null;
                    CardButtons[i].Tag = null;
                }
                else
                {
                    var c = Deck.Cards[i];
                    if (CardButtons[i].Tag == c) continue;

                    CardButtons[i].BackgroundImageLayout = ImageLayout.Zoom;
                    CardButtons[i].BackgroundImage = ImageLoader.GetSmallImageByName(c.Name);
                    CardButtons[i].Text = "";
                    CardButtons[i].Tag = c;
                }
            }


            for (int i = 0; i < 4; i++)
            {
                if (i >= Deck.Runes.Count)
                {
                    RuneButtons[i].Text = "";
                    RuneButtons[i].BackgroundImage = null;
                }
                else
                {
                    var r = Deck.Runes[i];
                    var b = Rune.Runes.Find(x => x.Id == r.RuneId).Name;
                    var s = r.GetSkill();
                    string sName = "";
                    if (s != null) // shouldn't be possible
                    {
                        sName = s.Name + " -> " + s.Desc;
                    }
                    RuneButtons[i].Text = String.Format("{0}\n{1}\n{2}", b, r.Level, sName);
                    RuneButtons[i].BackgroundImageLayout = ImageLayout.Zoom;
                    RuneButtons[i].BackgroundImage = ImageLoader.GetRuneImageById(r.RuneId);
                    toolTip1.SetToolTip(RuneButtons[i], String.Format("{0}\nLevel: {1}\n{2}", r.Name, r.Level, sName));
                }
            }
        }

        private void btnCard_Click(object sender, EventArgs e)
        {
            UserCard editCard = null;

            int i = Array.IndexOf(CardButtons, sender);
            if (i < Deck.Cards.Count)
            {
                editCard = Deck.Cards[i];
            }

            List<UserCard> userCards;
            if (User != null && User.UserCards.Count > 0)
            {
                userCards = User.UserCards;
            }
            else
            {
                userCards = UserCards;
            }
            searcher.SetUserCards(userCards);
            searcher.SetCard(editCard);

            if (searcher.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            if (editCard == null)
            {
                Deck.Cards.Add(searcher.SelectedCard);
            }
            else
            {
                Deck.Cards[i] = searcher.SelectedCard;
            }

            UpdateTooltips();
            UpdateDeckCost();

            // fire deck changed event so that the parent can reset the battle.
            OnDeckChanged(EventArgs.Empty);
        }


        private void btnRune_Click(object sender, EventArgs e)
        {
            UserRune editRune = null;

            int i = Array.IndexOf(RuneButtons, sender);
            if (i < Deck.Runes.Count)
            {
                editRune = Deck.Runes[i];
            }

            List<UserRune> userRunes;
            if (User != null)
            {
                userRunes = User.UserRunes;
            }
            else
            {
                userRunes = new List<UserRune>();
            }

            var searcher = new RuneSearcher(editRune, userRunes);

            if (searcher.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            if (Deck.Runes.Find(x => x.Name == searcher.SelectedRune.Name) != null)
            {
                MessageBox.Show(String.Format("You already have a {0} rune in your deck.", searcher.SelectedRune.Name));
                return;
            }

            if (editRune == null)
            {
                Deck.Runes.Add(searcher.SelectedRune);
            }
            else
            {
                Deck.Runes[i] = searcher.SelectedRune;
            }

            UpdateTooltips();

            // fire deck changed event so that the parent can reset the battle.
            OnDeckChanged(EventArgs.Empty);
        }


        public event EventHandler DeckChanged;

        protected virtual void OnDeckChanged(EventArgs e)
        {
            UpdateDeckCost();

            EventHandler handler = DeckChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void btnCard_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int i = Array.IndexOf(CardButtons, sender);
                if (i >= Deck.Cards.Count) return;

                Deck.Cards.RemoveAt(i);
                UpdateTooltips();
                UpdateDeckCost();
                OnDeckChanged(EventArgs.Empty);
            }
        }

        private void btnCard_MouseHover(object sender, EventArgs e)
        {
            int i = Array.IndexOf(CardButtons, sender);
            if (i >= Deck.Cards.Count) return;
            var card = Deck.Cards[i];
            var s = Skill.Get(card.SkillNew);
            string sName = "";
            if (s != null)
            {
                sName = "Evolve skill: " + s.Name;
            }
            toolTip1.SetToolTip(CardButtons[i], String.Format("{0}\nLevel: {1}\n{2}", card.Name, card.Level, sName));
        }

        private void btnRune_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int i = Array.IndexOf(RuneButtons, sender);
                if (i >= Deck.Runes.Count) return;

                Deck.Runes.RemoveAt(i);
                UpdateTooltips();
                OnDeckChanged(EventArgs.Empty);
            }
        }
        
        private void numEditHeroLevel_ValueChanged(object sender, EventArgs e)
        {
            User.Level = Convert.ToInt32(numEditHeroLevel.Value);
            //Settings.Default.playerLevel = user.Level;
            OnDeckChanged(EventArgs.Empty);
            UpdateDeckCost();
        }

        private void UpdateDeckCost()
        {
            if (User.Level == 0)
            {
                pbDeckCost.Visible = false;
                lblCost.Text = "";
                lblCostTitle.Visible = false;
                return;
            }
            int current = User.UserDeck.FullCost;
            int max = User.Cost;
            // TODO: Recalculate initiative value based on the edited deck, not on the user deck?
            lblCost.Text = String.Format("{0} / {1} (initiative: {2})", current, max, User.GetFohMagicNumber());
            lblCostTitle.Visible = true;
            pbDeckCost.Visible = true;
            pbDeckCost.Maximum = max;
            if (current <= max)
            {
                pbDeckCost.Value = current;
                pbDeckCost.SetState(1);
            }
            else
            {
                pbDeckCost.Value = max;
                pbDeckCost.SetState(2);
            }
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            Deck.Name = tbName.Text;
        }
    }
}
