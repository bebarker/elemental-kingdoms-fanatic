# Elemental Kingdoms Fanatic #

Elemental Kingdoms Fanatic is a tool for simulating battles in the game called Elemental Kingdoms. It has a user friendly interface for creating and editing decks. There is also a replay viewer which can be used to watch battles that can be obtained from the game servers, or battles that have been executed by this simulator. 

### Projects ###

The solution consists of 4 projects

* ElementalKingdoms - core functionality, generates ekf.dll
* ElementalKingdomsGui - User interface, generates Elemental Kingdoms Fanatic.exe
* ElementalKingdomsConsole - Command line interface, generates ekfc.exe. Been tested to work on Mono
* ElementalKingdomsTest - unit test

### How do I get set up? ###

* Download Visual Studio community edition (free) on https://www.visualstudio.com/
* Java is required for the card data file unpacker
* Open the solution file
* Run the ElementalKingdomsGui project

### Contribution guidelines ###

Feel free to submit pull requests. 

### Who do I talk to? ###

Leave a comment on http://peppa84.com or create an issue on this site.