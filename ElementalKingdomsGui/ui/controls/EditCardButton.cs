﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.ui;
using ElementalKingdoms.util;

namespace ElementalKingdomsGui.ui.controls
{
    public partial class EditCardButton : UserControl
    {
        private UserCard editCard;
        public UserCard EditCard {
            get { return editCard; }
            set { editCard = value; UpdateCard(); }
        }

        public string DefaultText { get; set; }

        public EditCardButton()
        {
            InitializeComponent();
        }

        private void btnEditCard_Click(object sender, EventArgs e)
        {
            CardSearcher searcher = new CardSearcher();
            
            searcher.SetCard(EditCard);

            if (searcher.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            
            EditCard = searcher.SelectedCard;
        }

        private void UpdateCard()
        {
            if (editCard != null)
            {
                btnEditCard.BackgroundImageLayout = ImageLayout.Zoom;
                btnEditCard.BackgroundImage = ImageLoader.GetSmallImageByName(editCard.Name);
                btnEditCard.Text = editCard.Name;
                btnEditCard.Tag = editCard;
            }
            else
            {
                btnEditCard.BackgroundImage = null;
                btnEditCard.Text = DefaultText;
                btnEditCard.Tag = null;

            }
        }
    }
}
