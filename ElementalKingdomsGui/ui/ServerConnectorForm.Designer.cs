﻿namespace ElementalKingdoms.ui
{
    partial class ServerConnectorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerConnectorForm));
            this.bcConnectToServer = new System.ComponentModel.BackgroundWorker();
            this.btnConnect = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnSkills = new System.Windows.Forms.Button();
            this.btnRunes = new System.Windows.Forms.Button();
            this.btnLeague = new System.Windows.Forms.Button();
            this.btnMaps = new System.Windows.Forms.Button();
            this.btnCards = new System.Windows.Forms.Button();
            this.pbLeagueCheck = new System.Windows.Forms.PictureBox();
            this.pbMapsCheck = new System.Windows.Forms.PictureBox();
            this.pbRunesCheck = new System.Windows.Forms.PictureBox();
            this.pbSkillsCheck = new System.Windows.Forms.PictureBox();
            this.pbCardsCheck = new System.Windows.Forms.PictureBox();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLeagueCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMapsCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRunesCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSkillsCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardsCheck)).BeginInit();
            this.SuspendLayout();
            // 
            // bcConnectToServer
            // 
            this.bcConnectToServer.WorkerReportsProgress = true;
            this.bcConnectToServer.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bcConnectToServer_DoWork);
            this.bcConnectToServer.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bcConnectToServer_ProgressChanged);
            this.bcConnectToServer.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bcConnectToServer_RunWorkerCompleted);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(12, 12);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lblMessage});
            this.statusStrip1.Location = new System.Drawing.Point(0, 289);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(512, 25);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(99, 20);
            this.lblStatus.Text = "Disconnected";
            // 
            // lblMessage
            // 
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 20);
            // 
            // comboBox1
            // 
            this.comboBox1.Enabled = false;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 58);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 24);
            this.comboBox1.TabIndex = 3;
            // 
            // btnSkills
            // 
            this.btnSkills.Enabled = false;
            this.btnSkills.Location = new System.Drawing.Point(12, 117);
            this.btnSkills.Name = "btnSkills";
            this.btnSkills.Size = new System.Drawing.Size(75, 23);
            this.btnSkills.TabIndex = 5;
            this.btnSkills.Text = "Skills";
            this.btnSkills.UseVisualStyleBackColor = true;
            this.btnSkills.Click += new System.EventHandler(this.btnSkills_Click);
            // 
            // btnRunes
            // 
            this.btnRunes.Enabled = false;
            this.btnRunes.Location = new System.Drawing.Point(12, 146);
            this.btnRunes.Name = "btnRunes";
            this.btnRunes.Size = new System.Drawing.Size(75, 23);
            this.btnRunes.TabIndex = 6;
            this.btnRunes.Text = "Runes";
            this.btnRunes.UseVisualStyleBackColor = true;
            this.btnRunes.Click += new System.EventHandler(this.btnRunes_Click);
            // 
            // btnLeague
            // 
            this.btnLeague.Enabled = false;
            this.btnLeague.Location = new System.Drawing.Point(12, 204);
            this.btnLeague.Name = "btnLeague";
            this.btnLeague.Size = new System.Drawing.Size(75, 23);
            this.btnLeague.TabIndex = 7;
            this.btnLeague.Text = "League";
            this.btnLeague.UseVisualStyleBackColor = true;
            this.btnLeague.Click += new System.EventHandler(this.btnLeague_Click);
            // 
            // btnMaps
            // 
            this.btnMaps.Enabled = false;
            this.btnMaps.Location = new System.Drawing.Point(12, 175);
            this.btnMaps.Name = "btnMaps";
            this.btnMaps.Size = new System.Drawing.Size(75, 23);
            this.btnMaps.TabIndex = 8;
            this.btnMaps.Text = "Maps";
            this.btnMaps.UseVisualStyleBackColor = true;
            this.btnMaps.Click += new System.EventHandler(this.btnMaps_Click);
            // 
            // btnCards
            // 
            this.btnCards.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCards.Enabled = false;
            this.btnCards.Location = new System.Drawing.Point(12, 88);
            this.btnCards.Name = "btnCards";
            this.btnCards.Size = new System.Drawing.Size(75, 23);
            this.btnCards.TabIndex = 4;
            this.btnCards.Text = "Cards";
            this.btnCards.UseVisualStyleBackColor = true;
            this.btnCards.Click += new System.EventHandler(this.btnCards_Click);
            // 
            // pbLeagueCheck
            // 
            this.pbLeagueCheck.Image = global::ElementalKingdomsGui.Properties.Resources.checkmark;
            this.pbLeagueCheck.Location = new System.Drawing.Point(95, 204);
            this.pbLeagueCheck.Name = "pbLeagueCheck";
            this.pbLeagueCheck.Size = new System.Drawing.Size(38, 23);
            this.pbLeagueCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbLeagueCheck.TabIndex = 9;
            this.pbLeagueCheck.TabStop = false;
            this.pbLeagueCheck.Visible = false;
            // 
            // pbMapsCheck
            // 
            this.pbMapsCheck.Image = global::ElementalKingdomsGui.Properties.Resources.checkmark;
            this.pbMapsCheck.Location = new System.Drawing.Point(95, 175);
            this.pbMapsCheck.Name = "pbMapsCheck";
            this.pbMapsCheck.Size = new System.Drawing.Size(38, 23);
            this.pbMapsCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMapsCheck.TabIndex = 10;
            this.pbMapsCheck.TabStop = false;
            this.pbMapsCheck.Visible = false;
            // 
            // pbRunesCheck
            // 
            this.pbRunesCheck.Image = global::ElementalKingdomsGui.Properties.Resources.checkmark;
            this.pbRunesCheck.Location = new System.Drawing.Point(95, 146);
            this.pbRunesCheck.Name = "pbRunesCheck";
            this.pbRunesCheck.Size = new System.Drawing.Size(38, 23);
            this.pbRunesCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbRunesCheck.TabIndex = 11;
            this.pbRunesCheck.TabStop = false;
            this.pbRunesCheck.Visible = false;
            // 
            // pbSkillsCheck
            // 
            this.pbSkillsCheck.Image = global::ElementalKingdomsGui.Properties.Resources.checkmark;
            this.pbSkillsCheck.Location = new System.Drawing.Point(95, 117);
            this.pbSkillsCheck.Name = "pbSkillsCheck";
            this.pbSkillsCheck.Size = new System.Drawing.Size(38, 23);
            this.pbSkillsCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbSkillsCheck.TabIndex = 12;
            this.pbSkillsCheck.TabStop = false;
            this.pbSkillsCheck.Visible = false;
            // 
            // pbCardsCheck
            // 
            this.pbCardsCheck.Image = global::ElementalKingdomsGui.Properties.Resources.checkmark;
            this.pbCardsCheck.Location = new System.Drawing.Point(95, 88);
            this.pbCardsCheck.Name = "pbCardsCheck";
            this.pbCardsCheck.Size = new System.Drawing.Size(38, 23);
            this.pbCardsCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbCardsCheck.TabIndex = 13;
            this.pbCardsCheck.TabStop = false;
            this.pbCardsCheck.Visible = false;
            // 
            // ServerConnectorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 314);
            this.Controls.Add(this.pbCardsCheck);
            this.Controls.Add(this.pbSkillsCheck);
            this.Controls.Add(this.pbRunesCheck);
            this.Controls.Add(this.pbMapsCheck);
            this.Controls.Add(this.pbLeagueCheck);
            this.Controls.Add(this.btnMaps);
            this.Controls.Add(this.btnLeague);
            this.Controls.Add(this.btnRunes);
            this.Controls.Add(this.btnSkills);
            this.Controls.Add(this.btnCards);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnConnect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ServerConnectorForm";
            this.Text = "Server data loader";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLeagueCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMapsCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRunesCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSkillsCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardsCheck)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker bcConnectToServer;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnCards;
        private System.Windows.Forms.Button btnSkills;
        private System.Windows.Forms.Button btnRunes;
        private System.Windows.Forms.Button btnLeague;
        private System.Windows.Forms.ToolStripStatusLabel lblMessage;
        private System.Windows.Forms.Button btnMaps;
        private System.Windows.Forms.PictureBox pbLeagueCheck;
        private System.Windows.Forms.PictureBox pbMapsCheck;
        private System.Windows.Forms.PictureBox pbRunesCheck;
        private System.Windows.Forms.PictureBox pbSkillsCheck;
        private System.Windows.Forms.PictureBox pbCardsCheck;
    }
}